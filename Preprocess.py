import sys
import urllib
import json
import pathlib as pL
import os
# import gi
# gi.require_version( "Notify", "0.7")
# from gi.repository import Notify
# Notify.init( "T")

from collections.abc import Iterator


def walkChapters( rootSections: list[ dict])-> Iterator[ dict]:
	for item in rootSections:
		yield item
		for subItem in walkChapters( item[ "Chapter"][ "sub_items"]):
			yield subItem


headerDenotation= "# "

# Effectively wikilink-> markdown format

if __name__== "__main__":
	if len( sys.argv)> 1:
		sys.exit( 0)

	# notif= Notify.Notification.new( str( sys.argv))
	# notif.show()

	context, book= json.load( sys.stdin)

	def conformDocumentLink( link: str)-> str:
		safe= link.replace( " ", "%20")
		return safe
	def conformHeaderLink( link: str)-> str:
		safe= link.replace( " ", "-").lower()
		return safe
	
	for chapter in walkChapters( book[ "sections"]):
		content: str= chapter[ "Chapter"][ "content"]
		sys.stderr.write( chapter[ "Chapter"][ "content"])
		sys.stderr.write( "\n")
		content= "\n".join(
			[
				line+ "  "
				for line in content.splitlines()
			]
		)

		# Need to replace:
		# [[{file link}]]          -> [{original text}]({safe text})
		# [[{file link}#{header}]] -> [{original text}]({safe link}#{safe header})
		# ![[{image link}]]        -> ![{original text- extension}]({safe link})

		# So need to find all matching [[ pair-s within the content, dont need to alter the one-s prefaced by !
		# Can search forward for [[ and backward for ]]
		# Then they can be paired
		# No that does not work
		# Search forward for [[
		# Then search for a ]] from there,
		# If it assumed that no nesting of [[ ]] is occuring then one can search for the following ]] after locating a [[

		# Queue replacements and loop through in reverse order to make the change-s

		start= 0
		changes= list()
		while True:
			initiatingIndex= content.find( "[[", start)
			if initiatingIndex== -1:
				break

			else:
				finalizingIndex= content.find( "]]", initiatingIndex)

				if finalizingIndex== -1:
					break
				else:
					# Found pair
					text= content[ initiatingIndex+ 2: finalizingIndex]


					if ( initiatingIndex!= 0) and ( content[ initiatingIndex- 1]== "!"):
						# Data link
						# Perhaps non image link-s should be removed
						# So in obsidian the link simply needs to reference any file within the structure whereas here
						# A link is required to have the path relative 
						# So if no path structure then locate file

						head, tail= os.path.split( text)
						if len( tail):
							extensionIndex= tail.rfind( ".")
							if extensionIndex!= -1:
								name= tail[ : extensionIndex]
							else:
								name= tail
						else:
							name= text

						if "/" not in text:
							# Search for file in root
							root= pL.Path( context[ "root"])
							root/= context[ "config"][ "book"][ "src"]
							matches= [ path for path in root.glob( "**/"+ text)]

							if len( matches):
								match= matches[ 0]
								match= match.relative_to( root)
								link= urllib.parse.quote( str( match))
						else:
							link= urllib.parse.quote( text)

					else:
						# Markdown link
						# URL-S ARE CURRENTLY UNSUPPORTED

						# If | in text then use the following text for name
						
						# So if hash then split, 
						# for link
						# with front ( escape space using %20, add a markdown extension if there is none), with back ( escape space using -)
						# for name use whole original and remove any extension in front if there is one ( i think this will not cause conflict)

						# If no hash
						# for link
						# escape space using %20
						# for name use whole original and remove any extension if there is one

						name= None
						barLocation= text.rfind( "|")
						if ( barLocation!= -1) and ( ( barLocation+ 1)!= len( text)):
							name= text[ barLocation+ 1: ]
							text= text[ : barLocation]

						doubleCrossPos= text.find( "#")
						if doubleCrossPos!= -1:
							document, heading= text.rsplit( "#", maxsplit= 1)

							extensionIndex= document.rfind( ".")
							if extensionIndex!= -1:
								if name== None:
									name= document[ : extensionIndex]+ headerDenotation+ heading
							else:
								if name== None:
									name= document+ headerDenotation+ heading
								document+= ".md"

							link= urllib.parse.quote( document)+ "#"+ conformHeaderLink( heading)

						else:
							link= urllib.parse.quote( text)

							extensionIndex= text.rfind( ".")
							if extensionIndex!= -1:
								if name== None:
									name= text[ : extensionIndex]
							else:
								if name== None:
									name= text
								link+= ".md"

					changes.append( ( ( initiatingIndex, finalizingIndex), ( name, link)))
					start= finalizingIndex+ 2

		for ( initiatingIndex, finalizingIndex), ( name, link) in reversed( changes):
			content= content[ : initiatingIndex]+ \
				f"[{name}]({link})"+ \
				content[ finalizingIndex+ 2: ]

		chapter[ "Chapter"][ "content"]= content


	print( json.dumps( book))
	

	printz= False
	if printz:
		import pprint
		import io
		ph= io.StringIO()
		pprint.pprint( context, ph)
		ph.write( "\n\n----------\n\n")
		pprint.pprint( book, ph)

		with open( "/home/huuman/dumpie", "w") as f:
			f.write( ph.getvalue())
	
