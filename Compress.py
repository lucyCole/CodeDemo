from zipfile import (
	ZipFile,
)
from pathlib import (
	Path,
)
fileName= Path( "/tmp/codeDemonstrations.zip")
file= ZipFile(
	file= fileName,
	mode= "w",
)
currentDir= Path( __file__).parent
codeDemonstrationsPath= Path( "codeDemonstrations")
file.write(
	currentDir/ "ConceptsCodeDemonstration.pdf",
	arcname= codeDemonstrationsPath/ "ConceptsCodeDemonstration.pdf",
)
examples= {
	"BackPropagationNeuralNet.py",
	"LPAndStochastic.py",
	"OptimisationDifferenciation.py",
	"ProbabilityConcepts.py",
	"SiteOptimisation.py",
}
demoItemsDir= codeDemonstrationsPath/ "items"
itemsDir= currentDir/ "items"
for example in examples:
	file.write(
		itemsDir/ example,
		arcname= demoItemsDir/ example,
	)
file.close()
