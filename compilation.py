import pathlib as pL
import math
from subprocess import (
	run,
)
import shutil
from mAAP.Utils import(
	TextUtils,
)
descriptions= {
	"BackPropagationNeuralNet": (
		"Back Propagation Neural Net",
		"""
This was done using the book, "An introduction to neural computing" by Aleksander and Morton.
It is a feed forward network that trains via back propagation, the activation function for all neurons is a sigmoid function.
""".strip(),
	),
	"LPAndStochastic": (
		"LP And Stochastic",
		"""
The linear programming portion is a implementation which steps around the boundary of the feasible solution region shape.
{ less than or equal to, greater than or equal to, equal to} constraints are supported with the two stage large number solution used.

The stochastic programming portion allows optimisation in a scenario where an arbitrary number of stages are involved.
At each stage, a variable must be decided upon whilst only knowing the probability of different future branching and not a definite knowledge.
The implementation here generates an input which can be used in the linear programming function.
""".strip(),
	),
	"OptimisationDifferenciation": (
		"Optimisation Differenciation",
		"""
This was designed as a test scenario for a problem requiring opimisation.
I wish to show off the symbolic differenciation aspect
""".strip(),
	),
	"ProbabilityConcepts": (
		"Probability Concepts",
		"""
This implements probability theory concepts starting from a probability space with common set operations such as the power set,
and then expanding to define concepts such as { distributions, random variables, product spaces, expectation, variance, independance, conditionality, stochastic processes, markov chains, markov chain classification, steady state probabilities}
It also includes a Matrix definition which includes inversion and equation system solving
""".strip(), # { reversible markov chains, n step probabilities, realisation}
	),
	"SiteOptimisation": (
		"Site Optimisation",
		"""
This is a test to experiment with non linear optimisation of an arbitrary function tree. It only works with specific functions but I wish to demonstrate my explorations.
""".strip(),
	),
}
intro= """
These are code files which are designed to convince the evaluator that I can operate competantly on the course,
They can be run with a python intepreter which supports version 3.10 or more recent.
This hopefully eleviates the time needed to evaluate my competancy in the subjects.
The files: { OptimisationDifferenciation, SiteOptimisation} rely upon my larger software stack and will not run, however the others operate with solely the python standard library.
If matplotlib and pyqt are installed then ProbabilityConcepts will use them to plot examples.
The pdf formatting may make running the files difficult, they are available in pure form at this location: "https://gitlab.com/lucyCole/CodeDemo/-/archive/main/CodeDemo-main.zip?path=items"
""".strip()
# chosen= "BackPropagationNeuralNet"
# chosen= "LPAndStochastic"
# chosen= "SiteOptimisation"

# chosen= "OptimisationDifferenciation"
# # chosen= "ProbabilityConcepts"

# descriptions= { chosen: descriptions[ chosen]}

def enforceNewLines( string):
	return "  \n".join(
		string.splitlines()
	)+ "  "

cd= pL.Path( __file__).parent
items= cd/ "items"

codeWrapping= r"""
mainfont: CodeNewRomanNerdFont-Regular.otf
monofont: CodeNewRomanNerdFontMono-Regular.otf
header-includes:
    - \usepackage{setspace}
""".strip()
# header-includes:
#  - \usepackage{fvextra}
#  - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,breakanywhere,commandchars=\\\{\}}
 # - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}


text= ""
# text+= "---\n"+ "header-includes:\n - \\usepackage{fvextra}\n - "+ r"\DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}"+ "\n---\n\nj\n\n"
text+= "---\n"+ codeWrapping+ "\n---\n"
text+= "# Concepts code demonstration: Lucy Coleclough\n"+ enforceNewLines( intro)+ "  \n\n"
text+= "\n".join(
	f"- [{ descriptions[ demoName][ 0]}](#{ descriptions[ demoName][ 0].lower().replace( " ", "-")})"
	# f"- [[#{ descriptions[ demoName][ 0]}]]"
	for demoName in descriptions
)
text+= "\n\n---\n  \n"
first= True
for demoName in descriptions:
	if first:
		first= False
	else:
		text+= "\n  \\\n  \\\n  \\\n  \\\n  \\\n  \\\n  \\\n  \n"
		# text+= "l\n"
	# text+= "# "+ descriptions[ demoName][ 0]+ "\n"
	demoIntro= descriptions[ demoName][ 1]
	text+= descriptions[ demoName][ 0]+ "\n---\n"
	text+= enforceNewLines( demoIntro)+ "\n  \n\n---\n\n\\normalsize\n"
	# text+= "\n"+ descriptions[ demoName][ 0].lower().replace( " ", "-")+ "\n---\n"
	code= ( cd/ items/ f"{ demoName}.py").read_text()
	# code= TextUtils.wrapTextChars(
	# 	code.splitlines(),
	# 	100,
	# )
	code= code.splitlines()
	total= len( code)
	size= 500
	iters= math.ceil( total/ size)
	blocks= list()
	for iter in range( iters):
		section= "\n".join(
			code[
				iter* size
				:
				( iter+ 1)* size
			]
		)
		# blocks.append( "\\small```python\n"+ section+ "\n```")
		blocks.append( "~~~~~{.python .numberLines}\n"+ section+ "\n~~~~~~")
		
	text+= "\n".join( blocks)+ "\n\\normalsize\n\n---\n"
	# text+= demoIntro+ "  \n\n~~~~~{.python}\n"+ code+ "\n~~~~~~\n\nj\n\n"

unified= cd/ "unified.md"
unified.write_text( text)

header= cd/ "head.tex"
# \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}"
# \begin{Highlighting}[breaklines,numbers=left]
header.write_text(
	r"""

% change style of quote, see also https://tex.stackexchange.com/a/436253/114857
\usepackage[most]{tcolorbox}

\definecolor{backquote}{RGB}{249,245,233}
\definecolor{bordercolor}{RGB}{221,221,221}

% change left border: https://tex.stackexchange.com/a/475716/114857
% change left margin: https://tex.stackexchange.com/a/457936/114857
\newtcolorbox{myquote}[1][]{%
    enhanced,
    breakable,
    size=minimal,
    left=10pt,
    top=5pt,
    frame hidden,
    boxrule=0pt,
    sharp corners=all,
    colback=backquote,
    borderline west={2pt}{0pt}{bordercolor},
    #1
}

% redefine quote environment to use the myquote environment, see https://tex.stackexchange.com/a/337587/114857
\renewenvironment{quote}{\begin{myquote}}{\end{myquote}}
""".strip()
)

metadata= cd/ "metadata.yml"
metadata.write_text(
	r"""
header-includes:
- |
  ```{=latex}
  \usepackage{fvextra}
  \begin{Highlighting}[breaklines,numbers=left]
""".strip()
)
  # \begin{document}

# \usepackage{fvextra}
# \begin{Highlighting}[breaklines,numbers=left]
run(
	[
		"pandoc",
		# "-f", "gfm",
		"--pdf-engine=xelatex",
		# "--highlight-style","zenburn",
		# "-H",
		# "./head.tex",
		"-V",
		"geometry:top=1cm, bottom=1cm, left=1cm, right=1cm",
		"-V", "colorlinks", "-V", "urlcolor=NavyBlue",
		# "-V", "mainfont:CodeNewRomanNerdFont-Regular.otf",
		"./unified.md",
		# "--metadata-file", "./metadata.yml", "-s",
		"-o",
		"./ConceptsCodeDemonstration.pdf",
	]
)

import sys;sys.exit()

bookSource= cd/ "src"
if bookSource.exists():
	shutil.rmtree( bookSource)
bookSource.mkdir(
	exist_ok= False,
)
demoLinks= "\n".join(
	[
		f"- [{ descriptions[ demoName][ 0]}]({ demoName}.md)"
		for demoName in descriptions
	]
)
summary= bookSource/ "SUMMARY.md"
summary.write_text(
	"- [Into](Intro.md)\n"+ demoLinks
)
introMd= bookSource/ "Intro.md"
introMd.write_text(
	intro+ "\n"+ demoLinks
)
for demoName in descriptions:
	demoMd= bookSource/ f"{ demoName}.md"
	code= ( cd/ items/ f"{ demoName}.py").read_text()
	demoIntro= descriptions[ demoName][ 1]
	demoMd.write_text(
		f"{ demoIntro}\n\n```py\n{ code}\n```"
	)

files= list(
	map(
		lambda p: p.name,
		list( bookSource.iterdir()),
	)
)


run(
	[
		"mdbook",
		"build",
	]
)
