"""
Whilst initial support was given to continuous spaces, they were abandoned until a full implementation can be attended to
For now only finite discrete spaces are fully supported
"""
from __future__ import annotations
import os
from pprint import pprint
import random
from typing import(
	Any,
	ForwardRef,
	TypedDict,
	TypeVar,
	Protocol,
	Union,
	# Self,
	ClassVar,
)
from collections.abc import(
	Callable,
	Collection,
	Sequence,
)
from dataclasses import(
	dataclass,
	field,
)
import math
from functools import(
	partial,
)

SpaceDataType= TypeVar( "SpaceDataType")
CountableSampleSpace= set[ SpaceDataType]
SampleSpaceSubset= CountableSampleSpace

SequenceType= TypeVar( "SequenceType")
def mult(
	q,
	w,
):
	return q* w
def add(
	q,
	w,
):
	return q+ w
def sub(
	q,
	w,
):
	return q- w

def apply(
	function: Callable[
		[
			SequenceType,
			SequenceType,
		],
		SequenceType,
	],
	sequence: Sequence,
)-> Union[
	None,
	SequenceType,
]:
	if len( sequence)== 0:
		return None
	currentItem= sequence[ 0]
	for item in sequence[ 1: ]:
		currentItem= function(
			currentItem,
			item,
		)
	return currentItem

def setUnion(
	q: set,
	w: set,
)-> set:
	for inputSet in ( q, w,):
		if isinstance(
			inputSet,
			(
				OpenInterval,
				UncountableProductSampleSpace,
			),
		):
			raise Exception( "Union between non discrete sets is not supported")
	return q.union( w)
	
def setIntersection(
	q: set,
	w: set,
)-> set:
	for inputSet in ( q, w,):
		if isinstance(
			inputSet,
			(
				OpenInterval,
				UncountableProductSampleSpace,
			),
		):
			raise Exception( "Union between non discrete sets is not supported")
	return q.intersection( w)

def integrateOverInterval(
	function,
	interval: ForwardRef( "OpenInterval"),
):
	integrationSteps= 20
	sum= 0.0
	offset= 0.00000000001
	step= ( ( interval.end- interval.start)- offset)/ integrationSteps
	location= offset
	for _ in range( integrationSteps):
		sum+= step* function( location)
		location+= step
	return sum

ValueType= TypeVar( "ValueType")
def cartesianSetProduct(
	*sets: Sequence[
		set[ ValueType]
	]
)-> set[
	tuple[
		ValueType,
		...,
	],
]:
	setLen= len( sets)
	def w(
		currentIndex: int,
		inputTuple: tuple[ ValueType],
	):
		currentSet= sets[ currentIndex]
		nextIndex= currentIndex+ 1
		for value in currentSet:
			currentTuple= inputTuple+ ( value,)
			if nextIndex== setLen:
				yield currentTuple
			else:
				for result in w(
					currentIndex= nextIndex,
					inputTuple= currentTuple
				):
					yield result

	return set(
		w(
			currentIndex= 0,
			inputTuple= tuple(),
		),
	)


InputType= TypeVar( "InputType")
def powerSet( inputSet: set[ InputType])-> set[
	frozenset[ InputType]
]:
	inputLen= len( inputSet)
	inputList= list( inputSet)
	powerSet= list()
	totalLen= int( math.pow( 2, inputLen))
	for iterationNumber in range(
		0,
		totalLen,
	):
		addedSet= list()
		binRep= bin( iterationNumber)
		binRepLen= len( binRep)
		for binRepNumber in range( binRepLen- 2):
			index= -1- binRepNumber
			if binRep[ index]== "1":
				addedSet.append( inputList[ index])
		powerSet.append( frozenset( addedSet))
	return set( powerSet)


def smallestSigmaAlgebra(
	# typing is wrong
	sampleSpace: CountableSampleSpace,
	inputSet: set[ SampleSpaceSubset],
):
	sigmaAlgebra= {
		frozenset(),
	}
	if isinstance(
		sampleSpace,
		(
			OpenInterval,
			UncountableProductSampleSpace,
		),
	):
		raise NotImplementedError( "Finding the smallest sigma algebra using a continuous sample space is not currently supported due to the calculation of the complement of items of the input set not being implemented")
		sigmaAlgebra.add( sampleSpace)
	else:
		sigmaAlgebra.add(
			frozenset( sampleSpace),
		)
	sigmaAlgebra= sigmaAlgebra.union( inputSet)
	inputSetUnions= set()
	for subSet in inputSet:
		if isinstance( subSet, OpenInterval):
			raise NotImplementedError( "Finding the smallest sigma algebra from a set which contains continuous members is not currently supported due to the calculation of ( the union between two continuous sets and the union between a continuous and non continuous set) not being implemented")
		subSet= frozenset( subSet)
		for existingSet in inputSetUnions.copy():
			inputSetUnions.add( existingSet.union( subSet))
		inputSetUnions.add( subSet)

	# add the complements
	for existingSet in inputSetUnions.copy():
		inputSetUnions.add(
			frozenset( sampleSpace.difference( existingSet))
		)
	return sigmaAlgebra.union(
		inputSetUnions,
	)

def chancesOfKOccurencesInNUniformTrials(
	n: int,
	k: int,
)-> int:
	return math.factorial( n)/ ( math.factorial( k)* math.factorial( n- k))

def greatestCommonDivisor( numbers: Collection[ int])-> int:
	numbers= list( numbers)
	numbersLen= len( numbers)
	if numbersLen< 2:
		if numbersLen== 1:
			return numbers[ 0]
		else:
			raise ValueError( "Input collection is empty")
	def greatestCommonDivisor2(
		q: int,
		w: int,
	)-> int:

		( largest, smallest)= ( q, w) if q> w else ( w, q)
		while True:
			result= largest% smallest
			if result== 0:
				return smallest
			largest= smallest
			smallest= result
	result= numbers[ 0]
	for number in numbers[ 1:]:
		result= greatestCommonDivisor2(
			q= result,
			w= number,
		)
	return result

class Matrix:
	@classmethod
	def identity(
		cls,
		size: int,
	):
		return cls(
			content= [
				[
					1.0 if ( column== row) else 0.0
					for column in range( size)
				]
				for row in range( size)
			]
		)
	def __init__(
		self,
		content: Sequence[
			Sequence[ float],
		], # sequence of sequence of column values, so sequence of rows
	):
		self.content= content
		self.rows= len( content)
		if self.rows> 0:
			self.columns= len( content[ 0])
		else:
			self.columns= 0

	def copy( self)-> Self:
		return Matrix(
			content= [
				row.copy()
				for row in self.content
			]
		)
	def __getitem__(
		self,
		index: int,
	)-> float:
		return self.content[ index]


	@staticmethod
	def mutatingAddition(
		q: Self,
		w: Any,
		subtract: bool,
	):
		if subtract:
			op= sub
		else:
			op= add
		if isinstance(
			w,
			(
				float,
				int,
			),
		):
			for rowIndex in range( q.rows):
				for columnIndex in range( q.columns):
					q.content[ rowIndex][ columnIndex]= op(
						q.content[ rowIndex][ columnIndex],
						w,
					)
		elif isinstance(
			w,
			Matrix,
		):
			if ( q.rows, q.columns)!= ( w.rows, w.columns):
				raise NotImplementedError
			
			for rowIndex in range( q.rows):
				for columnIndex in range( q.columns):
					q.content[ rowIndex][ columnIndex]= op(
						q.content[ rowIndex][ columnIndex],
						w.content[ rowIndex][ columnIndex],
					)
		else:
			raise NotImplementedError
	def __add__(
		self,
		other,
	):
		new= self.copy()
		self.mutatingAddition(
			q= new,
			w= other,
			subtract= False,
		)
		return new
	def __iadd__(
		self,
		other,
	):
		self.mutatingAddition(
			q= self,
			w= other,
			subtract= False,
		)
	def __sub__(
		self,
		other,
	):
		new= self.copy()
		self.mutatingAddition(
			q= new,
			w= other,
			subtract= True,
		)
		return new
	def __isub__(
		self,
		other,
	):
		self.mutatingAddition(
			q= self,
			w= other,
			subtract= True,
		)

	@staticmethod
	def mutatingMultiplication(
		q: Self,
		w: Any,
	):
		if isinstance(
			w,
			(
				float,
				int,
			),
		):
			for rowIndex in range( q.rows):
				for columnIndex in range( q.columns):
					q.content[ rowIndex][ columnIndex]*= w
		elif isinstance(
			w,
			Matrix,
		):
			if q.columns!= w.rows:
				raise NotImplementedError
			else:
				indexs= list( range( q.columns))
				rows= list()
				for rowIndex in range( q.rows):
					row= list()
					for columnIndex in range( w.columns):
						row.append(
							sum(
								map(
									lambda index: q.content[ rowIndex][ index]* w.content[ index][ columnIndex],
									indexs,
								)
							)
						)
					rows.append( row)
				q.content= rows
				q.rows= q.rows
				q.columns= w.columns
		else:
			raise NotImplementedError
	def __mul__(
		self,
		other,
	):
		new= self.copy()
		self.mutatingMultiplication(
			q= new,
			w= other,
		)
		return new
	def __imul__(
		self,
		other,
	):
		self.mutatingMultiplication(
			q= self,
			w= other,
		)


	@staticmethod
	def mutatingElimination(
		matrix: Self,
		equalRowNumbers: list[ float],
	):
		if len( equalRowNumbers)!= matrix.rows:
			raise ValueError( "Row numbers must match")

		if matrix.rows!= matrix.columns:
			raise NotImplementedError( "Only square matrix-s are currently supported")

		def move(
			source: int,
			sink: int,
		):
			nonlocal matrix, equalRowNumbers
			matrix.content.insert(
				sink,
				matrix.content.pop( source),
			)
			equalRowNumbers.insert(
				sink,
				equalRowNumbers.pop( source),
			)

		for unknownIndex in range( matrix.columns):
			currentPivotCoefficient= matrix.content[ unknownIndex][ unknownIndex]
			if currentPivotCoefficient== 0:
				foundRow= False
				for equationNumber in range(
					unknownIndex+ 1,
					matrix.rows,
				):
					currentPivotCoefficient= matrix.content[ equationNumber][ unknownIndex]
					if currentPivotCoefficient!= 0:
						foundRow= True
						break
				if not foundRow:
					raise NotImplementedError( "Column re-order-ing is not supported")
				move(
					source= equationNumber,
					sink= unknownIndex,
				)

			for equationNumber in range(
				unknownIndex+ 1,
				matrix.rows,
			):
				belowUnknownCoefficient= matrix.content[ equationNumber][ unknownIndex]
				if belowUnknownCoefficient!= 0:
					currentEquationMult= belowUnknownCoefficient/ currentPivotCoefficient
					for currentEquationUnknownIndex in range( matrix.columns):
						if currentEquationUnknownIndex== unknownIndex:
							continue
						matrix.content[ equationNumber][ currentEquationUnknownIndex]-= ( matrix.content[ unknownIndex][ currentEquationUnknownIndex]* currentEquationMult)
					equalRowNumbers[ equationNumber]-= ( equalRowNumbers[ unknownIndex]* currentEquationMult)

					matrix.content[ equationNumber][ unknownIndex]= 0.0

		knownValues= list()
		for unknownNumber in range( matrix.rows):
			unknownIndex= equationIndex= ( matrix.rows- 1)- unknownNumber
			knownCoefficientTotal= 0.0
			for knownCoefficientBackIndex in range(
				unknownNumber* -1,
				0,
			):
				knownCoefficientTotal+= matrix.content[ equationIndex][ knownCoefficientBackIndex]* knownValues[ knownCoefficientBackIndex]
			equalRowNumbers[ equationIndex]-= knownCoefficientTotal
			equalRowNumbers[ equationIndex]/= matrix.content[ equationIndex][ unknownIndex]
			knownValues.insert(
				0,
				equalRowNumbers[ equationIndex],
			)
		return knownValues

	def solveEquations(
		self,
		constants: list[ float],
	)-> list[ float]:
		return self.mutatingElimination(
			matrix= self.copy(),
			equalRowNumbers= constants.copy(),
		)


	@staticmethod
	def mutatingInversion( matrix: Self):
		"""
		Instead of duplicating work, one could make a general form converger function
		and then elimination uses the yield-(.) to modify a equality constant matrix
		inversion uses the yield-(.) to modify the identity
		"""
		if matrix.rows!= matrix.columns:
			raise ValueError( "Matrix must be square")
		size= matrix.rows
		identity= Matrix.identity( size)

		def move(
			source: int,
			sink: int,
		):
			nonlocal matrix, identity
			matrix.content.insert(
				sink,
				matrix.content.pop( source),
			)
			identity.content.insert(
				sink,
				identity.content.pop( source),
			)

		for index in range( size):
			currentPivotValue= matrix.content[ index][ index]
			if currentPivotValue== 0:
				foundRow= False
				for rowNumber in range(
					index+ 1,
					size,
				):
					currentPivotValue= matrix.content[ rowNumber][ index]
					if currentPivotValue!= 0:
						foundRow= True
						break
				if not foundRow:
					raise NotImplementedError( "Column re-order-ing is not supported")
				move(
					source= rowNumber,
					sink= index,
				)

			for rowNumber in range( size):
				if rowNumber== index:
					continue
				otherValue= matrix.content[ rowNumber][ index]
				if otherValue!= 0:
					currentRowMult= otherValue/ currentPivotValue
					for columnNumber in range( size):
						if columnNumber== index:
							matrix.content[ rowNumber][ columnNumber]= 0.0
						else:
							matrix.content[ rowNumber][ columnNumber]-= ( matrix.content[ index][ columnNumber]* currentRowMult)

						identity.content[ rowNumber][ columnNumber]-= ( identity.content[ index][ columnNumber] * currentRowMult)

		for index in range( size):
			value= matrix.content[ index][ index]
			identity.content[ index]= [
				columnValue/ value
				for columnValue in identity.content[ index]
			]
		return identity

	def getInverse( self)-> Self:
		return self.mutatingInversion(
			matrix= self.copy(),
		)

	def transpose( self)-> Self:
		return Matrix(
			content= [
				[
					self[ rowIndex][ columnIndex]
					for rowIndex in range( self.rows)
				]
				for columnIndex in range( self.columns)
			]
		)


def choose(
	choices: dict[
		Any,
		float,
	],
)-> Any:
	number= random.random()
	total= 0.0
	for choice in choices:
		total+= choices[ choice]
		if total> number:
			return choice




ProbabilityFunction= Callable[
	[
		SampleSpaceSubset,
	],
	float,
]

@dataclass
class OpenInterval:
	start: Union[
		math.inf,
		float,
	]
	end: Union[
		math.inf,
		float,
	]
	def __hash__( self):
		return hash(
			(
				self.start,
				self.end,
			),
		)

class UncountableProductSampleSpace():
	def __init__(
		self,
		base: Union[
			CountableSampleSpace,
			OpenInterval,
		],
	):
		self.base= base
		self.multipliedSpace= list()

	def multiply(
		self,
		newSpaces: list[
			Union[
				CountableSampleSpace,
				OpenInterval,
			]
		],
	):
		self.multipliedSpace+= newSpaces
	def __hash__( self):
		return hash(
			(
				self.base,
				tuple( self.multipliedSpace),
			),
		)

SampleSpace= Union[
	CountableSampleSpace,
	OpenInterval,
	UncountableProductSampleSpace,
]
EventType= Union[
	SampleSpaceSubset,
	OpenInterval,
]
SigmaAlgebra= set[ EventType]

class ProbabilitySequenceData( TypedDict):
	previousValues: list[
		list[ SpaceDataType],
	]
	sequencePosition: Union[
		int,
		None,
	]

ProbabilityMassOrDensityFunction= Callable[
	[
		SpaceDataType,
		SampleSpace,
		ProbabilitySequenceData,
	],
	float,
]

AbstractSpaceDataType= TypeVar( "AbstractSpaceDataType")
@dataclass
class RandomVariable:
# @dataclass
# class RandomVariable[ AbstractSpaceDataType]:
	abstractToRandom: Callable[
		[
			AbstractSpaceDataType,
		],
		float,
	]
	randomToAbstract: Callable[
		[
			float,
		],
		AbstractSpaceDataType,
	]
	probabilityMassFunction: Union[
		ProbabilityMassOrDensityFunction,
		None,
	]= None
	probabilityDensityFunction: Union[
		ProbabilityMassOrDensityFunction,
		None,
	]= None

RandomVariableViabilityVerifier= Callable[
	[
		ForwardRef( "ProbabilitySpace")
	],
	bool,
]

@dataclass
class SequenceialProbabilityConfiguration:
	previousValues: list[ slice]= field(
		default_factory= list,
	)
	affectedByMutatingClosure: bool= True
	affectedBySequencePosition: bool= False

class MarkovStationaryGroupType( TypedDict):
	members: set[ int]
	recurrance: bool
	period: Union[
		None,
		int,
	]

@dataclass(
	init= False,
)
class ProbabilitySpace:
	sampleSpace: CountableSampleSpace
	sigmaAlgebra: SigmaAlgebra
	probabilityMassOrDensityFunction: ProbabilityMassOrDensityFunction
	
	continuous: bool
	knownSampleSpaceRealStatus: Union[
		bool,
		None,
	]


	def __init__(
		self,
		sampleSpace: SampleSpace,
		sigmaAlgebra: SigmaAlgebra,
		probabilityMassOrDensityFunction: Union[
			ProbabilityMassOrDensityFunction,
			None,
		],
		knownSampleSpaceRealStatus: Union[
			bool,
			None,
		]= None,
		sequenceConfiguration: Union[
			SequenceialProbabilityConfiguration,
			None,
		]= None,
	):
		if sequenceConfiguration== None:
			sequenceConfiguration= SequenceialProbabilityConfiguration()
		self.continuous= isinstance(
			sampleSpace,
			(
				OpenInterval,
				UncountableProductSampleSpace,
			)
		)
		self.sampleSpace= sampleSpace
		self.sigmaAlgebra= sigmaAlgebra
		self.probabilityMassOrDensityFunction= probabilityMassOrDensityFunction
		self.knownSampleSpaceRealStatus= knownSampleSpaceRealStatus
		self.instanceDefinedRandomVariables= dict()
		self.propertys= dict()
		self.propertysUnderValidation= set()
		self.propertysUnderRetrieval= set()
		self.sequenceConfiguration= sequenceConfiguration
		self.markovStationaryClassifications= None

	class PropertyDefinitionSpace:
		class Validation:
			alwaysPresent= lambda space: True
			markovPositionUnrelated= lambda space: space.getPropertyValue( "markovian") and ( not space.getPropertyValue( "probabilityRelatedToSequencePosition"))
			markovPositionUnrelatedIndexs= lambda space: ProbabilitySpace.PropertyDefinitionSpace.Validation.markovPositionUnrelated( space) and space.getPropertyValue( "integerIndexSampleSpace")
			@staticmethod
			def irreducableMarkov( space):
				if ProbabilitySpace.PropertyDefinitionSpace.Validation.markovPositionUnrelatedIndexs( space):
					classes= space.getPropertyValue( "groups")
					if len( classes):
						return True
				return False

		class Retrieval:
			@staticmethod
			def sampleSpaceRealStatusRetrieval( space):
				spaceType= type( space.sampleSpace)
				if spaceType== OpenInterval:
					realStatus= True
				elif spaceType== UncountableProductSampleSpace:
					raise Exception
					realStatus= False
				else:
					realStatus= True
					for member in space.sampleSpace:
						if not isinstance(
							member,
							(
								int,
								float,
							),
						):
							realStatus= False
							break
				return realStatus

			integerSampleSpace= lambda space: frozenset( space.sampleSpace)== frozenset( range( len( space.sampleSpace)))

			@staticmethod
			def markovian( space):
				previousValues= space.sequenceConfiguration.previousValues
				if len( previousValues)== 0:
					return True
				else:
					if len( previousValues)== 1:
						if previousValues[ 0].start== -1:
							if previousValues[ 0].stop== None:
								return True
				return False

			@staticmethod
			def probabilityRelatedToSequencePosition( space):
				return space.sequenceConfiguration.affectedBySequencePosition or space.sequenceConfiguration.affectedByMutatingClosure

			@staticmethod
			def dependsOnSequence( space):
				return space.sequenceConfiguration.affectedBySequencePosition or ( len( space.sequenceConfiguration.previousValues)> 0)


			getTransitionProbabilities= lambda space: Matrix(
				content= [
					[
						space.probabilityMassOrDensityFunction(
							nextStateColumnIndex,
							space.sampleSpace,
							{
								"previousValues": [ [ currentStateRowIndex]] if ( len( space.sequenceConfiguration.previousValues)== 1) else [],
								"sequencePosition": None,
							},
						)
						for nextStateColumnIndex in range(
							len( space.sampleSpace)
						)
					]
					for currentStateRowIndex in range(
						len( space.sampleSpace)
					)
				]
			)

			@staticmethod
			def getAccessibilities( space):
				transitionMatrix: Matrix= space.getPropertyValue( "transitionProbabilities")
				accessibilities= dict()
				locatorDependencies= dict()
				locatorDependants= dict()
				currentChain= set()
				def ensurePrescenceAndAdd(
					dictionary: dict,
					locator,
					value,
				):
					if locator in dictionary:
						dictionary[ locator].add( value)
					else:
						dictionary[ locator]= { value,}
				def copyDependencies(
					old,
					new,
				):
					nonlocal locatorDependencies, locatorDependants
					dependencies= locatorDependencies[ old].copy()
					if new in dependencies:
						dependencies.remove( new)
					for dependency in dependencies:
						ensurePrescenceAndAdd(
							locatorDependants,
							dependency,
							new,
						)
					if new not in locatorDependencies:
						locatorDependencies[ new]= dependencies
					else:
						locatorDependencies[ new]= locatorDependencies[ new].union( dependencies)
				def elementAccessibilities( element: int)-> set[ SpaceDataType]:
					nonlocal accessibilities, locatorDependencies, locatorDependants, currentChain
					currentChain.add( element)
					accessibilities[ element]= { element,}
					# elementAccessibles= { element,}
					for connected in range( len( space.sampleSpace)):
						if connected== element:
							continue
						if transitionMatrix[ element][ connected]!= 0.0:
							if connected in currentChain:
								ensurePrescenceAndAdd(
									locatorDependencies,
									element,
									connected,
								)
								ensurePrescenceAndAdd(
									locatorDependants,
									connected,
									element,
								)
							else:
								if connected in locatorDependencies:
									accessibilities[ element]= accessibilities[ element].union( accessibilities[ connected])
									copyDependencies(
										old= connected,
										new= element,
									)
								else:
									accessibilities[ element].add( connected)
									if connected in accessibilities:
										connectedAccessibles= accessibilities[ connected]
									else:
										connectedAccessibles= elementAccessibilities( connected)
									accessibilities[ element]= accessibilities[ element].union( connectedAccessibles)
									if connected in locatorDependencies:
										copyDependencies(
											old= connected,
											new= element,
										)
									
					if element in locatorDependants:
						for dependant in locatorDependants[ element]:
							accessibilities[ dependant]= accessibilities[ dependant].union( accessibilities[ element])
							copyDependencies(
								old= element,
								new= dependant,
							)
							locatorDependencies[ dependant].remove( element)
						locatorDependants.pop( element)

					currentChain.remove( element)
					return accessibilities[ element]

				for element in space.sampleSpace:
					if element not in accessibilities:
						elementAccessibilities( element)

				return accessibilities

			@staticmethod
			def getGroups( space):
				def walkPeriod(
					start: int,
					current: int,
					group: set[ int],
					iteration: int,
					transitionMatrix: Matrix,
					encountered: set[ int],
					periods: set[ int],
				):
					iteration+= 1
					encountered.add( current)
					for ( connectedIndex, connectedValue) in enumerate( transitionMatrix[ current]):
						if connectedValue> 0.0:
							if connectedIndex in group:
								if connectedIndex== start:
									periods.add( iteration)
								else:
									if connectedIndex!= current:
										if connectedIndex not in encountered:
											walkPeriod(
												start= start,
												current= connectedIndex,
												group= group,
												iteration= iteration,
												transitionMatrix= transitionMatrix,
												encountered= encountered.copy(),
												periods= periods,
											)

				transitionMatrix= space.getPropertyValue( "transitionProbabilities")
				accessibles= space.getPropertyValue( "accessibilities")
				remaining= set( range( transitionMatrix.rows))
				taken= set()
				groups= list()
				for element in remaining:
					if element in taken:
						continue
					group= { element,}
					nonMemberContactable= False
					for accessible in accessibles[ element]:
						if element in accessibles[ accessible]:
							group.add( accessible)
							taken.add( accessible)
						else:
							nonMemberContactable= True

					recurrance= nonMemberContactable== False
					if recurrance:
						periods= set()
						walkPeriod(
							start= element,
							current= element,
							group= group,
							iteration= 0,
							transitionMatrix= transitionMatrix,
							encountered= set(),
							periods= periods,
						)
						period= greatestCommonDivisor( periods) 
					else:
						period= None
					groups.append(
						{
							"members": group,
							"recurrance": recurrance,
							"period": period,
						},
					)

				return groups

			@staticmethod
			def getTransientStatesMeanTimes( space):
				groups= space.getPropertyValue( "groups")
				transitionMatrix= space.getPropertyValue( "transitionProbabilities")
				transientStates= set()
				for group in groups:
					if group[ "recurrance"]== False:
						transientStates|= group[ "members"]
				# reduce transition matrix to transient states
				transientTransitionMatrix= Matrix(
					content= [
						[
							transitionMatrix[ rowIndex][ columnIndex]
							for columnIndex in range( transitionMatrix.columns)
							if columnIndex in transientStates
						]
						for rowIndex in range( transitionMatrix.rows)
						if rowIndex in transientStates
					]
				)
				return (
					Matrix.identity(
						size= transientTransitionMatrix.columns,
					)- transientTransitionMatrix
				).getInverse()

			@staticmethod
			def getSteadyStateTransitionProbabilities( space):
				transitionMatrix= space.getPropertyValue( "transitionProbabilities")
				equations= transitionMatrix.transpose()- Matrix.identity( transitionMatrix.rows)
				equations.content[ 0]= [ 1.0 for _ in range( equations.columns)]
				constants= [ 0.0 for _ in range( equations.rows)]
				constants[ 0]= 1.0
				steadyStateProbabilities= equations.solveEquations(
					constants= constants,
				)
				return steadyStateProbabilities

			@staticmethod
			def getReversedTransitionProbabilities( space):
				transitionMatrix= space.getPropertyValue( "transitionProbabilities")
				steadyStateProbabilities= space.getPropertyValue( "steadyStateTransitionProbabilities")

				rows= list()
				for forwardColumnIndex in range( transitionMatrix.columns):
					ownProbability= steadyStateProbabilities[ forwardColumnIndex]
					if ownProbability== 0.0:
						accumulateTotal= True
						total= 0.0
					else:
						accumulateTotal= False
					row= list()
					for forwardRowIndex in range( transitionMatrix.rows):
						transitionLikelyhood= steadyStateProbabilities[ forwardRowIndex]* transitionMatrix[ forwardRowIndex][ forwardColumnIndex]
						if accumulateTotal:
							total+= transitionLikelyhood
							row.append( transitionLikelyhood)
						else:
							row.append( transitionLikelyhood/ ownProbability)

					if accumulateTotal:
						row= list(
							map(
								lambda likelyhood: likelyhood/ total,
								row,
							)
						)
					rows.append( row)
				return Matrix(
					content= rows,
				)

		definitions= {
			"sampleSpaceRealStatus": (
				bool,
				Validation.alwaysPresent,
				Retrieval.sampleSpaceRealStatusRetrieval,
			),
			"markovian": (
				bool,
				Validation.alwaysPresent,
				Retrieval.markovian,
			),
			"probabilityRelatedToSequencePosition": (
				bool,
				Validation.alwaysPresent,
				Retrieval.probabilityRelatedToSequencePosition,
			),
			"dependsOnSequence": (
				bool,
				Validation.alwaysPresent,
				Retrieval.dependsOnSequence,
			),
			"integerIndexSampleSpace": (
				bool,
				Validation.alwaysPresent,
				Retrieval.integerSampleSpace,
			),
			"transitionProbabilities": (
				Matrix,
				Validation.markovPositionUnrelatedIndexs,
				Retrieval.getTransitionProbabilities,
			),
			"accessibilities": (
				dict[ SpaceDataType, set[ SpaceDataType]],
				Validation.markovPositionUnrelated,
				Retrieval.getAccessibilities,
			),
			"groups": (
				list[ MarkovStationaryGroupType],
				Validation.markovPositionUnrelated,
				Retrieval.getGroups,
			),
			"transientStatesMeanTimes": (
				Matrix,
				Validation.markovPositionUnrelatedIndexs,
				Retrieval.getTransientStatesMeanTimes,
			),
			"steadyStateTransitionProbabilities": (
				list[ float],
				Validation.markovPositionUnrelated,
				Retrieval.getSteadyStateTransitionProbabilities,
			),
			"reversedTransitionProbabilities": (
				Matrix,
				Validation.irreducableMarkov,
				Retrieval.getReversedTransitionProbabilities,
			),
		}
	class PropertyNotDefined( ValueError):pass
	class PropertyNotValidForInstance( ValueError):pass

	@classmethod
	def getPropertyDefinition(
		cls,
		propertyName: str,
	):
		if propertyName in cls.PropertyDefinitionSpace.definitions:
			return cls.PropertyDefinitionSpace.definitions[ propertyName]
		else:
			raise cls.PropertyNotDefined( propertyName)
	def getPropertyValue(
		self,
		propertyName: str,
	):
		if propertyName in self.propertys:
			value= self.propertys[ propertyName]
			if isinstance(
				value,
				self.PropertyNotValidForInstance,
			):
				raise value
			else:
				return value
		else:
			(
				_,
				validityFunction,
				retrievalFunction,
			)= self.getPropertyDefinition( propertyName)
			self.propertysUnderValidation.add( propertyName)
			valid= validityFunction( self)
			self.propertysUnderValidation.remove( propertyName)
			if not valid:
				error= self.PropertyNotValidForInstance( propertyName)
				self.propertys[ propertyName]= error
				raise error

			self.propertysUnderRetrieval.add( propertyName)
			value= retrievalFunction( self)
			self.propertysUnderRetrieval.remove( propertyName)
			self.propertys[ propertyName]= value
			return value

	def hasProperty(
		self,
		propertyName: str,
	)-> bool:
		if propertyName in self.propertys:
			return not isinstance(
				self.propertys[ propertyName],
				self.PropertyNotValidForInstance,
			)
		else:
			(
				_,
				validityFunction,
				_,
			)= self.getPropertyDefinition( propertyName)
			return validityFunction( self)

	def eventProbability(
		self,
		event: EventType
	)-> float:
		if self.sigmaAlgebra!= None: # allowing None due to computation constraint
			assert event in self.sigmaAlgebra
		if self.getPropertyValue( "dependsOnSequence"):
			raise Exception( "Space depends on sequence and can not be called outside of that context")

		if self.continuous:
			if not isinstance(
				event,
				OpenInterval,
			):
				# discrete points have a probability of 0.0
				return 0.0
			return integrateOverInterval(
				function= lambda location: self.probabilityMassOrDensityFunction(
					location,
					self.sampleSpace,
					{
						"previousValues": [],
						"sequencePosition": None,
					}
				),
				interval= event,
			)
		else:
			if isinstance(
				event,
				OpenInterval,
			):
				raise Exception( "Interval events for discrete probability spaces is not supported")
			sum= 0.0
			for member in event:
				sum+= self.probabilityMassOrDensityFunction(
					member,
					self.sampleSpace,
					{
						"previousValues": [],
						"sequencePosition": None,
					}
				)
			return sum

	@staticmethod
	def getProductSpace(
		spaces: Sequence[ Self],
	)-> Self:
		discreteSpaces= list()
		intervalSpaces= list()
		combinedSpaces= list()
		for space in spaces:
			if isinstance( space.sampleSpace, OpenInterval):
				intervalSpaces.append( space)
			elif isinstance( space.sampleSpace, UncountableProductSampleSpace):
				combinedSpaces.append( space)
			else:
				discreteSpaces.append( space)
		# discreteLen= len( discreteSpaces)
		intervalLen= len( intervalSpaces)
		combinedLen= len( combinedSpaces)
		if ( intervalLen+ combinedLen)== 0:
			sampleSpace= cartesianSetProduct(
				*[
					space.sampleSpace
					for space in discreteSpaces
				]
			)
			spaceLen= len( spaces)
			def w(
				currentIndex: int,
				inputSubSetSequence: tuple,
			):
				currentSpace= spaces[ currentIndex]
				nextIndex= currentIndex+ 1
				for subSet in currentSpace.sigmaAlgebra:
				# for value in currentSet:
					currentSubSetSequence= inputSubSetSequence+ ( subSet,)
					if nextIndex== spaceLen:
						yield frozenset(
							cartesianSetProduct(
								*currentSubSetSequence
							)
						)
					else:
						for result in w(
							currentIndex= nextIndex,
							inputSubSetSequence= currentSubSetSequence
						):
							yield result
			sigmaAlgebra= smallestSigmaAlgebra(
				sampleSpace= sampleSpace,
				inputSet= set(
					w(
						currentIndex= 0,
						inputSubSetSequence= tuple(),
					)
				),
			)
			# need to creat
		else:
			base= None
			multiplications= list()
			for space in spaces:
				if base== None:
					base= UncountableProductSampleSpace(
						base= space,
					)
				else:
					multiplications.append( space)
			base.multiply(
				newSpaces= multiplications,
			)
			sampleSpace= base
			# wrong i believe
			sigmaAlgebra= smallestSigmaAlgebra(
				sampleSpace= sampleSpace,
				inputSet= cartesianSetProduct(
					*[
						space.sigmaAlgebra
						for space in discreteSpaces
					]
				),
			)

		
		def f( value, sampleSpace):
			apply(
				mult,
				[
					spaces[ index].probabilityMassOrDensityFunction(
						constituantValue,
						spaces[ index].sampleSpace,
						{
							"previousValues": [],
							"sequencePosition": None,
						}
					)
					for ( index, constituantValue) in value
				]
			)
		return ProbabilitySpace(
			sampleSpace= sampleSpace,
			sigmaAlgebra= sigmaAlgebra,
			probabilityMassOrDensityFunction= lambda value, sampleSpace, sequenceData: apply(
				mult,
				[
					spaces[ index].probabilityMassOrDensityFunction(
						constituantValue,
						spaces[ index].sampleSpace,
						{
							"previousValues": [],
							"sequencePosition": None,
						}
					)
					for ( index, constituantValue) in enumerate( value)
				]
			),
		)

	def __mul__(
		self,
		other,
	):
		return self.getProductSpace(
			spaces= [
				self,
				other,
			],
		)
	
	classDefinedRandomVariables: ClassVar[
		dict[
			str,
			tuple[
				RandomVariableViabilityVerifier,
				RandomVariable,
			],
		]
	]= {
	}
	instanceDefinedRandomVariables: dict[
		str,
		RandomVariable,
	]

	def defineRandomVariable(
		self,
		name: str,
		randomVariable: RandomVariable,
	):
		self.instanceDefinedRandomVariables[ name]= randomVariable


	def getRandomVariableSampleSpace(
		self,
		name: str,
	)-> Self:
		sampleSpaceType= type( self.sampleSpace)
		randomVariable: RandomVariable= self.instanceDefinedRandomVariables[ name]
		if self.continuous:
			raise NotImplementedError( "Transformations of continuous spaces are not implemented")
		else:
			if randomVariable.probabilityMassFunction!= None:
				massFunction= randomVariable.probabilityMassFunction
			else:
				def massFunction(
					element,
					sampleSpace: SampleSpace,
					sequenceData: ProbabilitySequenceData,
				)-> float:
					return self.probabilityMassOrDensityFunction(
						randomVariable.randomToAbstract( element),
						self.sampleSpace,
						{
							"previousValues": [],
							"sequencePosition": None,
						},
					)
			space= ProbabilitySpace(
				sampleSpace= {
					randomVariable.abstractToRandom( element)
					for element in self.sampleSpace
				},
				sigmaAlgebra= {
					frozenset(
						{
							randomVariable.abstractToRandom( element)
							for element in sampleSubSet
						}
					)
					for sampleSubSet in self.sigmaAlgebra
				},
				probabilityMassOrDensityFunction= massFunction,
				knownSampleSpaceRealStatus= True,
			)

		return space

	def getExpectedValue(
		self,
		functionOnSelf: Union[
			Callable[
				[ float,],
				float
			],
			None,
		]= None
	)-> float:
		assert self.getPropertyValue( "sampleSpaceRealStatus")
		if functionOnSelf== None:
			iterationFunction= lambda value: value* self.probabilityMassOrDensityFunction(
				value,
				self.sampleSpace,
				{
					"previousValues": [],
					"sequencePosition": None,
				},
			)
		else:
			iterationFunction= lambda value: functionOnSelf( value)* self.probabilityMassOrDensityFunction(
				value,
				self.sampleSpace,
				{
					"previousValues": [],
					"sequencePosition": None,
				},
			)
		if self.continuous:
			if isinstance(
				self.sampleSpace,
				OpenInterval,
			):
				return integrateOverInterval(
					function= iterationFunction,
					interval= self.sampleSpace,
				)
			else:
				raise NotImplementedError( "Integration over arbitrary continuous domain not implemented")
		else:
			return sum(
				map(
					iterationFunction,
					self.sampleSpace,
				)
			)
	def getVariance( self)-> float:
		return self.getExpectedValue(
			functionOnSelf= lambda element: element** 2,
		)- ( self.getExpectedValue()** 2)

	def getStandardDeviation( self)-> float:
		return math.sqrt( self.getVariance())

	def conditionalProbability(
		self,
		events: Sequence,
	):
		return self.eventProbability(
			setIntersection(
				events[ 0],
				events[ 1],
			),
		)/ self.eventProbability( events[ 0])

	def testEventIndependance(
		self,
		events: Collection[ set],
	)-> bool:
		eventsLen= len( events)
		if eventsLen< 2:
			if eventsLen< 1:
				raise ValueError( "Length of events must be 1 or more")
			else:
				return True

		return math.isclose(
			self.eventProbability(
				apply(
					setIntersection,
					events,
				),
			),
			apply(
				mult,
				list(
					map(
						self.eventProbability,
						events,
					)
				),
			),
		)

		
	def getBinomialDistributionSpace(
		self,
		sampleNumber: int,
		sucsessEvent: set,
	):
		assert sampleNumber> 0
		assert not self.continuous
		sampleSpace= set(
			range( sampleNumber+ 1),
		)
		sucsessProbability= self.eventProbability( sucsessEvent)
		failureProbability= 1- sucsessProbability
		def probabilityMassFunction(
			trialNumber: int,
			sampleSpace: SampleSpace,
			sequenceData: ProbabilitySequenceData,
		):
			return chancesOfKOccurencesInNUniformTrials(
				n= sampleNumber,
				k= trialNumber,
			)* math.pow(
				sucsessProbability,
				trialNumber,
			)* math.pow(
				failureProbability,
				sampleNumber- trialNumber,
			)

		return ProbabilitySpace(
			sampleSpace= sampleSpace,
			sigmaAlgebra= smallestSigmaAlgebra(
				sampleSpace= sampleSpace,
				inputSet= {
					frozenset( { element})
					for element in sampleSpace
				},
			),
			probabilityMassOrDensityFunction= probabilityMassFunction,
			knownSampleSpaceRealStatus= True,
		)

	@classmethod
	def getHyperGeometricDistribution(
		cls,
		sampleNumber: int,
		typeNumbers: tuple[ int],
	):
		totalNumber= sum( typeNumbers)
		assert sampleNumber<= totalNumber

		setLen= len( typeNumbers)
		sets= [
			set( range( max+ 1))
			for max in typeNumbers
		]
		def w(
			currentIndex: int,
			inputTuple: tuple[ ValueType],
			total: int,
		):
			currentSet= sets[ currentIndex]
			nextIndex= currentIndex+ 1
			remaining= sampleNumber- total
			for value in currentSet:
				if value> remaining:
					continue
				currentTuple= inputTuple+ ( value,)
				if nextIndex== setLen:
					if sum( currentTuple)== sampleNumber:
						yield currentTuple
				else:
					for result in w(
						currentIndex= nextIndex,
						inputTuple= currentTuple,
						total= total+ value,
					):
						yield result

		sampleSpace= set(
			w(
				currentIndex= 0,
				inputTuple= tuple(),
				total= 0,
			),
		)
		def massFunction(
			element,
			sampleSpace,
			sequenceData,
		):
			assert sum( element)== sampleNumber
			assert len( element)== setLen
			i= 0
			for typeNumber in element:
				assert typeNumber<= typeNumbers[ i]
				i+= 1
			return apply(
				mult,
				list(
					map(
						lambda typeInfo: chancesOfKOccurencesInNUniformTrials(
							n= typeNumbers[ typeInfo[ 0]],
							k= typeInfo[ 1],
						),
						list(
							enumerate( element),
						),
					),
				)
			)/ chancesOfKOccurencesInNUniformTrials(
				n= totalNumber,
				k= sampleNumber,
			)

		return ProbabilitySpace(
			sampleSpace= sampleSpace,
			# sigmaAlgebra= smallestSigmaAlgebra(
			# 	sampleSpace= sampleSpace,
			# 	inputSet= {
			# 		frozenset( { element})
			# 		for element in sampleSpace
			# 	}
			# ),
			probabilityMassOrDensityFunction= massFunction,
			sigmaAlgebra= None, # computation issue
			knownSampleSpaceRealStatus= False,
		)
	

	# so there is backward forward or joint search mechanisms to find the probability of a single state given a known signal
	# given a sequence of signals and predicting the matching state sequence, one can optimise for the most correct states or for the most acurate whole sequence prediction
	# they both use similar concepts of forward and backward
	def getHiddenSampleSpace(
		self,
		emmisionProbabilitys: Matrix,
		emmisionRealisation: Sequence[ int],
		initialProbabilitys: list[ float]| None= None,
	):
		if initialProbabilitys== None:
			initialProbabilitys= self.getPropertyValue( "steadyStateTransitionProbabilities")
		transitionMatrix= self.getPropertyValue( "transitionProbabilities")
		sequenceLen= len( emmisionRealisation)
		eventProbabilitys= dict()
		def w(
			currentIndex: int,
			inputTuple: tuple[ ValueType],
			currentProbability: float,
		):
			nonlocal eventProbabilitys
			nextIndex= currentIndex+ 1
			for state in self.sampleSpace:
				if currentIndex== 0:
					stateProbability= initialProbabilitys[ state]
				else:
					previousState= inputTuple[ -1]
					stateProbability= transitionMatrix[ previousState][ state]
				currentTuple= inputTuple+ ( state,)
				currentStateProbability= currentProbability* stateProbability* emmisionProbabilitys[ state][ emmisionRealisation[ currentIndex]]
				if currentStateProbability!= 0.0:
					if nextIndex== sequenceLen:
						eventProbabilitys[ currentTuple]= currentStateProbability
					else:
						w(
							currentIndex= nextIndex,
							inputTuple= currentTuple,
							currentProbability= currentStateProbability,
						)

		w(
			currentIndex= 0,
			inputTuple= tuple(),
			currentProbability= 1.0,
		)
		sampleSpace= set( eventProbabilitys.keys())

		def probabilityMassFunction(
			realisation,
			sampleSpace,
			sequenceData,
		):
			if realisation in eventProbabilitys:
				return eventProbabilitys[ realisation]
			else:
				return 0.0

		return ProbabilitySpace(
			sampleSpace= sampleSpace,
			# sigmaAlgebra= smallestSigmaAlgebra(
			# 	sampleSpace= sampleSpace,
			# 	inputSet= { frozenset( sampleSpace)},
			# ),
			sigmaAlgebra= None,
			probabilityMassOrDensityFunction= probabilityMassFunction,
			knownSampleSpaceRealStatus= False,
			sequenceConfiguration= SequenceialProbabilityConfiguration(
				previousValues= [],
				affectedByMutatingClosure= False,
				affectedBySequencePosition= False,
			),
		)


	def getNStepProbability(
		self,
		initialState: SpaceDataType,
		desiredState: SpaceDataType,
		stepNumber: int= 1,
		initialSequencePosition: int= 0,
	):
		if stepNumber== 0:
			return initialState== desiredState
		else:
			if stepNumber== -1:
				transitionMatrix= self.getPropertyValue( "reversedTransitionProbabilities")
			elif stepNumber== 1:
				transitionMatrix= self.getPropertyValue( "transitionProbabilities")
			elif stepNumber> 1:
				transitionMatrix= self.getPropertyValue( "transitionProbabilities").copy()
				for _ in range( stepNumber- 1):
					transitionMatrix*= transitionMatrix
			else:
				transitionMatrix= self.getPropertyValue( "reversedTransitionProbabilities")
				for _ in range( stepNumber- 1):
					transitionMatrix*= transitionMatrix
			return transitionMatrix[ initialState][ desiredState]

	# given a start state, go forward and back, obvious with markovian, what about non?, return sequence, then process for display
	# if i was fancy, i would allow calculation of the reversed transition matrix from using n step probabilitys from some point and not just using the steady state-(.)
	def realiseSequence(
		self,
		initialState: SpaceDataType,
		initialSequencePosition: int= 0,
		forwardSteps: int= 0,
		backwardSteps: int= 0,
		includeInitial: bool= True,
		join: bool= True,
	)-> tuple[
		list[ SpaceDataType],
		list[ SpaceDataType],
		list[ SpaceDataType],
	]:
		backwards= list()
		forwards= list()
		# So return 
		if ( backwardSteps> 0):
			# only supporting backward form steady state
			groups= self.getPropertyValue( "groups")
			for group in groups:
				if initialState in group[ "members"]:
					if group[ "recurrance"]== False:
						raise ValueError( "Can not calculate backwards from a state which is not feasible in a steady state")
					break
			reversedProbabilitys= self.getPropertyValue( "reversedTransitionProbabilities")
			previous= initialState
			for _ in range( backwardSteps):
				previous= choose(
					choices= {
						index: reversedProbabilitys[ previous][ index]
						for index in range( reversedProbabilitys.columns)
					},
				)
				backwards.append( previous)
			backwards.reverse()
		if ( forwardSteps> 0):
			if self.hasProperty( "transitionProbabilities"):
				transitionMatrix= self.getPropertyValue( "transitionProbabilities")
				previous= initialState
				for _ in range( forwardSteps):
					previous= choose(
						choices= {
							index: transitionMatrix[ previous][ index]
							for index in range( transitionMatrix.columns)
						},
					)
					forwards.append( previous)
			else:
				for sequencePosition in range(
					initialSequencePosition+ 1,
					initialSequencePosition+ 1+ forwardSteps,
				):
					currentSequence= backwards+ [ initialState]+ forwards
					iterationPreviousValues= [
						currentSequence[ slicer]
						for slicer in self.sequenceConfiguration.previousValues
					]
					choices= {
						element: self.probabilityMassOrDensityFunction(
							element,
							self.sampleSpace,
							{
								"previousValues": iterationPreviousValues,
								"sequencePosition": sequencePosition if self.sequenceConfiguration.affectedBySequencePosition else None,
							},
						)
						for element in self.sampleSpace
					}
					choice= choose( choices)
					forwards.append( choice)

		middle= [ initialState] if includeInitial else []
		if join:
			return backwards+ middle+ forwards
		else:
			return (
				backwards,
				middle,
				forwards,
			)


print( "\033c")

try:
	import matplotlib.pyplot as pyplot
	pyplot.switch_backend( "QtAgg")
	plotting= True
	figure= pyplot.figure()
	currentAxis= figure.add_axes( [ 0.15, 0.1, 0.8, 0.8], visible= False)
	def addAxis():
		global currentAxis
		currentAxis.set_visible( False)
		currentAxis= figure.add_axes( [ 0.15, 0.1, 0.8, 0.8], visible= False)
		currentAxis.set_visible( True)
	pyplot.show( block= False)
	print( "A plotting window is opened, this is used to draw charts to, it will not re open if closed\n")
except (
	ModuleNotFoundError,
	ImportError,
):
	plotting= False

def br():
	print( "")
	input( "Press enter to continue:")
	if plotting:
		currentAxis.set_visible( False)
		figure.canvas.draw()
	( width, _)= os.get_terminal_size()
	print( "")
	print( "".join( [ "=" for _ in range( width)]))
	print( "")


if not plotting:
	print( "matplotlib and pyqt are not present, therefore plotting is disabled")
	br()
	print( "")


diceSampleSpace= { 1, 2, 3, 4, 5, 6}
print( "A finite discrete sample can be chosen: ", diceSampleSpace)
diceSigmaAlgebra= powerSet( diceSampleSpace)
print( "The power set can be taken as the sigma algebra: ", diceSigmaAlgebra)
print( "The probability mass function can be the uniform distribution")
diceProbabilitySpace= ProbabilitySpace(
	sampleSpace= diceSampleSpace,
	sigmaAlgebra= diceSigmaAlgebra,
	probabilityMassOrDensityFunction= lambda item, sampleSpace, sequenceData: 1/ len( sampleSpace),
)
event= { 1, 2}
print( f"The probability of the event { event} is therefore { diceProbabilitySpace.eventProbability( event)}")
br()


# lineSampleSpace= OpenInterval( -1, 1)
# lineTopology= {
# 	frozenset( { -0.3}),
# 	frozenset( { -0.2, 0.63}),
# }
# lineProbabilitySpace= ProbabilitySpace(
# 	sampleSpace= lineSampleSpace,
# 	sigmaAlgebra= smallestSigmaAlgebra(
# 		sampleSpace= lineSampleSpace,
# 		inputSet= lineTopology,
# 	),
# 	probabilityMassOrDensityFunction= lambda item, sampleSpace: 0.5,
# )

otherSampleSpace= { "q", "w"}
print( "Another finite discrete sample can be chosen: ", otherSampleSpace)
otherSigmaAlgebra= powerSet( otherSampleSpace)
print( "The power set can be taken as the sigma algebra: ", otherSigmaAlgebra)
print( "The probability mass function can also be the uniform distribution")

otherProbabilitySpace= ProbabilitySpace(
	sampleSpace= otherSampleSpace,
	sigmaAlgebra= powerSet( otherSampleSpace),
	probabilityMassOrDensityFunction= lambda item, sampleSpace, sequenceData: 1/ len( sampleSpace),
)

br()

print( "The product space can then be taken")
combinedSpace= diceProbabilitySpace* otherProbabilitySpace
print( "The sample space is therefore", combinedSpace.sampleSpace)
event= { ( 5, "q")}
print( f"The probability of the event { event} is therefore { combinedSpace.eventProbability( event)}")
event= { ( 2, "w"), ( 2, "q")}
print( f"The probability of the event { event} is therefore { combinedSpace.eventProbability( event)}")
br()

def to( element):
	return element[ 0]+ ord( element[ 1])

def fromz( element):
	for char in otherProbabilitySpace.sampleSpace:
		asciiInt= ord( char)
		complement= element- asciiInt
		if complement in diceProbabilitySpace.sampleSpace:
			return ( complement, char,)
print( "A real valued random variable can be taken on this combined space which adds the number to the ascii number of the character")
combinedSpace.defineRandomVariable(
	name= "addition",
	randomVariable= RandomVariable(
		abstractToRandom= to,
		randomToAbstract= fromz,
	),
)


randomSpace= combinedSpace.getRandomVariableSampleSpace( "addition")
print( "The random variable sample space is therefore: ", randomSpace.sampleSpace)
print( "The expected value can be calculated as: ", randomSpace.getExpectedValue())
print( "The variance can be calculated as: ", randomSpace.getVariance())
print( "The standard deviation can be calculated as: ", randomSpace.getStandardDeviation())
print( "")
multipleOf2Event= {
	number
	for number in randomSpace.sampleSpace
	if ( number% 2)== 0
}
multipleOf3Event= {
	number
	for number in randomSpace.sampleSpace
	if ( number% 3)== 0
	}
lessThanMeanEvent= {
	number
	for number in randomSpace.sampleSpace
	if number< randomSpace.getExpectedValue()
}
events= [ multipleOf2Event, lessThanMeanEvent]
print( "The conditional probability of ( the event of elements less than the expected value) given ( the event of the multiples of 2) can be found")
print( f"P( { events[ 1]} | { events[ 0]}) = ", randomSpace.conditionalProbability( events))
print( "Independance: ", randomSpace.testEventIndependance( events))
print( "")
events= [ multipleOf2Event, multipleOf3Event]
print( "The conditional probability of ( the event of the multiples of 3) given ( the event of the multiples of 2) can be found")
print( f"P( { events[ 1]} | { events[ 0]}) = ", randomSpace.conditionalProbability( events))
print( "Independance: ", randomSpace.testEventIndependance( events))

br()

independantSampleSpace= { 0, 1, 2}
independantSpace= ProbabilitySpace(
	sampleSpace= independantSampleSpace,
	sigmaAlgebra= powerSet( independantSampleSpace),
	probabilityMassOrDensityFunction= lambda element, space, sequenceData: 1/ len( space),
)
print( "The independance of events can be tested")

events= [ { 0, 1}, { 1, 2}]
print( "sample space: ", independantSampleSpace)
print( "uniform distribution")
print( "events: ", events)
print( "independance: ", independantSpace.testEventIndependance( events= events,))
print( "")


independantSampleSpace0= { 1, 2, 3, 4, 5, 6}
independantSampleSpace1= cartesianSetProduct( independantSampleSpace0, independantSampleSpace0)
qEvent= {
	( firstThrow, secondThrow)
	for ( firstThrow, secondThrow) in independantSampleSpace1
	if firstThrow== 6
}
wEvent= {
	( firstThrow, secondThrow)
	for ( firstThrow, secondThrow) in independantSampleSpace1
	if ( firstThrow+ secondThrow)== 7
}
independantSpace= ProbabilitySpace(
	sampleSpace= independantSampleSpace1,
	sigmaAlgebra= smallestSigmaAlgebra( independantSampleSpace1, { frozenset( qEvent), frozenset( wEvent), frozenset( qEvent.intersection( wEvent))}),
	probabilityMassOrDensityFunction= lambda element, space, sequenceData: 1/ len( space),
)
events= [ qEvent, wEvent]
print( "Two dice throws")
print( "sample space: ", independantSampleSpace1)
print( "uniform distribution")
print( "events, the first throw is 6 or the sum is 7: ", events)
print( "independance: ", independantSpace.testEventIndependance( events= events, ))

br()

print( "Given the space of the two dice throws, one can take the binomial distribution, where a success case is rolling the same number on both dice")
successEvent= {
	( firstThrow, secondThrow)
	for ( firstThrow, secondThrow) in independantSpace.sampleSpace
	if firstThrow== secondThrow
}
# computing the power set is not feasible here
independantSpace.sigmaAlgebra= smallestSigmaAlgebra(
	sampleSpace= independantSpace.sampleSpace,
	inputSet= { frozenset( successEvent)},
)
sampleNumber= 10
binomialSpace= independantSpace.getBinomialDistributionSpace(
	sampleNumber= sampleNumber,
	sucsessEvent= successEvent,
)

if plotting:
	addAxis()
	currentAxis.bar(
		list( range( sampleNumber+ 1)),
		[
			binomialSpace.eventProbability( { occurenceNumber})
			for occurenceNumber in range( sampleNumber+ 1)
		],
	)
	pyplot.xlabel( f"Matching dice numbers in { sampleNumber} repetitions")
	pyplot.ylabel( "Probability")
	figure.canvas.draw()
else:
	print( f"Given { sampleNumber} repetitions")
	for occurenceNumber in range( sampleNumber+ 1):
		print( f"The probability of { occurenceNumber} matching dice numbers is { binomialSpace.eventProbability( { occurenceNumber})}")



br()

# Given a sample space of types which add up to the same value
# with a withdrawal of a specific size, what is the probability of a specific withdrawal event
sampleNumber= 7
typeNumbers= ( 3, 7, 3, 6)
print( "Hyper geometric distribution")
print( f"Given a situation where { typeNumbers} different types are present and they are encountered { sampleNumber} times")
hyperGeoSpace= ProbabilitySpace.getHyperGeometricDistribution(
	sampleNumber= sampleNumber,
	typeNumbers= typeNumbers,
)
print( f"All possible withdrawals are given by: { hyperGeoSpace.sampleSpace}")
event= { ( 1, 1, 1, 4)}
print( f"The probability of the event { event} can be taken: { hyperGeoSpace.eventProbability( { ( 1, 1, 1, 4)})}")


br()

def displayMarkovSpace( space):
	print( "- A markovian probability space can be defined")
	print( "- The transition matrix can be used:")
	transitionMatrix= space.getPropertyValue( "transitionProbabilities")
	pprint( transitionMatrix.content)
	classes= space.getPropertyValue( "groups")
	print( f"\n- The classes can be found: { classes}")
	steadyStates= space.getPropertyValue( "steadyStateTransitionProbabilities")
	print( f"\n- The probability of a state occuring far enough into the future for the probability to be irrespective of the current state can be found: { steadyStates}")
	transientMeanTimes= space.getPropertyValue( "transientStatesMeanTimes")
	print( "\n- The mean number of steps that a realisation will exist in a transient state before leaving the transient class can be found:")
	pprint( transientMeanTimes.content)
	backwardsProbabilities= space.getPropertyValue( "reversedTransitionProbabilities")
	print( "\n- The matrix of probabilitys of the current state having been another state in the previous sequence step can be found:")
	pprint( backwardsProbabilities.content)

	transientState= None
	for group in classes:
		if not group[ "recurrance"]:
			transientState= list( group[ "members"])[ 0]
			break
	if transientState!= None:
		realisation= space.realiseSequence(
			initialState= transientState,
			forwardSteps= 12,
			backwardSteps= 0,
			join= False,
		)
		print( f"\n- The sequence can be realised into the future when starting from a transient state { transientState}:")
		print( realisation)
	for group in classes:
		if group[ "recurrance"]:
			recurrantState= list( group[ "members"])[ 0]
			break
	realisation= space.realiseSequence(
		initialState= recurrantState,
		forwardSteps= 12,
		backwardSteps= 7,
		join= False,
	)
	print( f"\n- The sequence can be realised into the future and past when starting from a recurrant state { recurrantState}:")
	print( realisation)

	if plotting:
		print( "\nA realisation is plotted")
		addAxis()
		steps= 100
		realisation= space.realiseSequence(
			initialState= 0,
			forwardSteps= steps,
			backwardSteps= 0,
			join= True,
		)
		currentAxis.plot(
			list( range( steps+ 1)),
			realisation,
		)
		currentAxis.grid(
			axis= "y",
		)
		pyplot.xlabel( "Sequence position")
		pyplot.ylabel( "State")
		figure.canvas.draw()

testSampleSpace= { 0, 1, 2, 3, 4, 5, 6}
testTransitionMatrix= Matrix(
	content= [
		[ 0.0, 0.3, 0.0, 0.7, 0.0, 0.0, 0.0],
		[ 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		[ 0.0, 0.2, 0.0, 0.0, 0.4, 0.4, 0.0],
		[ 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
		[ 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
		[ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0],
		# [ 0.8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2],
		[ 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
		# [ 0.0, 0.0, 0.0, 0.3, 0.5, 0.0, 0.0],
	]
)
test= ProbabilitySpace(
	sampleSpace= testSampleSpace,
	sigmaAlgebra= powerSet( testSampleSpace),
	probabilityMassOrDensityFunction= lambda element, sampleSpace, sequenceData: testTransitionMatrix[ sequenceData[ "previousValues"][ 0][ 0]][ element],
	sequenceConfiguration= SequenceialProbabilityConfiguration(
		previousValues= [ slice( -1, None)],
		affectedByMutatingClosure= False,
		affectedBySequencePosition= False,
	)
)
displayMarkovSpace( test)

br()

print( "\nagain\n")
testSampleSpace= { 0, 1, 2, 3}
testTransitionMatrix= Matrix(
	content= [
		[ 0.08, 0.632, 0.264, 0.08],
		[ 0.184, 0.368, 0.368, 0.184],
		[ 0.368, 0.0, 0.368, 0.368],
		[ 0.368, 0.0, 0.0, 0.368],
	]
).transpose()
test= ProbabilitySpace(
	sampleSpace= testSampleSpace,
	sigmaAlgebra= powerSet( testSampleSpace),
	probabilityMassOrDensityFunction= lambda element, sampleSpace, sequenceData: testTransitionMatrix[ sequenceData[ "previousValues"][ 0][ 0]][ element],
	sequenceConfiguration= SequenceialProbabilityConfiguration(
		previousValues= [ slice( -1, None)],
		affectedByMutatingClosure= False,
		affectedBySequencePosition= False,
	)
)

displayMarkovSpace( test)

if plotting:
	br()
	print( "A random walk realisation is plotted")
	addAxis()
	walkLength= 1000
	walkSpace= set(
		range(
			walkLength* -1,
			walkLength+ 1,
		)
	)
	def walkMassFunction(
		element,
		sampleSpace,
		sequenceData,
	):
		if len( sequenceData[ "previousValues"][ 0])== 0:
			previous= 0
		else:
			previous= sequenceData[ "previousValues"][ 0][ 0]
		if abs( element- previous)== 1:
			return 0.5
		else:
			return 0.0
	randomWalk= ProbabilitySpace(
		sampleSpace= walkSpace,
		sigmaAlgebra= { frozenset( {}), frozenset( walkSpace)},
		probabilityMassOrDensityFunction= walkMassFunction,
		sequenceConfiguration= SequenceialProbabilityConfiguration(
			previousValues= [ slice( -1, None)],
			affectedByMutatingClosure= False,
			affectedBySequencePosition= False,
		)
	)
	currentAxis.plot(
		list( range( walkLength)),
		randomWalk.realiseSequence(
			initialState= 0,
			forwardSteps= walkLength,
			includeInitial= False,
		)
	)
	pyplot.xlabel( "Sequence position")
	pyplot.ylabel( "State")
	figure.canvas.draw()

br()


testSampleSpace= { 0, 1, 2, 3, 4, 5, 6}
testTransitionMatrix= Matrix(
	content= [
		[ 0.0, 0.3, 0.0, 0.7, 0.0, 0.0, 0.0],
		[ 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		[ 0.0, 0.2, 0.0, 0.0, 0.4, 0.4, 0.0],
		[ 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
		[ 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
		[ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0],
		# [ 0.8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2],
		[ 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
		# [ 0.0, 0.0, 0.0, 0.3, 0.5, 0.0, 0.0],
	]
)
test= ProbabilitySpace(
	sampleSpace= testSampleSpace,
	sigmaAlgebra= powerSet( testSampleSpace),
	probabilityMassOrDensityFunction= lambda element, sampleSpace, sequenceData: testTransitionMatrix[ sequenceData[ "previousValues"][ 0][ 0]][ element],
	sequenceConfiguration= SequenceialProbabilityConfiguration(
		previousValues= [ slice( -1, None)],
		affectedByMutatingClosure= False,
		affectedBySequencePosition= False,
	)
)
emmissionMatrix= Matrix(
	content= [
		[ 0.7, 0.3, 0.0],
		[ 0.2, 0.3, 0.5],
		[ 0.1, 0.0, 0.9],
		[ 0.0, 0.7, 0.3],
		[ 0.7, 0.1, 0.2],
		[ 0.6, 0.2, 0.2],
		[ 0.7, 0.1, 0.2],
	],
)
emmissions= [ 2, 1, 1, 2, 2, 2, 1, 0, 0, 0, 0, 1]
steadyStateProbabilitys= test.getPropertyValue( "steadyStateTransitionProbabilities")
steadyHiddenSpace= test.getHiddenSampleSpace(
	emmisionProbabilitys= emmissionMatrix,
	emmisionRealisation= emmissions,
	initialProbabilitys= steadyStateProbabilitys,
)
steadyOrdered= list( steadyHiddenSpace.sampleSpace)
steadyOrdered.sort(
	key= lambda sequence: steadyHiddenSpace.probabilityMassOrDensityFunction( sequence, None, None),
	reverse= True,
)
equalStateProbabilitys= [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
equalHiddenSpace= test.getHiddenSampleSpace(
	emmisionProbabilitys= emmissionMatrix,
	emmisionRealisation= emmissions,
	initialProbabilitys= equalStateProbabilitys,
)
equalOrdered= list( equalHiddenSpace.sampleSpace)
equalOrdered.sort(
	key= lambda sequence: equalHiddenSpace.probabilityMassOrDensityFunction( sequence, None, None),
	reverse= True,
)
print( "Given a markovian sequence with known transitions:")
pprint( testTransitionMatrix.content)
print( "\nAnd known chances of emmission:")
pprint( emmissionMatrix.content)
print( "\nAnd a sequence of emmissions:")
print( emmissions)
print( "\nUsing the steady state probabilitys:")
print( steadyStateProbabilitys)
print( "\nOne can construct a probability space of possible state sequence-s:")
print( steadyHiddenSpace.sampleSpace)
print( "\nAnd therefore determine order of likelyhood from most likely to least likely:")
print( steadyOrdered)

br()

print( "\nUsing the state probabilitys:")
print( equalStateProbabilitys)
print( "\nOne can construct a probability space of possible state sequence-s:")
print( equalHiddenSpace.sampleSpace)
print( "\nAnd therefore determine order of likelyhood from most likely to least likely:")
print( equalOrdered)

br()

if plotting:
	pyplot.close( figure)




