"""
`optimise` is the point of entry for linear programming
this calls `optimiseSystem`
`stochasticProgram` generates input for `optimise`
"""
import time
import pprint
from enum import IntFlag, auto
from dataclasses import dataclass
from typing import(
	Any,
	TypedDict,
	Union,
)
from collections.abc import(
	Sequence,
	Collection,
)

@dataclass
class Constraint:
	class Type( IntFlag):
		LTE= auto()
		GTE= auto()
		EQ= auto()

	type: Type
	coefficients: list[ float]
	constant: float
	def copy(
		self,
	):
		return Constraint(
			type= self.type,
			coefficients= self.coefficients.copy(),
			constant= self.constant,
		)


class OptimisationResponseType( IntFlag):
	SOLVED= auto()
	UNCONSTRAINED= auto()
	NO_FEASIBLE_SOLUTION= auto()

class SystemState( TypedDict):
	function: Constraint
	constraints: dict[
		int,
		Constraint,
	]

class OptimisationResponse0( TypedDict):
	type: OptimisationResponseType
	system: Union[
		SystemState,
		None,
	]
	detectedUnconstrainedVariableIndex: Union[
		None,
		int,
	]
class OptimisationResponse1( OptimisationResponse0):
	variableValues: list[ float]| None

class OptimisationRequest( IntFlag):
	MAXIMISE= auto()
	MINIMISE= auto()

def optimise(
	optimisationRequest: OptimisationRequest,
	optimisedFunctionCoefficients: list[ float],
	constraints: list[ Constraint],
)-> OptimisationResponse1:
	if optimisationRequest== OptimisationRequest.MINIMISE:
		optimisedFunctionCoefficients= [
			coefficient* -1
			for coefficient in optimisedFunctionCoefficients
		]

	# ensure all coefficient lengths match
	# deduce variable size
	inputVariableLength= len( optimisedFunctionCoefficients)
	for constraint in constraints:
		if len( constraint.coefficients)!= inputVariableLength:
			raise Exception( "Co-efficient length of constraints does not match")
	
	newConstraints= dict()
	newVariablesRequiringInitialSolution: list[ int]= list()
	totalVariableLength= inputVariableLength
	for constraint in constraints:
		existingLen= len( constraint.coefficients)
		variableLengthDifference= totalVariableLength- existingLen
		basicVariableIndex= totalVariableLength
		if constraint.type== Constraint.Type.EQ:
			newCoefficients= [ 1.0,]
			newVariablesRequiringInitialSolution.append( basicVariableIndex)
		elif constraint.type== Constraint.Type.LTE:
			newCoefficients= [ 1.0,]
		elif constraint.type== Constraint.Type.GTE:
			newCoefficients= [ -1.0, 1.0,]
			basicVariableIndex+= 1
			newVariablesRequiringInitialSolution.append( basicVariableIndex)
		else:
			raise NotImplementedError
		newConstraints[ basicVariableIndex]= Constraint(
			type= Constraint.Type.EQ,
			# pad any missing new coefficienct as 0.0
			coefficients= constraint.coefficients.copy()+ [ 0.0 for _ in range( variableLengthDifference)]+ newCoefficients,
			constant= constraint.constant,
		)
		totalVariableLength+= len( newCoefficients)
	# pad any new constraint-s
	for basicVariable in newConstraints:
		newConstraints[ basicVariable].coefficients+= [ 0.0 for _ in range( totalVariableLength- len( newConstraints[ basicVariable].coefficients))]

	optimisedFunction= Constraint(
		type= Constraint.Type.EQ,
		coefficients= [
			functionCoefficient* -1 # when subtracting variables from equation they are inverted
			for functionCoefficient in optimisedFunctionCoefficients
		]+ [
			0.0
			for _ in range( totalVariableLength- len( optimisedFunctionCoefficients))
		],
		constant= 0.0,
	)

	if len( newVariablesRequiringInitialSolution)> 0:
		# Solve for initial solution
		solveVariablesFunction= optimisedFunction.copy()
		# set all coefficients but the ones solved for in the function to 0
		solveVariablesSet= set( newVariablesRequiringInitialSolution)
		solveVariablesFunction.coefficients= [
			1.0 if index in solveVariablesSet else 0.0
			for ( index, _) in enumerate( solveVariablesFunction.coefficients)
		]

		for solveVariableIndex in newVariablesRequiringInitialSolution:
			constraint= newConstraints[ solveVariableIndex]
			solveVariablesFunction.constant-= constraint.constant
			solveVariablesFunction.coefficients= [
				solveVariablesFunction.coefficients[ coefficientIndex]- constraint.coefficients[ coefficientIndex]
				for coefficientIndex in range( len( solveVariablesFunction.coefficients))
			]

		initialFeasibleOptimisationResult= optimiseSystem(
			system= {
				"function": solveVariablesFunction,
				"constraints": newConstraints,
			},
		)
		for solveVariableIndex in newVariablesRequiringInitialSolution:
			if solveVariableIndex in initialFeasibleOptimisationResult[ "system"][ "constraints"]:
				# if initialFeasibleOptimisationResult[ "system"][ "constraints"][ solveVariableIndex]> 0:
				raise Exception( "UNSURE IF ALL ARTIFICIAL SHOULD BE UN PRESENT")
				return{
					"type": OptimisationResponseType.NO_FEASIBLE_SOLUTION,
					"system": None,
					"detectedUnconstrainedVariableIndex": None,
				}

		initialFeasibleSystem= initialFeasibleOptimisationResult[ "system"]
					
		for solveVariableIndex in reversed( sorted( newVariablesRequiringInitialSolution)):
			for basicVariableIndex in initialFeasibleSystem[ "constraints"]:
				initialFeasibleSystem[ "constraints"][ basicVariableIndex].coefficients.pop( solveVariableIndex)
			optimisedFunction.coefficients.pop( solveVariableIndex)

		# eliminate the input variable coefficients on the optimisation function by subtracting ( the corrosponding coefficient row for the variable* the size of the existing value)
		for inputFunctionCoefficientIndex in range( len( optimisedFunctionCoefficients)):
			if inputFunctionCoefficientIndex not in initialFeasibleSystem[ "constraints"]:
				continue
				raise Exception( "UNSURE IF THIS IS REQUIREMENT")
			if optimisedFunction.coefficients[ inputFunctionCoefficientIndex]!= 0:
				# find corrosponding equation
				eliminationEquation= initialFeasibleSystem[ "constraints"][ inputFunctionCoefficientIndex]
				multNumber= optimisedFunction.coefficients[ inputFunctionCoefficientIndex]
				# corrosponding value for row should be 1 to comply with elimination
				for coefficientIndex in range( len( optimisedFunction.coefficients)):
					if coefficientIndex== inputFunctionCoefficientIndex:
						continue
					optimisedFunction.coefficients[ coefficientIndex]-= ( eliminationEquation.coefficients[ coefficientIndex]* multNumber)
				optimisedFunction.constant-= ( eliminationEquation.constant* multNumber)

				optimisedFunction.coefficients[ inputFunctionCoefficientIndex]= 0.0

		optimisationResponse= optimiseSystem(
			system= {
				"function": optimisedFunction,
				"constraints": initialFeasibleSystem[ "constraints"],
			}
		)

	else:
		optimisationResponse= optimiseSystem(
			system= {
				"function": optimisedFunction,
				"constraints": newConstraints,
			},
		)
	if optimisationResponse[ "system"]!= None:
		optimisationResponse[ "variableValues"]= [
			optimisationResponse[ "system"][ "constraints"][ variableNumber].constant if variableNumber in optimisationResponse[ "system"][ "constraints"] else 0.0
			for variableNumber in range( inputVariableLength)
		]
	else:
		optimisationResponse[ "variableValues"]= None
	return optimisationResponse



def removeConstraint(
	removeFrom: Constraint,
	remove: Constraint,
	index: int,
):
	"""
	mutates "removeFrom" constraint
	find ( constraint value of index* -1) in current constraint and add the coefficients of the "remove" constraint to the coefficients of the "removeFrom" constraint
	"""
	pivotValue= removeFrom.coefficients[ index]
	if pivotValue== 0:
		return

	pivotValue*= -1
	for coefficientIndex in range( len( removeFrom.coefficients)):
		if coefficientIndex!= index:
			if remove.coefficients[ coefficientIndex]!= 0.0:
				removeFrom.coefficients[ coefficientIndex]+= remove.coefficients[ coefficientIndex]* pivotValue
		else:
			removeFrom.coefficients[ coefficientIndex]= 0.0

	if remove.constant!= 0.0:
		removeFrom.constant+= remove.constant* pivotValue

def optimiseSystem(
	system: SystemState,
)-> OptimisationResponse0:
	while True:
		# Check for optimality
		# The variable with the smallest maximisation coefficient will be the most optimal to alter
		smallestCentralFunctionCoefficient= 0.0
		newlyBasicCoefficientIndex= -1
		for ( optimisationCoefficientIndex, optimisationCoefficient) in enumerate( system[ "function"].coefficients):
			if optimisationCoefficient< smallestCentralFunctionCoefficient:
				smallestCentralFunctionCoefficient= optimisationCoefficient
				newlyBasicCoefficientIndex= optimisationCoefficientIndex

		if smallestCentralFunctionCoefficient>= 0.0:
			return{
				"type": OptimisationResponseType.SOLVED,
				"system": system,
				"detectedUnconstrainedVariableIndex": None,
			}
		# ties for smallest are broken arbitrarily
	
		# Transfer the state to the next most optimal polygon corner

		# Find which variable to increase
		# for each constraint, if the coefficient of the index of the newly basic index is above 0 then compare that to the value of the constant to find the ratio
		# the one with the smallest ratio, it~s basic variable is newlyNonBasic
		minimumConstraintBasicVariableIndex= None
		first= True
		coefficientToConstantRatio= 0.0
		for basicVariableIndex in system[ "constraints"]:
			constraint= system[ "constraints"][ basicVariableIndex]
			if constraint.coefficients[ newlyBasicCoefficientIndex]> 0:
				coefficientToConstantRatio= constraint.constant/ constraint.coefficients[ newlyBasicCoefficientIndex]
				if first:
					first= False
					smallestCoefficientToConstantRatio= coefficientToConstantRatio
					minimumConstraintBasicVariableIndex= basicVariableIndex
				else:
					if coefficientToConstantRatio< smallestCoefficientToConstantRatio:
						smallestCoefficientToConstantRatio= coefficientToConstantRatio
						minimumConstraintBasicVariableIndex= basicVariableIndex
		if first:
			return{
				"type": OptimisationResponseType.UNCONSTRAINED,
				"system": system,
				"detectedUnconstrainedVariableIndex": newlyBasicCoefficientIndex,
			}
		# ties for smallest ratio are broken arbitrarily

		# ensure the constraint associated with the newly non basic variable has a coefficient of 1 for the newly basic variable
		# minimumConstraint= system[ "constraints"][ minimumConstraintIndex][ 1]
		minimumConstraint= system[ "constraints"][ minimumConstraintBasicVariableIndex]
		pivotCoefficient= minimumConstraint.coefficients[ newlyBasicCoefficientIndex]
		if pivotCoefficient!= 1.0:
			for coefficientIndex in range( len( minimumConstraint.coefficients)):
				if coefficientIndex!= newlyBasicCoefficientIndex:
					minimumConstraint.coefficients[ coefficientIndex]/= pivotCoefficient
			minimumConstraint.constant/= pivotCoefficient
			minimumConstraint.coefficients[ newlyBasicCoefficientIndex]= 1.0

		# ensure that every other constraint in the system has a coefficient of 0
		# achieve this by adding the constraint row associated with the newly non basic variable
		removeConstraint(
			removeFrom= system[ "function"],
			remove= minimumConstraint,
			index= newlyBasicCoefficientIndex,
		)
		for basicVariableIndex in system[ "constraints"]:
			if basicVariableIndex== minimumConstraintBasicVariableIndex:
				continue
			removeConstraint(
				removeFrom= system[ "constraints"][ basicVariableIndex],
				remove= minimumConstraint,
				index= newlyBasicCoefficientIndex,
			)

		# associate the pivot row with the newly basic variable
		system[ "constraints"][ newlyBasicCoefficientIndex]= system[ "constraints"].pop( minimumConstraintBasicVariableIndex)


def printSystem(
	optimisationRequest,
	optimisedFunctionCoefficients,
	constraints,
):
	def numberString( number):
		result= str( number)
		if len( result)> 4:
			try:
				index= result.index( ".")
				return result[ : index]
			except:
				return result
		else:
			return result
	print( optimisationRequest.name)
	indexLine= ""
	functionLine= ""
	constraintLines= [ "" for _ in constraints]
	first= True
	for index in range( len( optimisedFunctionCoefficients)):
		if first:
			first= False
			sep= ""
		else:
			sep= " | "
		indexString= numberString( index)
		funcString= numberString( optimisedFunctionCoefficients[ index])
		constraintStrings= [ numberString( constraint.coefficients[ index]) for constraint in constraints]
		maxLen= max(
			map(
				lambda item: len( item),
				constraintStrings+ [ funcString, indexString],
			)
		)
		indexLine+= sep+ indexString.ljust( maxLen)
		functionLine+= sep+ funcString.ljust( maxLen)
		for constraintIndex in range( len( constraints)):
			constraintLines[ constraintIndex]+= sep+ constraintStrings[ constraintIndex].ljust( maxLen)

	equalitySymbols= {
		Constraint.Type.EQ: "=",
		Constraint.Type.GTE: ">=",
		Constraint.Type.LTE: "<=",
	}
	maxSymbolLen= max(
		map(
			lambda item: len( item),
			equalitySymbols.values(),
		)
	)
	for constraintIndex in range( len( constraints)):
		constraint= constraints[ constraintIndex]
		constraintLines[ constraintIndex]+= f" { equalitySymbols[ constraint.type].ljust( maxSymbolLen)} "+ str( constraint.constant)

	
	print( "INDEXS:")
	print( indexLine)
	print( "FUNCTION COEFFICIENTS:")
	print( functionLine)
	print( "CONSTRAINTS:")
	print( "\n".join( constraintLines))
		


print( "\033c")
def br():
	input( "Press enter to continue:")


wydnorGlass= {
	"optimisationRequest": OptimisationRequest.MAXIMISE,
	"optimisedFunctionCoefficients": [ 3.0, 5.0],
	"constraints": [
		Constraint(
			type= Constraint.Type.LTE,
			coefficients= [ 1.0, 0.0],
			constant= 4,
		),
		Constraint(
			type= Constraint.Type.LTE,
			coefficients= [ 0.0, 2.0],
			constant= 12,
		),
		Constraint(
			type= Constraint.Type.LTE,
			coefficients= [ 3.0, 2.0],
			constant= 18,
		),
	],
}
print( "First optimise the wyndsor glass example from the textbook by Hillier and Lieberman: Introduction to operations research:")
print( "")
printSystem( **wydnorGlass)
br()
result= optimise( **wydnorGlass)
print( "\nVARIABLE VALUES:")
print( result[ "variableValues"])

br()

radiationTherapy= {
	"optimisationRequest": OptimisationRequest.MINIMISE,
	"optimisedFunctionCoefficients": [ 0.4, 0.5],
	"constraints": [
		Constraint(
			type= Constraint.Type.LTE,
			coefficients= [ 0.3, 0.1],
			constant= 2.7,
		),
		Constraint(
			type= Constraint.Type.EQ,
			coefficients= [ 0.5, 0.5],
			constant= 6,
		),
		Constraint(
			type= Constraint.Type.GTE,
			coefficients= [ 0.6, 0.4],
			constant= 6,
		),
	],
}
print( "\n\n")
print( "Then optimise the radiation therapy example from the same book:")
print( "")
printSystem( **radiationTherapy)
br()
result= optimise( **radiationTherapy)
print( "\nVARIABLE VALUES:")
print( result[ "variableValues"])

br()


ConstraintIndex= int
StageVariableIndex= int
class Possibility( TypedDict):
	probabilityWeight: float
	constraintValues= Sequence[
		Sequence[ float], # as long as the stage variables
	] # as long as constraints

class Stage( TypedDict):
	possibilities: Collection[ Possibility]
	optimisationCoefficientsOfVariables: Sequence[ float]

def stochasticProgram(
	stages: Sequence[ Stage],
	constraints: Sequence[
		tuple[
			Constraint.Type,
			float,
		],
	],
):
	optimisedFunctionCoefficients= list()
	constraintCoefficients= [
		[ [ ]]
		for _ in range( len( constraints))
	]
	stageLen= len( stages)
	maxStageIndex= stageLen- 1
	def w(
		stageIndex: int,
		currentOptimisationFunctionMultiplier: float,
		currentConstraintRowIndex: int,
	):
		nonlocal optimisedFunctionCoefficients, constraintCoefficients
		nextStageIndex= stageIndex+ 1
		stage= stages[ stageIndex]
		stageVariableLen= len( stage[ "optimisationCoefficientsOfVariables"])
		blankPossibilityEntrys= [ 0.0 for _ in range( stageVariableLen)]

		optimisedFunctionCoefficients+= [
			opCoef* currentOptimisationFunctionMultiplier
			for opCoef in stage[ "optimisationCoefficientsOfVariables"]
		]
		
		possibilities= stage[ "possibilities"]
		possibilityLen= len( possibilities)
		totalProbabilityWeight= sum(
			map(
				lambda possibility: possibility[ "probabilityWeight"],
				possibilities,
			),
		)
		if possibilityLen> 1:
			addedRows= possibilityLen- 1
			for _ in range( addedRows):
				for row in constraintCoefficients:
					row.insert(
						currentConstraintRowIndex,
						row[ currentConstraintRowIndex].copy()
					)
		else:
			addedRows= 0

		for possibilityNumber in range( possibilityLen):
			
			for constraintNumber in range( len( constraintCoefficients)):
				constraintCoefficients[ constraintNumber][ currentConstraintRowIndex+ possibilityNumber]+= list( possibilities[ possibilityNumber][ "constraintValues"][ constraintNumber])
		blankSubsequentStageCoefficients= None
		previousConstraintRowIndexs= list()
		for possibility in possibilities:

			relativeProbability= possibility[ "probabilityWeight"]/ totalProbabilityWeight
			if blankSubsequentStageCoefficients== None:
				beforeIndexLength= len( constraintCoefficients[ 0][ currentConstraintRowIndex])
			if nextStageIndex<= maxStageIndex:
				nextAddedRows= w(
					stageIndex= nextStageIndex,
					currentOptimisationFunctionMultiplier= currentOptimisationFunctionMultiplier* relativeProbability,
					currentConstraintRowIndex= currentConstraintRowIndex,
				)
			else:
				nextAddedRows= 0
			if blankSubsequentStageCoefficients== None:
				subsequentStageCoefficientLength= len( constraintCoefficients[ 0][ currentConstraintRowIndex])- beforeIndexLength
				blankSubsequentStageCoefficients= [ 0.0 for _ in range( subsequentStageCoefficientLength)]
			nextConstraintRowIndex= currentConstraintRowIndex+ ( 1+ nextAddedRows)
			for nonCurrentConstraintRowIndex in previousConstraintRowIndexs+ list(
				range(
					nextConstraintRowIndex,
					nextConstraintRowIndex+ ( ( possibilityLen- 1)- len( previousConstraintRowIndexs))
				),
			):
				for constraintNumber in range( len( constraintCoefficients)):
					constraintCoefficients[ constraintNumber][ nonCurrentConstraintRowIndex]+= blankSubsequentStageCoefficients.copy()#blankPossibilityEntrys.copy()
			
			previousConstraintRowIndexs.append( currentConstraintRowIndex)
			currentConstraintRowIndex= nextConstraintRowIndex

		return addedRows


	w(
		stageIndex= 0,
		currentOptimisationFunctionMultiplier= 1.0,
		currentConstraintRowIndex= 0,
	)

	return {
		"optimisedFunctionCoefficients": optimisedFunctionCoefficients,
		"constraints": [
			Constraint(
				type= constraintType,
				coefficients= coefficientRow,
				constant= constraintConstant,
			)
			for ( constraintNumber, ( constraintType, constraintConstant)) in enumerate( constraints)
			for coefficientRow in constraintCoefficients[ constraintNumber]
		],
	}



print( "\n\nThen obtain a linear programming input from the following problem, in which there is a single stage of uncertainty")
print( "This example is taken from the following video: https://www.youtube.com/watch?v=mBxnwpsifjc, but also supports arbitrary stages of uncertainty")
print( "One must decide what crops to plant before knowing what the yields will be")
print( "One must then decide how much of each crop to sell and how much to buy in order to feed the cows")
print( "So we therefore have a decision variable for the distribution of the crops, and then multiple decision variables for the purchasing and selling of each crop depending on which yield event actually occured")
br()
print( "")

optimisationInput= stochasticProgram(
	stages= [
		{
			"possibilities": [
				{
					"probabilityWeight": 1,
					"constraintValues": [
						( 1, 1, 1,),
						( 3, 0, 0,),
						( 0, 3.6, 0,),
						( 0, 0, -24,),
						( 0, 0, 0,),
					],
				},
				{
					"probabilityWeight": 1,
					"constraintValues": [
						( 1, 1, 1,),
						( 2.5, 0, 0,),
						( 0, 3, 0,),
						( 0, 0, -20,),
						( 0, 0, 0,),
					],
				},
				{
					"probabilityWeight": 1,
					"constraintValues": [
						( 1, 1, 1,),
						( 2, 0, 0,),
						( 0, 2.4, 0,),
						( 0, 0, -16,),
						( 0, 0, 0,),
					],
				},
			],
			"optimisationCoefficientsOfVariables": ( 150, 230, 260,),
		},
		{
			"possibilities": [
				{
					"probabilityWeight": 1,
					"constraintValues": [
						( 0, 0, 0, 0, 0, 0,),
						( 1, 0, -1, 0, 0, 0,),
						( 0, 1, 0, -1, 0, 0,),
						( 0, 0, 0, 0, 1, 1,),
						( 0, 0, 0, 0, 1, 0,),
					],
				},
			],
			"optimisationCoefficientsOfVariables": ( 238, 210, -170, -150, -36, -10),
		},
	],
	constraints= [
		( Constraint.Type.LTE, 500,),
		( Constraint.Type.GTE, 200,),
		( Constraint.Type.GTE, 240,),
		( Constraint.Type.LTE, 0,),
		( Constraint.Type.LTE, 6000,),
	]
)

optimisationInput[ "optimisationRequest"]= OptimisationRequest.MINIMISE
print( "The resulting optimisation problem is:")
printSystem( **optimisationInput)

br()
print( "")
print( "The resulting variable inputs are:")
result= optimise(
	**optimisationInput,
)

print( result[ "variableValues"])
print( "")
print( "Therefore the decider can now know the optimal crops to plant given the likelyhood of each yield scenario, and can also know ahead of time what the most optimal decisions are ")


