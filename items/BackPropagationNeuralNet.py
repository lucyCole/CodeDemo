"""
A feed forward back propagation neural net creator

Generate the training data from boolan ops
create the network with random values
loop until trained
"""


from pprint import pprint
from dataclasses import dataclass, field
from typing import Callable, ForwardRef, NewType, ClassVar, Union, List, Tuple
from math import exp
import random

class Globals:
	trainingData: list[ tuple[ tuple[ int, int, int], tuple[ int]]]= list()
	momentum: float= 0.9
	trainingRate: float= 0.5
	weightInitialisationRange: Tuple[ float, float]= ( -1, 1)
	weightInitialisationRandomSeed: float= 0.0
	acceptableErrorThreshold: float= 0.001
	hiddenLayersQuantity: int= 2
	neuronsInEachHiddenLayerQuantity: int= 8

# Define gates for training data generation
print( "\033c")
input( "Press enter to see the training data:")
print( "\n")
logicGates= {
	"AND": lambda a, b: ( a== True) and ( b== True),
	"OR": lambda a, b: ( a== True) or ( b== True),  
	"XOR": lambda a, b: a!= b,
	"NOR": lambda a, b: ( not a) and ( not b),
	# "NAND": lambda a, b: not ( ( a== True) and ( b== True)),
	# "XNOR": lambda a, b: a== b,
}
logicGateNamesOrdered= tuple( logicGates.keys())
logicGateValuesOrdered= tuple( logicGates.values())

# Generate training data

for index, logicGate in enumerate( logicGateValuesOrdered):
	for a in range( 2):
		for b in range( 2):
			Globals.trainingData.append( ( ( index, a, b), ( int( logicGate( a, b)), )))

pprint( Globals.trainingData)
print( "")
print( "Different logic gates are being trained")
print( "This is in the format: ( ( gateType, a, b), ( expectedResult))")
# alternateTrainingSet= [
# 	((2.7810836,2.550537003),( 1, 0)),
# 	((1.465489372,2.362125076),(1, 0)),
# 	((3.396561688,4.400293529),(1, 0)),
# 	((1.38807019,1.850220317),(1, 0)),
# 	((3.06407232,3.005305973),(1, 0)),
# 	((7.627531214,2.759262235),( 0, 1)),
# 	((5.332441248,2.088626775),( 0, 1)),
# 	((6.922596716,1.77106367),( 0, 1)),
# 	((8.675418651,-0.242068655),( 0, 1)),
# 	((7.673756466,3.508563011),( 0, 1))
# ]
# Globals.trainingData= alternateTrainingSet

@dataclass
class Weight():
	fromNeuron: ForwardRef( "Neuron")
	toNeuron: ForwardRef( "Neuron")
	weightValue: float
	previousWeightDelta: float= field( default= 0, init= False)
	prevChange: Union[ float, None]= field( default= None, init= False)

WeightLayer= NewType( "WeightLayer", list[ Weight])

def sigmoidFunction( neuron, activationEnergy: float)-> float:
	return 1/ ( 1+ exp( -1* activationEnergy))

def derivedSigmoidFunction( neuron, activationEnergy: float)-> float:
	return activationEnergy* ( 1- activationEnergy)

@dataclass
class Neuron():
	activationFunction: ClassVar[ Callable[ [ float], float]]= sigmoidFunction
	derivedActivationFunction: ClassVar[ Callable[ [ float], float]]= derivedSigmoidFunction

	threshold: float # Apparently the same as bias, 
	netInput: float= field( default= 0.0, init= False)
	error: float= field( default= 0.0, init= False)
	calculatedOutput: Union[ float, None]= field( default= None, init= False)
	bPHelper0: float= field( default= 0.0, init= False)


incrementingSeed= Globals.weightInitialisationRandomSeed

def getRandomValueInRange():
	global incrementingSeed
	random.seed( incrementingSeed)
	value= random.random()
	value*= Globals.weightInitialisationRange[ 1]- Globals.weightInitialisationRange[ 0] # Make it as big as the range
	value+= Globals.weightInitialisationRange[ 0] # Shift it to the correct position

	incrementingSeed+= 1

	return value



def constructNetwork( hiddenLayersQuantity: int, neuronsInEachHiddenLayerQuantity: int):
	neuronLayers: list[ list[ Neuron]]= list()
	weightLayers: list[ list[ Weight]]= list()

	# Construct neuron layers

	currentLayer= list()
	for neuronIndex in range( len( Globals.trainingData[ 0][ 0])):
		neuron= Neuron( threshold= getRandomValueInRange())
		currentLayer.append( neuron)

	neuronLayers.append( currentLayer)
	
	for hiddenLayerIndex in range( hiddenLayersQuantity):
		currentLayer= list()
		for neuronIndex in range( neuronsInEachHiddenLayerQuantity):
			neuron= Neuron( threshold= getRandomValueInRange())
			currentLayer.append( neuron)
		neuronLayers.append( currentLayer)

	currentLayer= list()
	for neuronIndex in range( len( Globals.trainingData[ 0][ 1])):
		neuron= Neuron( threshold= getRandomValueInRange())
		currentLayer.append( neuron)

	neuronLayers.append( currentLayer)
	
	# Construct weights between each layer


	for weightLayerIndex in range( len( neuronLayers)- 1):

		currentLayer= list()

		for beforeIndex, beforeNeuron in enumerate( neuronLayers[ weightLayerIndex]):
			for afterIndex, afterNeuron in enumerate( neuronLayers[ weightLayerIndex+ 1]):

				value= getRandomValueInRange()

				weight= Weight(
					fromNeuron= beforeNeuron,
					toNeuron= afterNeuron,
					weightValue= value
				)

				currentLayer.append( weight)

		weightLayers.append( currentLayer)

	return (
		weightLayers,
		neuronLayers,
	)


(
	weightLayers,
	neuronLayers,
)= constructNetwork(
	hiddenLayersQuantity= Globals.hiddenLayersQuantity,
	neuronsInEachHiddenLayerQuantity= Globals.neuronsInEachHiddenLayerQuantity
)

print( "")
input( "Press enter to print the network structure:")
print( "\n")
first= True
for neuronLayerIndex in range( len( neuronLayers)):
	if not first:
		print( "")
		print( "CONNECTION LAYER")
		# print( [ f"|Connection {str( weight.fromNeuron.threshold)[:5]} → {str( weight.toNeuron.threshold)[:5]}, strength {str( weight.weightValue)[ :5]}|" for weight in weightLayers[ neuronLayerIndex- 1]])
		beforeNeuronLayer= neuronLayers[ neuronLayerIndex- 1]
		afterNeuronLayer= neuronLayers[ neuronLayerIndex]
		# print( [ f"|Connection {str( weight.fromNeuron.threshold)[:5]} → {str( weight.toNeuron.threshold)[:5]}, strength {str( weight.weightValue)[ :5]}|" for weight in weightLayers[ neuronLayerIndex- 1]])
		print( [ f"|Connection {str( beforeNeuronLayer.index( weight.fromNeuron))} → {str( afterNeuronLayer.index( weight.toNeuron))}, strength {str( weight.weightValue)[ :5]}|" for weight in weightLayers[ neuronLayerIndex- 1]])
		print( "")
	else:
		first= False
	print( "NEURON LAYER")
	print( [ f"|Nron AP {str(neuron.threshold)[:5]}|" for ( neuronIndex, neuron) in enumerate( neuronLayers[ neuronLayerIndex])])

print( "")
print( "The network is fully connected")
print( "")
input( "Press enter to begin training, this takes about 34 seconds on my machine using cpython 3.10:")

def propagateForward( pattern: tuple[ tuple, tuple]):
	# Reset netInput
	for neuronLayer in neuronLayers:
		for neuron in neuronLayer:
			neuron.netInput= 0.0
	
	# Calculate outputs for input layer
	for index, neuron in enumerate( neuronLayers[ 0]):
		neuron.calculatedOutput= neuron.activationFunction( pattern[ 0][ index]+ neuron.threshold)

	# Loop through weight layers as they hold both neurons either side in memory and calculate the outputs
	for weightLayerIndex, weightLayer in enumerate( weightLayers):
		for weight in weightLayer:
			# Add to the netInput of the neuron that comes after us
			weight.toNeuron.netInput+= weight.fromNeuron.calculatedOutput* weight.weightValue

		# Then run the function on them
		for neuron in neuronLayers[ weightLayerIndex+ 1]:
			neuron.calculatedOutput= neuron.activationFunction( neuron.netInput+ neuron.threshold)

fullTrainingDataCycles= 0

while True:

	epochError= 0.0

	for pattern in Globals.trainingData:

		propagateForward( pattern)

		patternError= 0.0
		# Here we find the error upon the output neurons, train their threshold and also calculate the error for the whole pattern
		for outputIndex, neuron in enumerate( neuronLayers[ -1]):
			outputError= pattern[ 1][ outputIndex]- neuron.calculatedOutput
			neuron.error= outputError* neuron.derivedActivationFunction( neuron.calculatedOutput)
			neuron.threshold+= Globals.trainingRate* outputError* neuron.derivedActivationFunction( neuron.calculatedOutput)
			patternError+= outputError** 2 

		patternError*= 0.5
		epochError+= patternError
		

		# Now calculate the error for each neuron by working backwards
		# So work backwards through the weight layers
		weightLayersLength= len( weightLayers)
		for reverseIndex in range( weightLayersLength):
			weightLayerIndex= weightLayersLength- ( reverseIndex+ 1)
			weightLayer= weightLayers[ weightLayerIndex]


			if weightLayerIndex!= 0:

				# Since we are fully connected we can sum up the error for every weights toNeuron and use this value for every neuron in the prior layer
				# Sum up the error on the following neuron* the weights value
				for weight in weightLayer:
					# Update each weights 
					momentumComponent= Globals.momentum* weight.previousWeightDelta
					weightDelta= Globals.trainingRate* weight.toNeuron.error* weight.fromNeuron.calculatedOutput+ momentumComponent
					weight.previousWeightDelta= weightDelta
					weight.weightValue+= weightDelta


					# "Part of back prop is that the new weights be calculated first and then used in adjustments to earlier units"
					# upon the fromNeuron layer accumulate the weight values * the new error from the connected toNeuron
					weight.fromNeuron.bPHelper0+= weight.weightValue* weight.toNeuron.error


				# Calculate the error for the prior neuron layer
				for neuron in neuronLayers[ weightLayerIndex]:
					# INSTEAD OF USING TOTALFROMERROR I NEED TO CALCULATE THE SUM OF ERRORK*WEIGHTJK
					# WHERE K IS EACH CONNECTED NEURON AFTER THE CURRENT
					neuron.error= neuron.bPHelper0* neuron.derivedActivationFunction( neuron.calculatedOutput)

					neuron.threshold+= neuron.bPHelper0* neuron.derivedActivationFunction( neuron.calculatedOutput)

					# Reset accumulated sum fot next step~s usage
					neuron.bPHelper0= 0.0

			else:
				# We only calculate deltas on first weight layer and dont calculate any errors
				for weight in weightLayer:
					# Update each weights 
					weightDelta= Globals.trainingRate* weight.toNeuron.error* weight.fromNeuron.calculatedOutput
					weight.weightValue+= weightDelta
			
		
	
	print( "\rEPOCH: ", fullTrainingDataCycles, "| ERROR: ", epochError, end= "")

	if epochError< Globals.acceptableErrorThreshold:
		break

	if fullTrainingDataCycles>= 9999999:
		break

	fullTrainingDataCycles+= 1


print( "\n")
print( "Training complete")
print( "")
input( "Press enter to view the results")


for pattern in Globals.trainingData:

	print( "")
	print( logicGateNamesOrdered[ pattern[ 0][ 0]], pattern[ 0])
	
	print( "EXPECTED: ", end= "")
	print( pattern[ 1], end= "")
	print( " | RESULT: ", end= "")
	propagateForward( pattern)
	print( tuple( [ round( neuron.calculatedOutput) for neuron in neuronLayers[ -1]]))

