"""
Here the concept of a request is defined for each operation type
a addition( q, w) operation under request for maximisation will request the highest possible value from each of its contained operations
a sin( q) operation ( degrees) under request for maximisation will request an input which satisys: ( ( q- 90)% 360)== 0, as all satisfying input-(.) to sin will return 1
a max( q, w) operation under request for minimization will request an input q which satisfies: q<= w, as any value of q equal to or less than w will cause w to be returned

╭──────────────────────────────────────────────────────────────╮
│                              ╭────┬─╮                        │
│                              │mult│0│                        │
│                 ╭────────────┴────┴─┴───────╮                │
│                 │                           │                │
│                 │                           │                │
│                 ▼                           ▼                │
│               ╭────┬─╮                     ╭────┬─╮          │
│               │mult│1│                 ╭───┤mult│4│          │
│        ╭──────┴────┴─┴─╮               │   ╰────┴─┴───╮      │
│        │               │               ▼              │      │
│        │               │              ┌──┐            │      │
│        │               │              │-1│            │      │
│        ▼               ▼              └──┘            ▼      │
│    ╭─────┬─╮         ╭─────┬─╮                      ╭─────┬─╮│
│    │clamp│2│         │clamp│3│                      │clamp│5││
│ ╭──┴┬────┼─╯      ╭──┴┬────┼─╯                  ╭───┴┬────┼─╯│
│ │   │    │        │   │    │                    │    │    │  │
│ ▼   ▼    ▼        ▼   ▼    ▼                    ▼    ▼    ▼  │
│+--+ ┌──┐┌──┐    +--+  ┌──┐┌──┐                +--+  ┌───┐┌──┐│
│|x0| │-3││5 │    |x1|  │0 ││70│                |x0|  │-60││-1││
│+--+ └──┘└──┘    +--+  └──┘└──┘                +--+  └───┘└──┘│
╰──────────────────────────────────────────────────────────────╯

so as this is being done for testing purposes and no constraints are present, an assumption is made that upon recieving a maximisation request, a clamp will set the free contained value to the value of the maximium contained
in the above case, [ clamp| 2] would set x0 to 5


Implements a class `Operation` which subclasses the class `BaseOperation.Operation`
This implements `Operation.beginOptimisation`
the implementation initialises value-(.) on the operation-(.) in the tree and then runs `iterateThroughRemainingInputs`
`iterateThroughRemainingInputs` will loop through every input in the function tree
for each input it will loop through each site of that input
	for each site, it will loop through all optimisation request states that are generated for that site in the current function state
		for each request state, the current site will assign a value to the current input and then call `iterateThroughRemainingInputs` for all remaining inputs

what this means is that every possible combination of sites under every request can test out assignments and the one yielding the most optimal result is chosen

the downside is that all these choices are only informed by the path from the root to the site and not from the entire function
therefore when a function requires consideration of multiple areas of a function, the code here is not sufficient
"""
from mAAP.CustomEnum import(
	IntFlag,
	auto,
)
from typing import(
	TypedDict,
	ClassVar,
	ForwardRef,
)
from collections.abc import(
	Hashable,
	Iterator,
)
from math import(
	inf as infinity,
	sin,
)
negativeInfinity= infinity* -1
from mAAP.Tests.OptimisationTests.BaseOperation import(
	Operation as BaseOperation,
	Input,
	InputLocator,
	Constant,
	OperationType,
	OperationContainedIdentifier,
	InputValue,
	setSession,
)
from mAAP.NavigableNetwork import(
	NavigableNetwork,
	NetworkElementSpecifier,
	NetworkSubsets,
)
from mAAP.DataSpecification import(
	specificationed,
	specValue,
)

ProposedInputs= tuple[ float]
ResultDataValues= dict[ ProposedInputs, float]
class OptimizationRequests:

	class OptimizationRequest:
		def __hash__( self):
			return 0

		def decideBetweenResults(
			self,
			results: ResultDataValues,
		)-> ProposedInputs:
			"""
			expecting 1..* value in the results
			"""
			raise NotImplementedError

	class Maximization:
		def decideBetweenResults(
			self,
			results: ResultDataValues,
		)-> ProposedInputs:
			results= results.copy()
			( maximumProposed, maximumResult)= results.popitem()
			for proposedInput in results:
				if results[ proposedInput]> maximumResult:
					maximumResult= results[ proposedInput]
					maximumProposed= proposedInput
					
			return maximumProposed

	class Minimization:
		def decideBetweenResults(
			self,
			results: ResultDataValues,
		)-> ProposedInputs:
			results= results.copy()
			( minimumProposed, minimumResult)= results.popitem()
			for proposedInput in results:
				if results[ proposedInput]< minimumResult:
					minimumResult= results[ proposedInput]
					minimumProposed= proposedInput
					
			return minimumProposed

	@specificationed
	class ObtainValue:
		value: float
		def __hash__( self):
			return hash( ( OptimizationRequests.ObtainValue, self.value))

		def distance(
			self,
			value,
		):
			return abs( value- self.value)

		def decideBetweenResults(
			self,
			results: ResultDataValues,
		)-> ProposedInputs:
			results= results.copy()
			( closestProposed, initialResult)= results.popitem()
			closestDistance= self.distance( initialResult)
			for proposedInput in results:
				proposedDistance= self.distance( results[ proposedInput])
				if proposedDistance< closestDistance:
					closestDistance= proposedDistance
					closestProposed= proposedInput
					
			return closestProposed
	# class MaximizeAbsolute:
	# 	...
	# class MinimizeAbsolute:
	# 	...

@specificationed
class OptimizationRuntimeData:
	# decided: bool= True
	dependantOnInputs: set[ InputLocator]= specValue( default_factory= set)
	valueCalculable: bool= True
	setRequest: None| OptimizationRequests.OptimizationRequest= None

@specificationed
class SingleInputData:
	sites: set[ NetworkElementSpecifier]
	setValue: None| float= None
	dependantOperations: set[ ForwardRef( "Operation")]= specValue( default_factory= set)
InputData= dict[ InputLocator, SingleInputData]

@specificationed
class ResultData:
	order: tuple[ InputLocator]
	values: dict[ OptimizationRequests.OptimizationRequest, ResultDataValues]

def walkThroughRequestStates(
	inputOptimisationTypes,
	indexList,
	operationSpecifier: NetworkElementSpecifier[ ForwardRef( "Operation")],
	currentIndexListIndex= -1,
	previousOperationSpecifier= None,
)-> Iterator[ ForwardRef( "Operation")]:
	if operationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest== None:
		# Generate requests
		if previousOperationSpecifier== None:
			requests= inputOptimisationTypes
		else:
			previousIndexIntoCurrent= indexList[ currentIndexListIndex][ 1]
			requests= previousOperationSpecifier.element.generateRequests(
				request= previousOperationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest,
				desiredContained= { previousIndexIntoCurrent},
			)[ previousIndexIntoCurrent]

		currentIndexListIndex+= 1
		if currentIndexListIndex== len( indexList):
			# If last operation is not set then loop through request-(.) setting them and yielding them
			for request in requests:
				operationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= request
				yield operationSpecifier.element
				operationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= None
		else:
			( currentIndexIntoNextLinkageType, currentIndexIntoNextId)= indexList[ currentIndexListIndex]
			nextOperationSpecifier= operationSpecifier.getContainedSpecifier(
				linkageType= currentIndexIntoNextLinkageType,
				connectionId= currentIndexIntoNextId,
			)
			for request in requests:
				operationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= request
				# Walk next in index list
				# So need to get the lt and connId of contained
				for lastOperationState in walkThroughRequestStates(
					inputOptimisationTypes= None,
					indexList= indexList,
					operationSpecifier= nextOperationSpecifier,
					currentIndexListIndex= currentIndexListIndex,
					previousOperationSpecifier= operationSpecifier,
				):
					yield lastOperationState
				operationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= None
	else:
		# if not at the end then walk to the next
		# if at the end then simply yield that
		currentIndexListIndex+= 1
		if currentIndexListIndex== len( indexList):
			yield operationSpecifier.element
		else:
			( currentIndexIntoNextLinkageType, currentIndexIntoNextId)= indexList[ currentIndexListIndex]
			nextOperationSpecifier= operationSpecifier.getContainedSpecifier(
				linkageType= currentIndexIntoNextLinkageType,
				connectionId= currentIndexIntoNextId,
			)
			for lastOperationState in walkThroughRequestStates(
				inputOptimisationTypes= None,
				indexList= indexList,
				operationSpecifier= nextOperationSpecifier,
				currentIndexListIndex= currentIndexListIndex,
				previousOperationSpecifier= operationSpecifier,
			):
				yield lastOperationState


def iterateThroughRemainingInputs(
	optimisationTypes: set[ OptimizationRequests.OptimizationRequest],
	inputData: InputData,
	remainingInputs: set[ InputLocator],
	rootOperation: ForwardRef( "Operation"),
	resultData: ResultData,
):
	for inputLocator in remainingInputs:
		for siteSpecifier in inputData[ inputLocator].sites:
			# Iterate over each request state
			# Retain state-(?) between iteration
			for lastOperationInState in walkThroughRequestStates(
				inputOptimisationTypes= optimisationTypes,
				indexList= siteSpecifier.indexList[ : -1],
				operationSpecifier= NetworkElementSpecifier(
					rootElement= rootOperation,
					element= rootOperation,
					indexList= [],
				),
				previousOperationSpecifier= None,
			):
				# Alway-s set the input
				# then
				# If there are remaining input-(.) then run through them
				# if not then calculate the final value of the function and record it
				# be sure to adjust the valueCalculable set-ing of operation-(.)
				inputValue= lastOperationInState.assignInputValue(
					containedOperation= siteSpecifier.indexList[ -1][ 1],
					request= lastOperationInState.runtimeData[ OperationType.OPTIMIZATION].setRequest,
				)
				if inputValue== None:
					continue

				# Set all affected
				setOperations= set()
				for dependantOperation in inputData[ inputLocator].dependantOperations:
					# Check if all other input dependencies are met
					dependenciesMet= True
					for dependantOnInput in dependantOperation.runtimeData[ OperationType.OPTIMIZATION].dependantOnInputs.difference( { inputLocator}):
						if inputData[ dependantOnInput].setValue== None:
							dependenciesMet= False

					if dependenciesMet:
						setOperations.add( dependantOperation)
						dependantOperation.runtimeData[ OperationType.OPTIMIZATION].valueCalculable= True

				inputData[ inputLocator].setValue= inputValue

				# If remaining input-(?) then run again with those
				# If no remaining input-(?) then calculate function
				newlyRemainingInputs= remainingInputs.copy()
				newlyRemainingInputs.remove( inputLocator)
				if len( newlyRemainingInputs)> 0:
					iterateThroughRemainingInputs(
						optimisationTypes= optimisationTypes,
						inputData= inputData,
						remainingInputs= newlyRemainingInputs,
						rootOperation= rootOperation,
						resultData= resultData,
					)
				else:
					resultData.values[ rootOperation.runtimeData[ OperationType.OPTIMIZATION].setRequest][ tuple( ( inputData[ orderedInputLocator].setValue for orderedInputLocator in resultData.order))]= rootOperation.calculateValue()

				inputData[ inputLocator].setValue= None

				for newlySetOperation in setOperations:
					newlySetOperation.runtimeData[ OperationType.OPTIMIZATION].valueCalculable= False
					# Clear any cache
					newlySetOperation.resultCache= None
					
				
				# Make sure to set the value calculable for any op-(.) that are altered by any set variable

			# So have request-(?) for the operation above the input with the network state properly configured
			# For each of these, the k

			# previousOperation= None
			# for descendingSpecifier in rootOperation.walkInNetworkSubset(
			# 	networkSubset= NetworkSubsets.RootToLocation(
			# 		location= siteSpecifier.getContainingSpecifier()[ 0],
			# 		walkType= NavigableNetwork.WalkType.FIRST_ROOT_TO_TIP,
			# 	),
			# ):
			# 	if descendingSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest== None:
			# 		if previousOperation== None:
			# 			# root
			# 			requests= optimisationTypes
			# 		else:
			# 			descendingOperationLocator= descendingSpecifier.indexList[ -1][ 1]
			# 			requests= previousOperation.generateRequests(
			# 				request= previousOperation.runtimeData[ OperationType.OPTIMIZATION].setRequest,
			# 				desiredContained= { descendingOperationLocator},
			# 			)[ descendingOperationLocator]
			# 		for request in requests:
			# 			descendingSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= request
			# 			...
			# 			descendingSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= None
			# 	previousOperation= descendingSpecifier.element

def cacheWhenCalculable(
	self,
	operation: BaseOperation,
)-> bool:
	return operation.runtimeData[ OperationType.OPTIMIZATION].valueCalculable


class Operation( BaseOperation):

	def beginOptimisation(
		self,
		optimisationTypes: set[ OptimizationRequests.OptimizationRequest],
	):
		"""
		Creates desired session data
		record all input-(.)
		the operation result caching structire relies on knowing whether a operation is dependant upon a specific input
		so for each operation, the input-(.) that it is dependant on are recorded, then when assigning value-(.) for input-(.) in various part-(.) of the process, caching should work well
		"""
		if self.sessionData== None:
			setSession(
				self,
			)

		self.sessionData.cachingBehaviour= cacheWhenCalculable
		inputData: dict[ InputLocator, SingleInputData]= dict()
		def retrieveInputValueFromSetValues(
			sessionData,
			inputLocator: InputLocator,
		)-> InputValue| None:
			return inputData[ inputLocator].setValue
		self.sessionData.retrieveInputValue= retrieveInputValueFromSetValues
		
		walker= self.walkFromSpecifiedRoot(
			walkType= NavigableNetwork.WalkType.FIRST_TIP_TO_ROOT,
			# walkedLinkageTypes= BaseOperation.ElementLinkageTypes.CONTAINED_OPERATION#| BaseOperation.ElementLinkageTypes.CONTAINED_INPUT,
		)
		for specifier in walker:
			specifier.element.runtimeData[ OperationType.OPTIMIZATION]= OptimizationRuntimeData()
			# decided= True
			if isinstance( specifier.element, Operation):
				inputs= specifier.element.getContainedNetworkElements( BaseOperation.ElementLinkageTypes.CONTAINED_INPUT)
				if len( inputs):
					specifier.element.runtimeData[ OperationType.OPTIMIZATION].valueCalculable= False
					for ( inputLocator, input,) in inputs.iter(
						locator= True,
						index= False,
						contained= True,
					):
						site= specifier.getContainedSpecifier(
							linkageType= BaseOperation.ElementLinkageTypes.CONTAINED_INPUT,
							connectionId= inputLocator,
						)
						if input.locator in inputData:
							inputData[ input.locator].sites.add( site)
							inputData[ input.locator].dependantOperations.add( specifier.element)
						else:
							inputData[ input.locator]= SingleInputData(
								sites= { site},
								dependantOperations= { specifier.element},
							)
						specifier.element.runtimeData[ OperationType.OPTIMIZATION].dependantOnInputs.add( input.locator)

					# decided= False
					# op

				# if decided:
				for ( operation,) in specifier.element.getContainedNetworkElements( BaseOperation.ElementLinkageTypes.CONTAINED_OPERATION).iter():
					if len( operation.runtimeData[ OperationType.OPTIMIZATION].dependantOnInputs)> 0:
						specifier.element.runtimeData[ OperationType.OPTIMIZATION].valueCalculable= False

						specifier.element.runtimeData[ OperationType.OPTIMIZATION].dependantOnInputs|= operation.runtimeData[ OperationType.OPTIMIZATION].dependantOnInputs
					# if operation.runtimeData[ OperationType.OPTIMIZATION][ OptimizationRuntimeData.DECIDED]== False:
						# decided= False
						# break

			# Calculate here, perhaps calculate can be recursive
			# So caluculate calls calculate of contained
			# So why use decided?
			# Well at the top operation, it prevents chaining down every time
			# So combine these two, implement a calculate on the operation type
			# runtime locator can be optimisaion string, or maybe general operation type enum

			# specifier.element.runtimeData[ OperationType.OPTIMIZATION][ OptimizationRuntimeData.DECIDED]= decided

		resultData= ResultData(
			order= tuple( inputData.keys()),
			values= { request: dict() for request in optimisationTypes},
		)
		iterateThroughRemainingInputs(
			optimisationTypes= optimisationTypes,
			inputData= inputData,
			remainingInputs= set( inputData.keys()),
			rootOperation= self,
			resultData= resultData,
		)
		return {
			request: request.decideBetweenResults( resultData.values[ request])
			for request in resultData.values
		}


	def generateRequests(
		self,
		request: OptimizationRequests.OptimizationRequest,
		desiredContained: None| set[ OperationContainedIdentifier],
	)-> dict[
		OperationContainedIdentifier,
		set[ OptimizationRequests.OptimizationRequest],
	]:
		raise NotImplementedError

	def assignInputValue(
		self,
		containedOperation: OperationContainedIdentifier,
		request: OptimizationRequests.OptimizationRequest,
	)-> InputValue| None:
		if containedOperation not in self.containedOperationIdentifiersSet:
			raise ValueError( "Operation not present")
		return self._assignInputValue(
			containedOperation= containedOperation,
			request= request,
		)

	def _assignInputValue(
		self,
		containedOperation: OperationContainedIdentifier,
		request: OptimizationRequests.OptimizationRequest,
	)-> InputValue| None:
		raise NotImplementedError


class Operations:
	Input= Input
	Constant= Constant

	class Multiplication( Operation):
		containedOperationIdentifiers: ClassVar[ set]= {
			"q",
			"w",
		}
		def __hash__( self): return id( self)

		def _calculateValue( self):
			return self.containedOperations[ "q"].calculateValue()* self.containedOperations[ "w"].calculateValue()
		
		def generateRequests(
			self,
			request: OptimizationRequests.OptimizationRequest,
			desiredContained: None| set[ OperationContainedIdentifier],
		)-> dict[
			OperationContainedIdentifier,
			set[ OptimizationRequests.OptimizationRequest],
		]:
			# Only two contained
			# Generate regardless of set status
			# for { q, w} if other value known then use that to request
			# should never be generating for op which is known
	 		# should never be generating for op which is set

			requestType= type( request)

			if isinstance( desiredContained, dict) and ( len( desiredContained)== 1):
				# Check if the other~s value is known
				requestedOp= list( desiredContained)[ 0]
				otherOpId= self.containedOperationIdentifiers.difference( desiredContained).pop()
				otherOp= self.containedOperations[ otherOpId]
				if otherOp.runtimeData[ OperationType.OPTIMIZATION].valueCalculable:
					otherValue= otherOp.calculateValue()
					requestType
					if requestType in { OptimizationRequests.Maximization, OptimizationRequests.Minimization}:
						otherValueNegative= otherValue< 0
						match ( otherValueNegative, requestType):
							case ( False, OptimizationRequests.Maximization):
								generatedRequests= { requestedOp: { OptimizationRequests.Maximization()}}
							case ( False, OptimizationRequests.Minimization):
								generatedRequests= { requestedOp: { OptimizationRequests.Minimization()}}
							case ( True, OptimizationRequests.Maximization):
								generatedRequests= { requestedOp: { OptimizationRequests.Minimization()}}
							case ( True, OptimizationRequests.Minimization):
								generatedRequests= { requestedOp: { OptimizationRequests.Maximization()}}
					elif requestType== OptimizationRequests.ObtainValue:
						generatedRequests= {
							requestedOp: {
								OptimizationRequests.ObtainValue(
									value= request.value/ otherValue,
								),
							},
						}
					else:
						raise NotImplementedError
				else:
					if requestType in { OptimizationRequests.Maximization, OptimizationRequests.Minimization}:
						generatedRequests= {
							requestedOp: {
								OptimizationRequests.Minimization(),
								OptimizationRequests.Maximization(),
							}
						}
					elif requestType== OptimizationRequests.ObtainValue:
						generatedRequests= {
							requestedOp: {
								OptimizationRequests.ObtainValue(
									value= request.value,
								),
							},
						}
					else:
						raise NotImplementedError

			else:
				if requestType in { OptimizationRequests.Maximization, OptimizationRequests.Minimization}:
					generatedRequests= {
						"q": {
							OptimizationRequests.Minimization(),
							OptimizationRequests.Maximization(),
						},
						"w": {
							OptimizationRequests.Minimization(),
							OptimizationRequests.Maximization(),
						},
					}
				elif requestType== OptimizationRequests.ObtainValue:
					# Not sure what to do here, 
					# Could request both for both, but the iteration-s where they are both requested to be the same would be wasted
					# This may be a case where site optimisation is insufficient, good example case
					# Just request value for each at the minute
					generatedRequests= {
						"q": {
							OptimizationRequests.ObtainValue(
								value= request.value,
							),
						},
						"w": {
							OptimizationRequests.ObtainValue(
								value= request.value,
							),
						},
					}
				else:
					raise NotImplementedError

			return generatedRequests
			# If both are un decided then 
			# No matter the request

		def _assignInputValue(
			self,
			containedOperation: OperationContainedIdentifier,
			request: OptimizationRequests.OptimizationRequest,
		)-> InputValue| None:
			# Unsure here, maybe just infinity, well if other is known then important
			otherOperationId= self.containedOperationIdentifiers.difference( set( containedOperation)).pop()
			otherOperation= self.containedOperations[ otherOperationId]
			if otherOperation.runtimeData[ OperationType.OPTIMIZATION].valueCalculable:
				otherOperationValue= otherOperation.calculateValue()
			else:
				otherOperationValue= None
				
			match type( request):
				case OptimizationRequests.Maximization:
					if otherOperationValue== None:
						# Try for a positive, all that matters is that it is the oposite of the Minimization response
						return infinity
					else:
						if otherOperationValue< 0:
							return negativeInfinity
						else:
							return infinity
				case OptimizationRequests.Minimization:
					if otherOperationValue== None:
						return negativeInfinity
					else:
						if otherOperationValue< 0:
							return infinity
						else:
							return negativeInfinity
				case OptimizationRequests.ObtainValue:
					if otherOperationValue== None:
						# How to make it easiest for the other choice to pick the exact value, 
						# perhaps it is set here and the other choice must be 1
						return request.value
					else:
						return request.value/ otherOperationValue

	class Clamp( Operation):
		containedOperationIdentifiers: ClassVar= {
			"clamped",
			"minimum",
			"maximum",
		}

		def _calculateValue( self):
			return max(
				min(
					self.containedOperations[ "clamped"].calculateValue(),
					self.containedOperations[ "maximum"].calculateValue(),
				),
				self.containedOperations[ "minimum"].calculateValue(),
			)
		
		def generateRequests(
			self,
			request: OptimizationRequests.OptimizationRequest,
			desiredContained: None| set[ OperationContainedIdentifier],
		)-> dict[
			OperationContainedIdentifier,
			set[ OptimizationRequests.OptimizationRequest],
		]:
			# when maximising
			# for clamped maximize
			# for minimum, maximize
			# for minimum, maximize

			# opposite for minimizing

			# for obtenance as well, set all to the desired request
			# So pass through all for now
			return {
				"clamped": { request},
				"minimum": { request},
				"maximum": { request},
			}

		def _assignInputValue(
			self,
			containedOperation: OperationContainedIdentifier,
			request: OptimizationRequests.OptimizationRequest,
		)-> InputValue| None:
			# I think in a proper approach, one would assign infinitite value-(?) for minimization and maximisarion
			# but here use min and max contained value-(.) if possible
			# for obtenance
			# set each to the value
			match type( request):
				case OptimizationRequests.Maximization:
					if containedOperation== "clamped":
						if self.containedOperations[ "maximum"].runtimeData[ OperationType.OPTIMIZATION].valueCalculable:
							return self.containedOperations[ "maximum"].calculateValue()
						else:
							return None
					else:
						return infinity
				case OptimizationRequests.Minimization:
					if containedOperation== "clamped":
						if self.containedOperations[ "minimum"].runtimeData[ OperationType.OPTIMIZATION].valueCalculable:
							return self.containedOperations[ "minimum"].calculateValue()
						else:
							return None
					else:
						return negativeInfinity
				case OptimizationRequests.ObtainValue:
					return request.value

	class Sin( Operation):
		containedOperationIdentifiers: ClassVar= {
			"contained",
		}

		def _calculateValue( self):
			return sin( self.containedOperations[ "contained"]).calculateValue()
		
		def generateRequests(
			self,
			request: OptimizationRequests.OptimizationRequest,
			desiredContained: None| set[ OperationContainedIdentifier],
		)-> dict[
			OperationContainedIdentifier,
			set[ OptimizationRequests.OptimizationRequest],
		]:
			...
			# Radians
			# So request the value to satisfy the desired result

		def _assignInputValue(
			self,
			containedOperation: OperationContainedIdentifier,
			request: OptimizationRequests.OptimizationRequest,
		)-> InputValue| None:
			...
			# So same as request but assignment
			# perhaps assignment with constraint would require being able to select a request of the constraint system
			# maybe could map out whole constraint space
			# so how would one map out input-(.) dependance on each other?
			# how then would the optimization system hhok into this?, need to know how the optimisation system fit outside of site optimisation
