"""
This is designed to emulate a scenario in which one is locked inside a room with wheel-s on the wall, a number display on the wall.
It is known that, should the number reach the { highest, lowest, value closest to another} value it possibly can, then the door will open and they will be free.
The wheel-s affected the number on the wall
If one can also observe the mechanism-s linking the wheel-s to the number display then how can they escape the room?

To emulate this, there exists a `System` which has a sequence of `OperationStack`-s ( the mechanism between the wheel and the door) which are indexed by integer-s ( the index of the wheel). The system can be queried with `System.getRating` ( the number display).

an `Operation` in a stack can either be a `SingleOperation` which simply takes the previous result in the stack as input or `ElementOperation` which can take either the base value of an input or the result of a stack.

it is of note that the result-s of the utility portion of the system do not contribute to the system rating

Part of the exploratory effort was to develop a system which can compute the first derivative of the system in respect to a singular input
some call this symbolic differentiation, i believe partial differentiation
it will give way to a unifyed equation system using arbitrary nested function decleration
symbolic integration is seemingly much harder and using an existing implementation may be the way
"""
import pprint
from io import(
	StringIO,
)
from mAAP.DataSpecification import(
	specificationed,
	specValue,
)
from copy import(
	deepcopy,
)
import numpy
from gi.repository import(
	Gtk,
	Adw,
)
from mAAP.Widgets import(
	NicerPaned,
	ListViewPopover,
	DataVisualiser,
	DropDown,
)
from mAAP.Utils import(
	vecLen,
	ImplementationError,
	fitRange,
	lerp,
	NonGObjectGioListStore,
	MutableContainer,
)
from mAAP.CustomEnum import(
	IntFlag,
	auto,
)
from collections.abc import(
	Iterator,
)
from mAAP.DataForTypes import(
	getEditingWidgetList,
)
from typing import(
	Protocol,
	Self,
	TypedDict,
	ForwardRef,
	Union,
	Literal,
	ClassVar,
	Any,
)
# One thing of note is that certain configuration-s can cause some/ all value-s to become unstable, the derived ruleset-s no longer apply when every result is so random, so the ability to ( remember a stable state, to re approach a stable state, to never leave a stable state) is useful in combating these scenario-s
# Never leaving a stable state seems undesirable, often new configuration-s are found in a chaotic state

# Maybe also allow picking from predefined function
import random
import math

def safeDiv( q, w):
	if w== 0:
		return math.inf
	else:
		return q/ w
def safeLog( q, base):
	if q<= 0:
		return 0.0
	else:
		return math.log( q, base)
def safePow( q, w):
	if q== 0:
		if w< 0:
			return 0.0
	elif q< 0:
		( decimalComponent, _)= math.modf( w)
		if decimalComponent!= 0:
			return 0.0
	try:
		return math.pow( q, w)
	except Exception as e:
		print( "Q:", q)
		print( "W:", w)
		raise e


ElementIndex= int
OperationIndex= int
IterationId= int

@specificationed
class Operation():
	_stack: "OperationStack"= specValue( init= False)
	_stackPosition: int= specValue( init= False)

	_resultCache: dict[ IterationId, float]
	_derivativeCache: dict[ IterationId, dict[ ElementIndex, float]]

	# Operation-s are instanciated with their unique required data
	def __init__( self,):
		self._resultCache= dict()
		self._derivativeCache= dict()

	def _getAdditionalInfluencingElements(
		self,
	)-> set[ ElementIndex]:
		"""
		Retrieve all element-s which influence the result of this operation
		Do not include element-s which influences the result which is input into this operation
		This should only include element-s which influence this operation by means of special function of this operation
		"""
		return set()

	def _getResultFromInput(
		self,
		inputResult: float,
	)-> float:
		raise NotImplementedError

	def _getDerivativeFromInputs(
		self,
		freeElementIndex: ElementIndex,
		inputDerivative: float,
		inputResult: float,
	)-> float:
		# Imp-s may find the free element index useful
		raise NotImplementedError

	def cacheResult(
		self,
		result: float,
	):
		self._resultCache[ hash( self._stack._system._systemStateSummary)]= result

	def cacheDerivative(
		self,
		result: float,
		freeElementIndex: ElementIndex,
	):
		systemStateHash= hash( self._stack._system._systemStateSummary)
		if systemStateHash in self._derivativeCache:
			self._derivativeCache[ systemStateHash][ freeElementIndex]= result
		else:
			self._derivativeCache[ systemStateHash]= {
				freeElementIndex: result,
			}

	def getLowerOperation(
		self
	):
		"""Onwy to be cawwed buy wesposible cawwes who know they r not ayt the bowtom"""
		return self._stack.getOperationAtIndex( self._stackPosition- 1)

	def getInputResult(
		self,
	)-> float:
		if self._stackPosition== 0:
			inputResult= self._stack._system.getElementBaseValue( self._stack.rootElementIndex)
		else:
			inputResult= self.getLowerOperation().getResult()
		return inputResult

	def getResult(
		self,
	)-> float:
		# Look for cache result
		systemStateHash= hash( self._stack._system._systemStateSummary)
		if systemStateHash in self._resultCache:
			return self._resultCache[ systemStateHash]

		# Get result from element below in the stack
		inputResult= self.getInputResult()

		# Only take real component as imaginary number generation can occur for an unknown reason
		result= self._getResultFromInput(
			inputResult= inputResult,
		).real
		# Add result to cache
		self.cacheResult(
			result= result,
		)
		return result 
			
	def getInputDerivative(
		self,
		freeElementIndex: ElementIndex,
	):
		"""Returns None if the calling aspect should not calculate the derivative as it is known to be 0.0"""
		# If free is at the root then step down and get the deriv of that below, if at bottom then it is 1.0
		# If free is not at the root then 
		#   If free is not in stack-s influenced elements then getDerivative should return 0.0
		#   If it is then if this op~s index is the one which if the recorded ( first index of influence) then the input is 0.0
		#   If it is not the bottom recorded then get the deriv of that below
		if freeElementIndex== self._stack._rootElementIndex:
			if self._stackPosition== 0:
				return 1.0
			else:
				return self.getLowerOperation().getDerivative( freeElementIndex)
		else:
			if freeElementIndex not in self._stack._influencingElements:
				return None
			indexOfFreeElementInfluenceStart= self._stack._influencingElements[ freeElementIndex]
			if self._stackPosition> indexOfFreeElementInfluenceStart:
				return self.getLowerOperation().getDerivative( freeElementIndex)
			elif self._stackPosition== indexOfFreeElementInfluenceStart:
				return 0.0
			else:
				# Then it is below the occurence of influence of this stack
				# The sensitivity is 0.0, but with current design, this should never be called
				raise ImplementationError
				return 0.0
	
	def getDerivative(
		self,
		freeElementIndex: ElementIndex,
	):
		systemStateHash= ( self._stack._system._systemStateSummary)
		if systemStateHash in self._derivativeCache:
			if freeElementIndex in self._derivativeCache[ systemStateHash]:
				return self._derivativeCache[ systemStateHash][ freeElementIndex]

		inputDerivative= self.getInputDerivative( freeElementIndex)
		if inputDerivative== None:
			return 0.0
		inputResult= self.getInputResult()
		# Only take real component as imaginary number generation can occur for an unknown reason
		result= self._getDerivativeFromInputs(
			freeElementIndex= freeElementIndex,
			inputDerivative= inputDerivative,
			inputResult= inputResult,
		).real
		self.cacheDerivative(
			result= result,
			freeElementIndex= freeElementIndex,
		)
		return result


class StackSpace( IntFlag):
	SYSTEM_RATING_AFFECTOR= auto()
	UTILITY= auto()
StackSpaceSpecifier= Union[
	Literal[ StackSpace.SYSTEM_RATING_AFFECTOR],
	tuple[ Literal[ StackSpace.UTILITY], int],
]
StackSpecifier= tuple[ StackSpaceSpecifier, ElementIndex]

@specificationed
class OperationStack:

	_system: "System"= specValue( init= False)
	_rootElementIndex: ElementIndex= specValue( init= False)
	_influencingElements: dict[ ElementIndex, OperationIndex]= specValue( init= False)
	_influencingElementsFunctionState: int| None= specValue( init= False)
	_operations: list[ Operation]

	@property
	def rootElementIndex( self): return self._rootElementIndex

	def __init__(
		self,
	):
		self._operations= list()
		self._influencingElements= None
		self._influencingElementsFunctionState= None

	def iterOperations(
		self,
	)-> Iterator[ Operation]:
		return iter( self._operations)

	def __len__( self):
		return len( self._operations)
	
	def appendOperation(
		self,
		operation: Operation,
	):
		operation._stack= self
		operation._stackPosition= len( self._operations)
		# Register operation cache-s with session
		self._system._registerOperationCaches( operation)
		self._operations.append( operation)

	def getOperationAtIndex(
		self,
		index: int,
	)-> Operation:
		return self._operations[ index]

	def determineInfluencingElements(
		self,
	):
		if ( self._influencingElements!= None)and ( self._influencingElementsFunctionState== self._system._systemStateSummary[ 1]):
			return self._influencingElements
		# Construct self._influencingElements as dict[ ElementId, OperationIndexOfFirstOccurence]
		influencingElements= dict()
		for ( operationIndex, operation) in enumerate( self._operations):
			operationInfluencingElements= operation._getAdditionalInfluencingElements()
			operationInfluencingElements.difference_update( set( influencingElements.keys()))
			if self._rootElementIndex in operationInfluencingElements:
				operationInfluencingElements.remove( self._rootElementIndex)
			for newInfluencer in operationInfluencingElements:
				influencingElements[ newInfluencer]= operationIndex

		self._influencingElements= influencingElements
		self._influencingElementsFunctionState= self._system._systemStateSummary[ 1]
		return self._influencingElements

@specificationed
class System():

	Space= dict[ ElementIndex, OperationStack]
	BaseSpaceType= dict[ ElementIndex, list[ Operation]]
	BaseFunctionSpecification= tuple[
		BaseSpaceType,
		list[ BaseSpaceType],
	]

	elements: list[ float]
	# _iterationId: IterationId
	_systemStateSummary: tuple[
		int, # Incrmented value representing the element-s state
		int, # Incrmented value representing the function state
	]
	_sessionCache: list[ dict[ IterationId, Any]]
	function: list[
		Space,
		list[ Space],
	]

	# @property
	# def iterationId( self): return self._iterationId

	def __init__(
		self,
		# utilitySpaceNumber: int= 0,
		elementValues: list[ float]= None,
		baseFunction: BaseFunctionSpecification= None,
	):
		# self._iterationId= 0
		self._systemStateSummary= (
			0,
			0,
		)
		self._sessionCache= list()

		if elementValues== None:
			elementValues= []
		self.elements= elementValues
		if baseFunction!= None:
			self.setFunctionFromBase( baseFunction)
		else:
			self.function= (
				dict(),
				[],
			)

	def setFunctionFromBase(
		self,
		baseFunction: BaseFunctionSpecification,
		clearCurrentOperationCaches: bool= True,
	):
		self._sessionCache= list()
		self.function= (
			dict(),
			[],
		)
		def constructSpace(
			baseSpace,
			spaceSpecifier,
		):
			for elementIndex in baseSpace:
				stack= OperationStack()
				# Set system before append-ing
				stack._system= self
				baseOpList= baseSpace[ elementIndex]
				for operationInstance in baseOpList:
					stack.appendOperation( operationInstance)
				self.appendOperationStack(
					elementIndex= elementIndex,
					stack= stack,
					stackSpaceSpecifier= spaceSpecifier,
				)
			
		constructSpace(
			baseSpace= baseFunction[ 0],
			spaceSpecifier= StackSpace.SYSTEM_RATING_AFFECTOR,
		)
		for ( utilitySpaceIndex, utilitySpace) in enumerate( baseFunction[ 1]):
			self.function[ 1].append( dict())
			constructSpace(
				baseSpace= utilitySpace,
				spaceSpecifier= ( StackSpace.UTILITY, utilitySpaceIndex)
			)


		if clearCurrentOperationCaches== True:
			self.clearOperationCache( hash( self._systemStateSummary))

		self._systemStateSummary= (
			self._systemStateSummary[ 0],
			self._systemStateSummary[ 1]+ 1,
		)

		self.analyseFunction()

	def analyseFunction(
		self,
	):
		for space in self.iterSpaces():
			for rootElementIndex in space:
				operationStack= space[ rootElementIndex]
				operationStack.determineInfluencingElements()

	def iterSpaces(
		self,
	)-> Iterator[ Space]:
		yield self.function[ 0]
		for space in self.function[ 1]:
			yield space

	def setElementValue(
		self,
		index: int,
		value: float,
		clearCurrentOperationCaches: bool= True,
	):
		self.elements[ index]= value
		if clearCurrentOperationCaches== True:
			self.clearOperationCache( hash( self._systemStateSummary))

		self._systemStateSummary= (
			self._systemStateSummary[ 0]+ 1,
			self._systemStateSummary[ 1],
		)

	def setElements(
		self,
		elements: list[ float],
		clearCurrentOperationCaches: bool= True,
	):
		self.elements= elements
		if clearCurrentOperationCaches== True:
			self.clearOperationCache( hash( self._systemStateSummary))

		self._systemStateSummary= (
			self._systemStateSummary[ 0]+ 1,
			self._systemStateSummary[ 1],
		)

	def clearOperationCache(
		self,
		systemStateHash: int,
	):
		for operationCache in self._sessionCache:
			if systemStateHash in operationCache:
				operationCache.pop( systemStateHash)

	def _registerOperationCaches(
		self,
		operation: Operation,
	):
		self._sessionCache.append( operation._derivativeCache)
		self._sessionCache.append( operation._resultCache)
	
	def getElementBaseValue(
		self,
		elementIndex: ElementIndex,
	):
		return self.elements[ elementIndex]

	def alterUtilitySpaceNumber(
		self,
		newNumber: int,
	):
		currentLen= len( self.function[ 1])
		if newNumber== currentLen:
			return
		if newNumber> currentLen:
			self.function[ 1]+= [ dict() for newIndex in range( newNumber- currentLen)]
		if newNumber< currentLen:
			self.function= (
				self.function[ 0],
				self.function[ 1][ : newNumber],
			)
	
	def getSensitivityToElement(
		self,
		freeElementIndex: ElementIndex,
	)-> float:
		totalResult= 0.0
		for stackRootElementIndex in self.function[ 0]:
			stack= self.function[ 0][ stackRootElementIndex]
			if len( stack)> 0:
				totalResult+= stack.getOperationAtIndex( -1).getDerivative( freeElementIndex)
		return totalResult
	def getRating(
		self,
	)-> float:
		totalResult= 0.0
		for stackRootElementIndex in self.function[ 0]:
			stack= self.function[ 0][ stackRootElementIndex]
			if len( stack)> 0:
				totalResult+= stack.getOperationAtIndex( -1).getResult()
		return totalResult

	def appendOperationStack(
		self,
		elementIndex: ElementIndex,
		stack: OperationStack,
		stackSpaceSpecifier: StackSpaceSpecifier,
	):
		stack._system= self
		stack._rootElementIndex= elementIndex
		stackSpace= self.getStackSpace( stackSpaceSpecifier)
		stackSpace[ elementIndex]= stack
		
	def getStackSpace(
		self,
		stackSpaceSpecifier: StackSpaceSpecifier,
	)-> Space:
		if isinstance( stackSpaceSpecifier, tuple):
			stackSpace= self.function[ 1][ stackSpaceSpecifier[ 1]]
		else:
			stackSpace= self.function[ 0]
		return stackSpace

	def getOperationStack(
		self,
		stackSpecifier: StackSpecifier,
	)-> OperationStack:
		( stackSpaceSpecifier, elementIndex)= stackSpecifier
		stackSpace= self.getStackSpace( stackSpaceSpecifier)
		return stackSpace[ elementIndex]

class ElementValueSourceType( IntFlag):
	SYSTEM_RATING_AFFECTOR= StackSpace.SYSTEM_RATING_AFFECTOR
	UTILITY= StackSpace.UTILITY
	BASE_VALUE= auto()
elementValueSourceSpecifierData= {
	ElementValueSourceType.SYSTEM_RATING_AFFECTOR: ElementValueSourceType.SYSTEM_RATING_AFFECTOR,
	ElementValueSourceType.UTILITY: tuple[ ElementValueSourceType.UTILITY, int],
	ElementValueSourceType.BASE_VALUE: ElementValueSourceType.BASE_VALUE,
}
ElementValueSourceSpecifier= Union[ *elementValueSourceSpecifierData.values()]


class SingleOperation( Operation):
	def __init__(
		self,
	):
		Operation.__init__( self)

	def _getResultFromInput(
		self,
		inputResult: float,
	)-> float:
		return self.calculateResult(
			calculatedResult= inputResult,
		)

	def _getDerivativeFromInputs(
		self,
		freeElementIndex: ElementIndex,
		inputDerivative: float,
		inputResult: float,
	)-> float:
		return self.calculateDerivative(
			calculatedDerivative= inputDerivative,
			calculatedResult= inputResult,
		)
	
	@staticmethod
	def calculateResult(
		calculatedResult: float
	):
		raise NotImplementedError
	@staticmethod
	def calculateDerivative(
		calculatedDerivative: float,
		calculatedResult: float,
	):
		raise NotImplementedError

class ElementOperation( Operation):

	def __init__(
		self,
		combinedElementIndex: ElementIndex,
		combinedElementValueSource: ElementValueSourceSpecifier,
	):
		self.combinedElementIndex= combinedElementIndex
		self.combinedElementValueSource= combinedElementValueSource
		Operation.__init__( self)

	def _getAdditionalInfluencingElements(
		self,
	):
		if self.combinedElementValueSource== ElementValueSourceType.BASE_VALUE:
			return { self.combinedElementIndex}
		else:
			otherStack= self._stack._system.getOperationStack(
				stackSpecifier= ( self.combinedElementValueSource, self.combinedElementIndex),
			)
			influencingElements= set( otherStack.determineInfluencingElements().keys())
			influencingElements.add( self.combinedElementIndex)
			return influencingElements

	def _getResultFromInput(
		self,
		inputResult: float,
	)-> float:
		if self.combinedElementValueSource== ElementValueSourceType.BASE_VALUE:
			otherElementValue= self._stack._system.getElementBaseValue( self.combinedElementIndex)
		else:
			otherStack= self._stack._system.getOperationStack(
				stackSpecifier= ( self.combinedElementValueSource, self.combinedElementIndex),
			)
			otherElementValue= otherStack.getOperationAtIndex( -1).getResult()

		return self.calculateResult(
			calculatedResult= inputResult,
			otherElementResult= otherElementValue,
		)

	def _getDerivativeFromInputs(
		self,
		freeElementIndex: ElementIndex,
		inputDerivative: float,
		inputResult: float,
	)-> float:
		# Only need the result value of the other element
		# If that element is the result of a function
		# If that element is:
		#        a function of another stack which is the free element
		#    Then pass the other element result
		# If that element is:
		#        the free element base value
		#        the base value of a not free element
		#        a function of another stack, which is not influenced by the free element
		#    Then no pass the other element result

		# Maybe just pass always anyway
		# The result should already be cached to be honest so the overhead is just cache retrieval and transfer
		# This discussion is important

		if self.combinedElementValueSource== ElementValueSourceType.BASE_VALUE:
			otherElementResultValue= self._stack._system.getElementBaseValue( self.combinedElementIndex)
			if freeElementIndex== self.combinedElementIndex:
				otherElementDerivativeValue= 1.0
			else:
				otherElementDerivativeValue= 0.0
		else:
			otherStack= self._stack._system.getOperationStack(
				stackSpecifier= ( self.combinedElementValueSource, self.combinedElementIndex),
			)
			otherStackLastOp= otherStack.getOperationAtIndex( -1)
			otherElementResultValue= otherStackLastOp.getResult()
			otherElementDerivativeValue= otherStackLastOp.getDerivative( freeElementIndex)

		return self.calculateDerivative(
			calculatedDerivative= inputDerivative,
			calculatedResult= inputResult,
			otherElementIsFreeElement= freeElementIndex== self.combinedElementIndex,
			otherElementCalculatedDerivative= otherElementDerivativeValue,
			otherElementCalculatedResult= otherElementResultValue,
		)

	@staticmethod
	def calculateResult(
		calculatedResult: float,
		otherElementResult: float,
	):
		raise NotImplementedError

	@staticmethod
	def calculateDerivative(
		calculatedDerivative: float,
		calculatedResult: float,
		otherElementIsFreeElement: bool,
		otherElementCalculatedDerivative: float,
		otherElementCalculatedResult: float,
	):
		raise NotImplementedError 

class Container( type):
	def __iter__( self):
		for thing in dir( self):
			if thing.startswith( "__")== False:
				yield getattr( self, thing)

class Operations:
	class Single( metaclass= Container):
		# def multWithRandNum( element):
		# 	randNum= fitRange( random.random(), 0, 1, -3.4, 6.12)
		# 	print( randNum)
		# 	return element* randNum
		# Bit too many wave-s for now, was dominating every stack
		# def cos( element):
		# 	return math.cos( element)
		# def tan( element):
		# 	return math.tan( element)
		# def powWithRandNum( element):
		# 	if element== 0:
		# 		return 0
		# 	else:
		# 		randNum= fitRange( random.random(), 0, 1, -5.2, 2.02)
		# 		print( randNum)
		# 		return element** randNum
		class MultWithNegOne( SingleOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
			)-> float:
				return calculatedResult* -1
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
			)-> float:
				return calculatedDerivative* -1
		class Sin( SingleOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
			)-> float:
				return math.sin( calculatedResult)
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
			)-> float:
				# TODO: Possible issue with system
				# This is abstracting the calculated component of the sin function and using the incremental rule for sensitivity calculation
				# It is posisble that this should not be performed when the calculated component of sin is not a composite of different function-s and is simply the free element ( when this is at index 0 of the stack)
				return math.cos( calculatedResult)* calculatedDerivative
	class Element( metaclass= Container):
		class Add( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				return calculatedResult+ otherElementResult
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	return calculatedDerivative+ 1.0
				# else:
				# 	return calculatedDerivative
				return calculatedDerivative+ otherElementCalculatedDerivative
		class Subtract( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				return calculatedResult- otherElementResult
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	return calculatedDerivative- 1.0
				# else:
				# 	return calculatedDerivative
				return calculatedDerivative- otherElementCalculatedDerivative
		class Mult( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				return calculatedResult* otherElementResult
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	# Sensitivity of output to solely element1 is 1.0
				# 	# Sensitivity of output to other multiplied number is `calculatedDerivative` 
				# 	# Result of element1 is element1
				# 	# Result of other multiplied number is 
				# 	# Using product rule
				# 	# TODO: understand how relationships come to form the product rule
				# 	return ( otherElementCalculatedDerivative* calculatedDerivative)+ ( calculatedResult* 1.0)
				# else:
				# 	return calculatedDerivative* otherElementCalculatedDerivative
				return ( calculatedResult* otherElementCalculatedDerivative)+ ( otherElementCalculatedResult* calculatedDerivative)

		class Divide( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				if otherElementResult== 0:
					return 0
				else:
					return calculatedResult/ otherElementResult
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	# See comment in Multiply.calculateDerivative
				# 	return safeDiv( ( ( otherElementCalculatedDerivative* calculatedDerivative)- ( calculatedResult* 1.0)), math.pow( otherElementCalculatedDerivative, 2))
				# else:
				# 	return safeDiv( calculatedDerivative, otherElementCalculatedDerivative)
				return safeDiv( ( ( otherElementCalculatedResult* calculatedDerivative)- ( calculatedResult* otherElementCalculatedDerivative)), math.pow( otherElementCalculatedResult, 2))
		class VecLen2D( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				return vecLen( ( calculatedResult, otherElementResult))
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	return ( 0.5* safePow( math.pow( calculatedResult, 2)+ math.pow( otherElementCalculatedDerivative, 2), -0.5))* ( ( 2* calculatedResult)+ ( 2* otherElementCalculatedDerivative))* calculatedDerivative

				# else:
				# 	return ( 0.5* safePow( math.pow( calculatedResult, 2)+ math.pow( otherElementCalculatedDerivative, 2), -0.5))* ( 2* calculatedResult)* calculatedDerivative
				return ( 0.5* safePow( math.pow( calculatedResult, 2)+ math.pow( otherElementCalculatedResult, 2), -0.5))* ( ( 2* calculatedResult* calculatedDerivative)+ ( 2* otherElementCalculatedResult* otherElementCalculatedDerivative))
		class Minimum( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				return min( calculatedResult, otherElementResult)
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if calculatedResult> otherElementCalculatedDerivative:
				# 	if otherElementIsFreeElement== True:
				# 		return 1.0
				# 	else:
				# 		return 0.0
				# elif calculatedResult== otherElementCalculatedDerivative:
				# 	if otherElementIsFreeElement== True:
				# 		return 1.0+ ( ( calculatedDerivative- 1.0)/ 2.0)
				# 	else:
				# 		return calculatedDerivative/ 2.0
				# else:
				# 	return calculatedDerivative
				if calculatedResult< otherElementCalculatedResult:
					return calculatedDerivative
				elif otherElementCalculatedResult< calculatedResult:
					return otherElementCalculatedDerivative
				else: # equal
					return lerp( calculatedDerivative, otherElementCalculatedDerivative, 0.5)
		class Pow( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				if calculatedResult== 0:
					if otherElementResult< 0:
						return 0.0
				elif calculatedResult< 0:
					( decimalComponent, _)= math.modf( otherElementResult)
					if decimalComponent!= 0.0:
						return 0.0
				return math.pow( calculatedResult, otherElementResult)
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	return ( math.pow( math.e, otherElementCalculatedDerivative* safeLog( calculatedResult, math.e)))* ( otherElementCalculatedDerivative* ( safeDiv( 1, calculatedResult)* calculatedDerivative)+ safeLog( calculatedResult, math.e))
				# else:
				# 	return ( otherElementCalculatedDerivative* safePow( calculatedResult, otherElementCalculatedDerivative- 1.0))* calculatedDerivative

				# New pow calc may be faster than cache retrieval
				if calculatedResult== 0:
					if otherElementCalculatedResult< 0:
						return 0.0
				elif calculatedResult< 0:
					( decimalComponent, _)= math.modf( otherElementCalculatedResult)
					if decimalComponent!= 0.0:
						return 0.0
				return math.pow( calculatedResult, otherElementCalculatedResult)* ( ( otherElementCalculatedResult* safeDiv( 1, calculatedResult)* calculatedDerivative)+ ( safeLog( calculatedResult, math.e)* otherElementCalculatedDerivative))


premadeFunctions= {
	"tt": ( 3, (
		{
			0: [ Operations.Element.VecLen2D(
				combinedElementIndex= 1,
				combinedElementValueSource= ( ElementValueSourceType.UTILITY, 0),
			)],
		},
		[
			{
			# 	2: [ Operations.Element.Mult(
			# 		combinedElementIndex= 1,
			# 		combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
			# 	)],
				1: [ Operations.Element.Mult(
					combinedElementIndex= 2,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				)],
			},
		],
	)),
	"Looping multiplication": ( 3, (
		{
			0: [ Operations.Element.Mult(
				combinedElementIndex= 1,
				combinedElementValueSource= ( ElementValueSourceType.UTILITY, 0),
			)],
		},
		[
			{
				2: [ Operations.Element.Mult(
					combinedElementIndex= 1,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				)],
				1: [ Operations.Element.Mult(
					combinedElementIndex= 2,
					combinedElementValueSource= ( ElementValueSourceType.UTILITY, 0),
				)],
			},
		],
	)),
	"Bwokie": ( 3, (
		{
			1: [
				Operations.Single.Sin(),
				Operations.Element.Mult(
					combinedElementIndex= 0,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				),
			],
			2: [
				Operations.Element.Minimum(
					combinedElementIndex= 1,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				),
				Operations.Element.Pow(
					combinedElementIndex= 1,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				),
			],
		},
		[],
	)),
}

def createRandomBaseFunction(
	elementNumber: int,
)-> System.BaseFunctionSpecification:
	systemAffectors= dict()
	utilitySpaces= list()

	startingElementSize= random.randrange( 2, elementNumber)
	startingElementPool= set()
	nonStartingElementPool= set( range( elementNumber))
	for _ in list( range( startingElementSize)):
		startingElement= random.choice( list( nonStartingElementPool))
		nonStartingElementPool.remove( startingElement)
		startingElementPool.add( startingElement)

	startingElements= list( startingElementPool)
	# Order does not matter if adding all starting tower-s,
	# If order does matter can use SequenceAndMappingContainer
	function= dict()
	for startingElement in startingElements:
		singleOperations= random.randrange( 0, 5)
		elementOperations= random.randrange( 0, 8)
		choices= list()
		if singleOperations> 0:
			choices.append( Operations.Single)
		if elementOperations> 0:
			choices.append( Operations.Element)
		operationSequence= list()
		while ( singleOperations+ elementOperations)> 0:
			choice= random.choice( choices)
			if choice== Operations.Single:
				operationSequence.append( random.choice( list( Operations.Single))())
				singleOperations-= 1
				if singleOperations== 0:
					choices.remove( Operations.Single)
			elif choice== Operations.Element:
				if len( nonStartingElementPool)> 0:
					otherElementIndex= random.choice( list( nonStartingElementPool))
					nonStartingElementPool.remove( otherElementIndex)
					startingElementPool.add( otherElementIndex)
				else:
					otherElementIndex= random.choice( list( startingElementPool))

				operationSequence.append( random.choice( list( Operations.Element))(
					combinedElementIndex= otherElementIndex,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				))
				# operationSequence.append( ( OperationType.ELEMENT, ( random.choice( list( Operations.Element)), otherElementIndex)))
				elementOperations-= 1
				if elementOperations== 0:
					choices.remove( Operations.Element)
		function[ startingElement]= operationSequence

	systemAffectors= function

	return (
		systemAffectors,
		utilitySpaces,
	)



# def applyFunction(
# 	elementValues: list[ float],
# 	function: ConstructedFunction,
# )-> float:
# 	totalValue= 0.0
# 	for startingElementIndex in function:
# 		startingValue= elementValues[ startingElementIndex]
# 		for ( opType, opData) in function[ startingElementIndex]:
# 			try:
# 				match opType:
# 					case OperationType.SINGLE:
# 						op= opData
# 						startingValue= op.getResult(
# 							element= startingValue,
# 						).real
# 					case OperationType.ELEMENT:
# 						( op, otherElementIndex)= opData
# 						otherElementValue= elementValues[ otherElementIndex]
# 						startingValue= op.getResult(
# 							element0= startingValue,
# 							element1= otherElementValue,
# 						).real
# 			except Exception as e:
# 				print( f"Error raised on application with { startingValue} as an input")
# 				raise e
# 		totalValue+= startingValue
# 	return totalValue

# def deriveFunction(
# 	freeElementIndex: int,
# 	elementValues: list[ float],
# 	function: ConstructedFunction,
# )-> float:
# 	otherStacks= set( function.keys())
# 	if freeElementIndex in function:
# 		ownStack= function[ freeElementIndex]
# 		otherStacks.remove( freeElementIndex)
# 		# Loop over own stack
# 		calcResult= elementValues[ freeElementIndex]
# 		calcDeriv= 1.0
# 		for ( opType, opData) in ownStack:
# 			match opType:
# 				case OperationType.SINGLE:
# 					op= opData
# 					try:
# 						calcDeriv= op.getDerivative(
# 							calculatedDerivative= calcDeriv,
# 							calculatedResult= calcResult,
# 						)
# 						calcResult= op.getResult(
# 							element= calcResult,
# 						).real
# 					except Exception as e:
# 						print( f"Error with { op.__name__} with\ncalcDeriv: { calcDeriv}\ncalcResult: { calcResult}")
# 						raise e
# 				case OperationType.ELEMENT:
# 					( op, otherElementIndex)= opData
# 					otherElementValue= elementValues[ otherElementIndex]
# 					try:
# 						calcDeriv= op.getDerivative(
# 							calculatedDerivative= calcDeriv,
# 							calculatedResult= calcResult,
# 							element1IsSelf= otherElementIndex== freeElementIndex,
# 							element1= otherElementValue,
# 						)
# 						calcResult= op.getResult(
# 							element0= calcResult,
# 							element1= otherElementValue,
# 						).real
# 					except Exception as e:
# 						print( f"Error with { op.__name__} with\ncalcDeriv: { calcDeriv}\ncalcResult: { calcResult}\notherElementValue: { otherElementValue}\notherElementIsSelf: { otherElementIndex== freeElementIndex}")
# 						raise e
# 	else:
# 		calcDeriv= 0.0
	
# 	# Loop over other stack-s
# 	for otherStackElementIndex in otherStacks:
# 		stack= function[ otherStackElementIndex]
# 		freeElementContributes= False
# 		for ( stackIndex, ( opType, opData)) in enumerate( stack):
# 			if opType== OperationType.ELEMENT:
# 				if opData[ 1]== freeElementIndex:
# 					freeElementContributes= True
# 					startStackIndex= stackIndex
# 					break
# 		if freeElementContributes== False:
# 			continue
# 		# Only calculate added calculation component-s if they are influenced by the free element

# 		stackCalcDeriv= 0.0
# 		# Calculate result up to stack start
# 		stackCalcResult= elementValues[ otherStackElementIndex]
# 		for ( opType, opData) in stack[ : startStackIndex]:
# 			match opType:
# 				case OperationType.SINGLE:
# 					op= opData
# 					stackCalcResult= op.getResult(
# 						element= stackCalcResult,
# 					).real
# 				case OperationType.ELEMENT:
# 					( op, otherElementIndex)= opData
# 					otherElementValue= elementValues[ otherElementIndex]
# 					stackCalcResult= op.getResult(
# 						element0= stackCalcResult,
# 						element1= otherElementValue,
# 					).real

# 		for ( opType, opData) in stack[ startStackIndex: ]:
# 			match opType:
# 				case OperationType.SINGLE:
# 					op= opData
# 					try:
# 						stackCalcDeriv= op.getDerivative(
# 							calculatedDerivative= stackCalcDeriv,
# 							calculatedResult= stackCalcResult,
# 						)
# 						stackCalcResult= op.getResult(
# 							element= stackCalcResult,
# 						).real
# 					except Exception as e:
# 						print( f"Error with { op.__name__} with\ncalcDeriv: { stackCalcDeriv}\ncalcResult: { stackCalcResult}")
# 						raise e
# 				case OperationType.ELEMENT:
# 					( op, otherElementIndex)= opData
# 					otherElementValue= elementValues[ otherElementIndex]
# 					try:
# 						stackCalcDeriv= op.getDerivative(
# 							calculatedDerivative= stackCalcDeriv,
# 							calculatedResult= stackCalcResult,
# 							element1IsSelf= otherElementIndex== freeElementIndex,
# 							element1= otherElementValue,
# 						)
# 						stackCalcResult= op.getResult(
# 							element0= stackCalcResult,
# 							element1= otherElementValue,
# 						).real
# 					except Exception as e:
# 						print( f"Error with { op.__name__} with\ncalcDeriv: { stackCalcDeriv}\ncalcResult: { stackCalcResult}\notherElementValue: { otherElementValue}\notherElementIsSelf: { otherElementIndex== freeElementIndex}")
# 						raise e

# 		calcDeriv+= stackCalcDeriv
# 	return calcDeriv

def displayResult(
	system: System,
	sessionState: dict,
):
	result= system.getRating()
	sessionState[ "label-s"][ "System rating"].set_label( str( result))
	for elementIndex in range( len( system.elements)):
		outputSensitivity= system.getSensitivityToElement( elementIndex)
		sessionState[ "label-s"][ "Output sensitivity to element-s"][ elementIndex].set_label( str( outputSensitivity))

def getNewResultsDisplay(
	system: System,
	sessionState: dict,
)-> Gtk.Widget:
	vbox0= Gtk.Box.new( Gtk.Orientation.VERTICAL, 7)
	resultHBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 7)
	resultHeading= Gtk.Label.new( "System rating:")
	resultHeading.set_halign( Gtk.Align.START)
	resultHeading.add_css_class( "caption-heading")
	resultHBox.append( resultHeading)
	resultLabel= Gtk.Label.new()
	resultLabel.set_halign( Gtk.Align.END)
	resultLabel.set_hexpand( True)
	resultLabel.add_css_class( "caption")
	resultLabel.add_css_class( "dim-label")
	resultHBox.append( resultLabel)
	vbox0.append( resultHBox)
	vbox0.append( Gtk.Separator.new( Gtk.Orientation.VERTICAL))
	sensHeading= Gtk.Label.new( "System rating sensitivity to each element:")
	sensHeading.set_halign( Gtk.Align.START)
	sensHeading.add_css_class( "caption-heading")
	vbox0.append( sensHeading)
	sensVBox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 3)
	indexMapping= dict()
	for elementIndex in range( len( system.elements)):
		sensHBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 3)
		sensIndexLabel= Gtk.Label.new( str( elementIndex)+ ":")
		sensIndexLabel.set_halign( Gtk.Align.START)
		sensIndexLabel.add_css_class( "caption")
		sensHBox.append( sensIndexLabel)
		sensitivityLabel= Gtk.Label.new()
		sensitivityLabel.set_halign( Gtk.Align.END)
		sensitivityLabel.set_hexpand( True)
		sensitivityLabel.add_css_class( "caption")
		sensitivityLabel.add_css_class( "dim-label")
		sensHBox.append( sensitivityLabel)
		sensVBox.append( sensHBox)
		indexMapping[ elementIndex]= sensitivityLabel
	vbox0.append( sensVBox)
	sessionState[ "label-s"][ "System rating"]= resultLabel
	sessionState[ "label-s"][ "Output sensitivity to element-s"]= indexMapping

	return vbox0
def getNewFunctionDisplay(
	system: System,
)-> Gtk.Widget:

	def populateSpace(
		space: System.Space,
	):
		elementsVBox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 10)
		firstElement= True
		for rootElementIndex in space:
			if firstElement== True:
				firstElement= False
			else:
				elementSep= Gtk.Separator.new( Gtk.Orientation.HORIZONTAL)
				elementsVBox.append( elementSep)
			operationStack: OperationStack= space[ rootElementIndex]
			elementHBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 5)
			elementIndexLabel= Gtk.Label.new( str( rootElementIndex))
			elementIndexLabel.set_halign( Gtk.Align.CENTER)
			elementIndexLabel.set_valign( Gtk.Align.START)
			elementHBox.append( elementIndexLabel)
			indexStackSep= Gtk.Separator.new( Gtk.Orientation.VERTICAL)
			elementHBox.append( indexStackSep)

			stackOpsVbox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 1)
			firstOp= True
			for operation in operationStack.iterOperations():
				if firstOp== True:
					firstOp= False
				else:
					opSep= Gtk.Separator.new( Gtk.Orientation.HORIZONTAL)
					stackOpsVbox.append( opSep)
				opHBox0= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 4)
				opTypeLabelBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 2)
				opTypeTypeLabel= Gtk.Label.new( "opType:")
				opTypeTypeLabel.add_css_class( "caption")
				opTypeTypeLabel.add_css_class( "dim-label")
				opTypeLabelBox.append( opTypeTypeLabel)
				opTypeLabel= Gtk.Label.new( operation.__class__.__base__.__name__)
				opTypeLabel.add_css_class( "caption-heading")
				opTypeLabel.add_css_class( "dim-label")
				opTypeLabel.set_halign( Gtk.Align.START)
				opTypeLabelBox.append( opTypeLabel)
				opHBox0.append( opTypeLabelBox)
				opHBoxSep= Gtk.Separator.new( Gtk.Orientation.HORIZONTAL)
				opHBox0.append( opHBoxSep)
				opHBox1= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 15)
				# opHBox1.set_halign( Gtk.Align.CENTER)
				opHBox1.set_hexpand( True)
				# opHBox1.set_homogeneous( True)
				if isinstance( operation, SingleOperation):
					opClass= operation.__class__
					opClassLabelBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 2)
					opClassTypeLabel= Gtk.Label.new( "op:")
					opClassTypeLabel.add_css_class( "caption")
					opClassTypeLabel.add_css_class( "dim-label")
					opClassLabelBox.append( opClassTypeLabel)
					opClassLabel= Gtk.Label.new( opClass.__name__)
					opClassLabel.add_css_class( "caption-heading")
					opClassLabel.add_css_class( "dim-label")
					opClassLabelBox.append( opClassLabel)
					opHBox1.append( opClassLabelBox)
				if isinstance( operation, ElementOperation):
					operation: ElementOperation
					opClass= operation.__class__
					opClassLabelBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 2)
					opClassTypeLabel= Gtk.Label.new( "op:")
					opClassTypeLabel.add_css_class( "caption")
					opClassTypeLabel.add_css_class( "dim-label")
					opClassLabelBox.append( opClassTypeLabel)
					opClassLabel= Gtk.Label.new( opClass.__name__)
					opClassLabel.add_css_class( "caption-heading")
					opClassLabel.add_css_class( "dim-label")
					opClassLabelBox.append( opClassLabel)
					opHBox1.append( opClassLabelBox)
					otherElementIndexLabelBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 2)
					otherElementIndexTypeLabel= Gtk.Label.new( "otherElementIndex:")
					otherElementIndexTypeLabel.add_css_class( "caption")
					otherElementIndexTypeLabel.add_css_class( "dim-label")
					otherElementIndexLabelBox.append( otherElementIndexTypeLabel)
					otherElementIndexLabel= Gtk.Label.new( str( operation.combinedElementIndex))
					otherElementIndexLabel.add_css_class( "caption-heading")
					otherElementIndexLabel.add_css_class( "dim-label")
					otherElementIndexLabelBox.append( otherElementIndexLabel)
					opHBox1.append( otherElementIndexLabelBox)
					otherElementLocationLabelBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 2)
					otherElementLocationTypeLabel= Gtk.Label.new( "otherElementLocation:")
					otherElementLocationTypeLabel.add_css_class( "caption")
					otherElementLocationTypeLabel.add_css_class( "dim-label")
					otherElementLocationLabelBox.append( otherElementLocationTypeLabel)
					if ( operation.combinedElementValueSource== ElementValueSourceType.SYSTEM_RATING_AFFECTOR)or ( operation.combinedElementValueSource== ElementValueSourceType.BASE_VALUE):
						otherElementSourceStr= operation.combinedElementValueSource.name
					else:
						otherElementSourceStr= f"( { operation.combinedElementValueSource[ 0].name}, { str( operation.combinedElementValueSource[ 1])})"
					otherElementLocationLabel= Gtk.Label.new( otherElementSourceStr)
					otherElementLocationLabel.add_css_class( "caption-heading")
					otherElementLocationLabel.add_css_class( "dim-label")
					otherElementLocationLabelBox.append( otherElementLocationLabel)
					opHBox1.append( otherElementLocationLabelBox)
				opHBox0.append( opHBox1)
				stackOpsVbox.append( opHBox0)
			elementHBox.append( stackOpsVbox)
			elementsVBox.append( elementHBox)
		return elementsVBox
	stackBox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 5)
	systemContribTitle= Gtk.Label.new( "Stack contributing to system rating:")
	systemContribTitle.add_css_class( "heading")
	systemContribTitle.set_halign( Gtk.Align.START)
	stackBox.append( systemContribTitle)
	systemContribWidget= populateSpace( system.function[ 0])
	stackBox.append( systemContribWidget)
	for ( utilSpaceIndex, utilSpace) in enumerate( system.function[ 1]):
		utilSpaceTitle= Gtk.Label.new( f"Util space { utilSpaceIndex}:")
		utilSpaceTitle.add_css_class( "heading")
		utilSpaceTitle.set_halign( Gtk.Align.START)
		stackBox.append( utilSpaceTitle)
		utilSpaceWidget= populateSpace( utilSpace)
		stackBox.append( utilSpaceWidget)
	return stackBox


def elementNumberUpdated(
	elementNumberWidget,
	elementNumber: int,
	system: System,
	elementValueEditor,
	resultsBin: Adw.Bin,
	resultsValueDisplays: dict,
):
	# Construct new result-s pane
	# Alter element-s in system
	existingValues= system.elements.copy()
	currentElementNumber= len( existingValues)
	if currentElementNumber!= elementNumber:
		if currentElementNumber> elementNumber:
			newValues= existingValues[ : elementNumber]
		else:
			newValues= existingValues+ [ 0.0 for _ in elementNumber- currentElementNumber]
		system.setElements( newValues)
		elementValueEditor.setValue( newValues)
		widget= getNewResultsDisplay( system, resultsValueDisplays)
		resultsBin.set_child( widget)

def updateElementNumber(
	newElementNumber: int,
	oldElementValues: list[ float],
)-> list[ float]:
	oldELementNumber= len( oldElementValues)
	if oldELementNumber> newElementNumber:
		newValues= oldElementValues[ : newElementNumber]
	else:
		newValues= oldElementValues+ [ 0.0 for _ in range( newElementNumber- oldELementNumber)]
	return newValues
	

def calculateNewGraphDisplay(
	system: System,
	sessionState: "SessionState",
	desiredElementIndex: int,
):
	# Display:
	# - System result
	# - System sensitivity to element ( calculated)
	# - System sensitivity to element ( estimated ( central difference))

	currentElementValue= system.elements[ desiredElementIndex]
	settings= sessionState[ "graphSetting-s"]
	displayAmount= settings.displayAmount
	displayDivisions= settings.displayDivisions
	# calculate start and then after each step
	calculationStart= currentElementValue- ( displayAmount/ 2)
	calculationStepCount= displayDivisions+ 1
	calculationStep= displayAmount/ calculationStepCount
	differenceApproxSize= calculationStep* 2

	locationArray= list()
	valueArray= list()
	for calculationIndex in range( 1+ calculationStepCount):
		distanceFromStart= calculationIndex* calculationStep
		calculationLocation= calculationStart+ distanceFromStart
		locationArray.append( calculationLocation)

		system.setElementValue(
			index= desiredElementIndex,
			value= calculationLocation,
		)
		systemRating= system.getRating()
		sRSensitivity= system.getSensitivityToElement(
			freeElementIndex= desiredElementIndex,
		)
		if calculationIndex> 1:
			# calculate central difference of that behind the current
			difference= ( systemRating- valueArray[ calculationIndex- 2][ 0])/ differenceApproxSize
			valueArray[ calculationIndex- 1][ 2]= difference

		valueArray.append( [ systemRating, sRSensitivity, 0.0])
	# Add on central difference to array for first and last
	if len( valueArray)> 2:
		valueArray[ 0][ 2]= valueArray[ 1][ 2]
		valueArray[ -1][ 2]= valueArray[ -2][ 2]
	valueNPArray= numpy.array( valueArray)
	sessionState[ "graph"].axis.clear()
	( sRLine, sRSensLineCalc, sRSensLineApprox)= sessionState[ "graph"].axis.plot( locationArray, valueNPArray)
	sessionState[ "graph"].axis.legend( ( sRLine, sRSensLineCalc, sRSensLineApprox), ( "System rating", "Rating sensitivity ( calculated)", "Rating sensitivity ( approximated)"))
	sessionState[ "graph"].figureCanvas.draw()

	system.setElementValue(
		index= desiredElementIndex,
		value= currentElementValue
	)

def graphInvalidation(
	system: System,
	sessionState: "SessionState",
):
	desiredElementIndex= sessionState[ "graphSetting-s"].viewedElement
	if desiredElementIndex!= None:
		calculateNewGraphDisplay(
			system= system,
			sessionState= sessionState,
			desiredElementIndex= desiredElementIndex,
		)

@specificationed
class GraphSettings:
	displayAmount: float= 28.12102
	displayDivisions: int= 400
	viewedElement: int| None= 0


LabelResultState= TypedDict( "LabelResultState", {
	"System rating": Gtk.Label,
	"Output sensitivity to element-s": list[ Gtk.Label],
})
SessionState= TypedDict( "SessionState", {
	"label-s": LabelResultState,
	"graph": DataVisualiser,
	"graphSetting-s": GraphSettings,
})

def unconstainedLeversUiGen()-> Gtk.Widget:
	global premadeFunctions
	hpane0= NicerPaned( Gtk.Orientation.HORIZONTAL)
	hpane0.set_shrink_end_child( False)
	hpane0.set_shrink_start_child( False)
	vbox0= Gtk.Box.new( Gtk.Orientation.VERTICAL, 8)
	# elementNumberPicker= Gtk.SpinButton.new( None, 1.0, 1)
	elementsTitle= Gtk.Label.new( "Number of element-s")
	elementsTitle.add_css_class( "caption")
	elementsTitle.add_css_class( "dim-label")
	elementsTitle.set_halign( Gtk.Align.START)
	vbox0.append( elementsTitle)
	elementNumberPicker= getEditingWidgetList( int)[ 1][ 0]()
	elementNumberPicker.setMinimum( 3)
	vbox0.append( elementNumberPicker)
	# alterElementNumberButton= Gtk.Button.new_with_label( "Alter element number")
	# vbox0.append( alterElementNumberButton)
	vbox0.append( Gtk.Separator.new( Gtk.Orientation.HORIZONTAL))
	elementValueTitle= Gtk.Label.new( "Value of element-s")
	elementValueTitle.add_css_class( "caption")
	elementValueTitle.add_css_class( "dim-label")
	elementValueTitle.set_halign( Gtk.Align.START)
	vbox0.append( elementValueTitle)
	elementValueEditor= getEditingWidgetList( tuple[ float])[ 1][ 0]()
	elementValueEditor.set_vexpand( True)
	# elementValuesPlaceholderBin= Gtk.ScrolledWindow.new()
	# elementValuesPlaceholderBin.set_vexpand( True)
	vbox0.append( elementValueEditor)
	hpane0.set_start_child( vbox0)
	hpane1= NicerPaned( Gtk.Orientation.HORIZONTAL)
	hpane1.set_shrink_end_child( False)
	hpane1.set_shrink_start_child( False)
	functionVbox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 7)
	functionCreationBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 4)
	premadeFunctionSelectionButton= Gtk.MenuButton.new()
	premadeFunctionSelectionButton.set_label( "Select premade function")
	premadeFunctionSelectionToggleButton= premadeFunctionSelectionButton.get_first_child()
	premadeFunctionSelectionToggleButton.add_css_class( "pill")
	premadeFunctionSelectionToggleButton.add_css_class( "thinner")
	premadeFunctionSelectionToggleButton.add_css_class( "caption")
	premadeFunctionSelectionButton.set_halign( Gtk.Align.START)
	premadeFunctionSelectionButton.set_valign( Gtk.Align.CENTER)
	premadeFunctionSelection= ListViewPopover()
	premadeFunctionSelection.setItems( premadeFunctions.keys())
	premadeFunctionSelectionButton.set_popover( premadeFunctionSelection)
	functionCreationBox.append( premadeFunctionSelectionButton)
	...
	orSep= Gtk.Box.new( Gtk.Orientation.VERTICAL, 3)
	orSepSep0= Gtk.Separator.new( Gtk.Orientation.VERTICAL)
	orSepSep0.set_halign( Gtk.Align.CENTER)
	orSepSep0.set_size_request( -1, 22)
	orSep.append( orSepSep0)
	orSepLabel= Gtk.Label.new( "or")
	orSepLabel.add_css_class( "caption")
	orSepLabel.add_css_class( "dim-label")
	orSep.append( orSepLabel)
	orSepSep1= Gtk.Separator.new( Gtk.Orientation.VERTICAL)
	orSepSep1.set_halign( Gtk.Align.CENTER)
	orSepSep1.set_size_request( -1, 22)
	orSep.append( orSepSep1)
	orSep.set_valign( Gtk.Align.CENTER)
	functionCreationBox.append( orSep)
	randomFunctionButton= Gtk.Button.new_with_label( "Create random function")
	randomFunctionButton.add_css_class( "pill")
	randomFunctionButton.add_css_class( "thinner")
	randomFunctionButton.add_css_class( "caption")
	randomFunctionButton.set_halign( Gtk.Align.START)
	randomFunctionButton.set_valign( Gtk.Align.CENTER)
	functionCreationBox.append( randomFunctionButton)
	functionCreationBox.set_halign( Gtk.Align.CENTER)
	functionCreationBox.set_valign( Gtk.Align.CENTER)
	functionVbox.append( functionCreationBox)
	functionVbox.append( Gtk.Separator.new( Gtk.Orientation.VERTICAL))
	beep= Gtk.Label.new( "Complex function viewing and editing is disabled")
	beep.add_css_class( "caption")
	beep.add_css_class( "dim-label")
	functionVbox.append( beep)
	# functionViewer= getEditingWidgetList( dict[ int, Any])[ 1][ 0]()
	# functionViewer.set_sensitive( False)
	# functionViewer= Gtk.Label.new()
	# functionViewer.add_css_class( "caption")
	# functionViewer.add_css_class( "dim-label")
	functionViewScroll= Gtk.ScrolledWindow.new()
	functionViewScroll.set_vexpand( True)
	functionViewScroll.set_propagate_natural_width( True)
	functionVbox.append( functionViewScroll)
	hpane1.set_start_child( functionVbox)
	...
	resultVbox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 8)
	resultBin= Adw.Bin.new()
	resultVbox.append( resultBin)
	graph= DataVisualiser()
	resultVbox.append( graph)
	gSButton= Gtk.MenuButton.new()
	gSPopover= Gtk.Popover.new()
	graphSettingsEditor= getEditingWidgetList( GraphSettings)[ 1][ 0]()
	initialGraphSettings= GraphSettings()
	graphSettingsEditor.setValue( initialGraphSettings)
	gSPopover.set_child( graphSettingsEditor)
	gSButton.set_popover( gSPopover)
	resultVbox.append( gSButton)
	hpane1.set_end_child( resultVbox)
	hpane0.set_end_child( hpane1)

	# Upon initialisation
	#    set system elements, set system function, set element number editor, set element value editor, create func widget, create results widget, update displayed result-s
	# When element number updated:
	#    update element value editor, update system elements, re calculate the result widget, recalculate the displayed results, increment system iteration( for cache purposes)
	# When element values update-ed
	#    recalculate the displayed result-s, update system elements, increment system iteration
	# when new function created
	#    if updating element number
	#        set system elements, set system function, change element num editor, change element value editor, recreate results widget, recreate function widget, recalculate display, increment system iteration
	#    else
	#        set system function, recreate function widget, recalculate display, increment system iteration
	

	sessionState= {
		"label-s": dict(),
		"graph": graph,
		"graphSetting-s": initialGraphSettings,
	}
	# Default setup, 3 element-s, no function
	defaultElementNumber= 3
	elementNumberPicker.setValue( defaultElementNumber)
	elements= [ 0.0 for _ in range( defaultElementNumber)]
	system= System(
		elementValues= elements,
		baseFunction= ( {}, []),
	)
	elementValueEditor.setValue( elements)
	updateFunctionView= lambda: functionViewScroll.set_child( getNewFunctionDisplay( system))
	updateFunctionView()
	updateResultDisplay= lambda: resultBin.set_child( getNewResultsDisplay(
		system= system,
		sessionState= sessionState,
	))
	updateResultDisplay()
	displayResult(
		system= system,
		sessionState= sessionState,
	)


	def elementNumberPickerUpdated( picker, newNumber):
		nonlocal system, sessionState, elementValueEditor
		currentNumber= len( system.elements)
		if currentNumber== newNumber:
			return
		newValues= updateElementNumber(
			newElementNumber= newNumber,
			oldElementValues= system.elements
		)
		elementValueEditor.setValue( newValues)
		system.setElements( newValues)
		updateResultDisplay()
		displayResult(
			system= system,
			sessionState= sessionState,
		)
		graphInvalidation(
			system= system,
			sessionState= sessionState,
		)

	elementNumberPicker.connect( "valueUpdated", elementNumberPickerUpdated)
	def elementValueEditorUpdated( editor, newValuesTuple: tuple[ float]):
		nonlocal system, sessionState
		newValues= list( newValuesTuple)
		graphViewedElement= sessionState[ "graphSetting-s"].viewedElement
		if graphViewedElement!= None:
			for elementIndex in range( len( system.elements)):
				if newValues[ elementIndex]!= system.elements[ elementIndex]:
					if elementIndex!= graphViewedElement:
						# Graph is invalid
						graphInvalidation(
							system= system,
							sessionState= sessionState,
						)
						break
		system.setElements( newValues)
		displayResult(
			system= system,
			sessionState= sessionState,
		)
	elementValueEditor.connect( "valueUpdated", elementValueEditorUpdated)

	def randomFunctionOnClick( button):
		nonlocal elementNumberPicker, system, sessionState
		newBaseFunc= createRandomBaseFunction(
			elementNumber= elementNumberPicker.getValue(),
		)
		system.setFunctionFromBase( newBaseFunc)
		print( system.function)
		updateFunctionView()
		displayResult(
			system= system,
			sessionState= sessionState,
		)
		graphInvalidation(
			system= system,
			sessionState= sessionState,
		)
	randomFunctionButton.connect( "clicked", randomFunctionOnClick)

	def premadeFunctionOnClick( selectedItem):
		nonlocal system, elementValueEditor, elementNumberPicker
		( desiredElementNumber, newBaseFunc)= premadeFunctions[ selectedItem]
		currentElementNumber= elementNumberPicker.getValue()
		if desiredElementNumber!= currentElementNumber:
			newValues= updateElementNumber(
				newElementNumber= desiredElementNumber,
				oldElementValues= system.elements,
			)
			elementNumberPicker.setValue( desiredElementNumber)
			elementValueEditor.setValue( newValues)
			system.setElements( newValues)
			updateResultDisplay()
		newBaseFunc= deepcopy( newBaseFunc)
		system.setFunctionFromBase( newBaseFunc)
		updateFunctionView()
		displayResult(
			system= system,
			sessionState= sessionState,
		)
		graphInvalidation(
			system= system,
			sessionState= sessionState,
		)
	premadeFunctionSelection.connectSignalWithFunction( "itemChosen", premadeFunctionOnClick)

	def graphSettingsUpdated(
		editor,
		newValue: GraphSettings,
	):
		nonlocal sessionState, system
		sessionState[ "graphSetting-s"]= newValue
		graphInvalidation(
			system= system,
			sessionState= sessionState,
		)
	graphSettingsEditor.connect( "valueUpdated", graphSettingsUpdated)

	return hpane0
