---
mainfont: CodeNewRomanNerdFont-Regular.otf
monofont: CodeNewRomanNerdFontMono-Regular.otf
header-includes:
    - \usepackage{setspace}
---
# Concepts code demonstration: Lucy Coleclough
These are code files which are designed to convince the evaluator that I can operate competantly on the course,  
They can be run with a python intepreter which supports version 3.10 or more recent.  
This hopefully eleviates the time needed to evaluate my competancy in the subjects.  
The files: { OptimisationDifferenciation, SiteOptimisation} rely upon my larger software stack and will not run, however the others operate with solely the python standard library.  
If matplotlib and pyqt are installed then ProbabilityConcepts will use them to plot examples.  
The pdf formatting may make running the files difficult, they are available in pure form at this location: "https://gitlab.com/lucyCole/CodeDemo/-/archive/main/CodeDemo-main.zip?path=items"    

- [Back Propagation Neural Net](#back-propagation-neural-net)
- [LP And Stochastic](#lp-and-stochastic)
- [Optimisation Differenciation](#optimisation-differenciation)
- [Probability Concepts](#probability-concepts)
- [Site Optimisation](#site-optimisation)

---
  
Back Propagation Neural Net
---
This was done using the book, "An introduction to neural computing" by Aleksander and Morton.  
It is a feed forward network that trains via back propagation, the activation function for all neurons is a sigmoid function.  
  

---

\normalsize
~~~~~{.python .numberLines}
"""
A feed forward back propagation neural net creator

Generate the training data from boolan ops
create the network with random values
loop until trained
"""


from pprint import pprint
from dataclasses import dataclass, field
from typing import Callable, ForwardRef, NewType, ClassVar, Union, List, Tuple
from math import exp
import random

class Globals:
	trainingData: list[ tuple[ tuple[ int, int, int], tuple[ int]]]= list()
	momentum: float= 0.9
	trainingRate: float= 0.5
	weightInitialisationRange: Tuple[ float, float]= ( -1, 1)
	weightInitialisationRandomSeed: float= 0.0
	acceptableErrorThreshold: float= 0.001
	hiddenLayersQuantity: int= 2
	neuronsInEachHiddenLayerQuantity: int= 8

# Define gates for training data generation
print( "\033c")
input( "Press enter to see the training data:")
print( "\n")
logicGates= {
	"AND": lambda a, b: ( a== True) and ( b== True),
	"OR": lambda a, b: ( a== True) or ( b== True),  
	"XOR": lambda a, b: a!= b,
	"NOR": lambda a, b: ( not a) and ( not b),
	# "NAND": lambda a, b: not ( ( a== True) and ( b== True)),
	# "XNOR": lambda a, b: a== b,
}
logicGateNamesOrdered= tuple( logicGates.keys())
logicGateValuesOrdered= tuple( logicGates.values())

# Generate training data

for index, logicGate in enumerate( logicGateValuesOrdered):
	for a in range( 2):
		for b in range( 2):
			Globals.trainingData.append( ( ( index, a, b), ( int( logicGate( a, b)), )))

pprint( Globals.trainingData)
print( "")
print( "Different logic gates are being trained")
print( "This is in the format: ( ( gateType, a, b), ( expectedResult))")
# alternateTrainingSet= [
# 	((2.7810836,2.550537003),( 1, 0)),
# 	((1.465489372,2.362125076),(1, 0)),
# 	((3.396561688,4.400293529),(1, 0)),
# 	((1.38807019,1.850220317),(1, 0)),
# 	((3.06407232,3.005305973),(1, 0)),
# 	((7.627531214,2.759262235),( 0, 1)),
# 	((5.332441248,2.088626775),( 0, 1)),
# 	((6.922596716,1.77106367),( 0, 1)),
# 	((8.675418651,-0.242068655),( 0, 1)),
# 	((7.673756466,3.508563011),( 0, 1))
# ]
# Globals.trainingData= alternateTrainingSet

@dataclass
class Weight():
	fromNeuron: ForwardRef( "Neuron")
	toNeuron: ForwardRef( "Neuron")
	weightValue: float
	previousWeightDelta: float= field( default= 0, init= False)
	prevChange: Union[ float, None]= field( default= None, init= False)

WeightLayer= NewType( "WeightLayer", list[ Weight])

def sigmoidFunction( neuron, activationEnergy: float)-> float:
	return 1/ ( 1+ exp( -1* activationEnergy))

def derivedSigmoidFunction( neuron, activationEnergy: float)-> float:
	return activationEnergy* ( 1- activationEnergy)

@dataclass
class Neuron():
	activationFunction: ClassVar[ Callable[ [ float], float]]= sigmoidFunction
	derivedActivationFunction: ClassVar[ Callable[ [ float], float]]= derivedSigmoidFunction

	threshold: float # Apparently the same as bias, 
	netInput: float= field( default= 0.0, init= False)
	error: float= field( default= 0.0, init= False)
	calculatedOutput: Union[ float, None]= field( default= None, init= False)
	bPHelper0: float= field( default= 0.0, init= False)


incrementingSeed= Globals.weightInitialisationRandomSeed

def getRandomValueInRange():
	global incrementingSeed
	random.seed( incrementingSeed)
	value= random.random()
	value*= Globals.weightInitialisationRange[ 1]- Globals.weightInitialisationRange[ 0] # Make it as big as the range
	value+= Globals.weightInitialisationRange[ 0] # Shift it to the correct position

	incrementingSeed+= 1

	return value



def constructNetwork( hiddenLayersQuantity: int, neuronsInEachHiddenLayerQuantity: int):
	neuronLayers: list[ list[ Neuron]]= list()
	weightLayers: list[ list[ Weight]]= list()

	# Construct neuron layers

	currentLayer= list()
	for neuronIndex in range( len( Globals.trainingData[ 0][ 0])):
		neuron= Neuron( threshold= getRandomValueInRange())
		currentLayer.append( neuron)

	neuronLayers.append( currentLayer)
	
	for hiddenLayerIndex in range( hiddenLayersQuantity):
		currentLayer= list()
		for neuronIndex in range( neuronsInEachHiddenLayerQuantity):
			neuron= Neuron( threshold= getRandomValueInRange())
			currentLayer.append( neuron)
		neuronLayers.append( currentLayer)

	currentLayer= list()
	for neuronIndex in range( len( Globals.trainingData[ 0][ 1])):
		neuron= Neuron( threshold= getRandomValueInRange())
		currentLayer.append( neuron)

	neuronLayers.append( currentLayer)
	
	# Construct weights between each layer


	for weightLayerIndex in range( len( neuronLayers)- 1):

		currentLayer= list()

		for beforeIndex, beforeNeuron in enumerate( neuronLayers[ weightLayerIndex]):
			for afterIndex, afterNeuron in enumerate( neuronLayers[ weightLayerIndex+ 1]):

				value= getRandomValueInRange()

				weight= Weight(
					fromNeuron= beforeNeuron,
					toNeuron= afterNeuron,
					weightValue= value
				)

				currentLayer.append( weight)

		weightLayers.append( currentLayer)

	return (
		weightLayers,
		neuronLayers,
	)


(
	weightLayers,
	neuronLayers,
)= constructNetwork(
	hiddenLayersQuantity= Globals.hiddenLayersQuantity,
	neuronsInEachHiddenLayerQuantity= Globals.neuronsInEachHiddenLayerQuantity
)

print( "")
input( "Press enter to print the network structure:")
print( "\n")
first= True
for neuronLayerIndex in range( len( neuronLayers)):
	if not first:
		print( "")
		print( "CONNECTION LAYER")
		# print( [ f"|Connection {str( weight.fromNeuron.threshold)[:5]} → {str( weight.toNeuron.threshold)[:5]}, strength {str( weight.weightValue)[ :5]}|" for weight in weightLayers[ neuronLayerIndex- 1]])
		beforeNeuronLayer= neuronLayers[ neuronLayerIndex- 1]
		afterNeuronLayer= neuronLayers[ neuronLayerIndex]
		# print( [ f"|Connection {str( weight.fromNeuron.threshold)[:5]} → {str( weight.toNeuron.threshold)[:5]}, strength {str( weight.weightValue)[ :5]}|" for weight in weightLayers[ neuronLayerIndex- 1]])
		print( [ f"|Connection {str( beforeNeuronLayer.index( weight.fromNeuron))} → {str( afterNeuronLayer.index( weight.toNeuron))}, strength {str( weight.weightValue)[ :5]}|" for weight in weightLayers[ neuronLayerIndex- 1]])
		print( "")
	else:
		first= False
	print( "NEURON LAYER")
	print( [ f"|Nron AP {str(neuron.threshold)[:5]}|" for ( neuronIndex, neuron) in enumerate( neuronLayers[ neuronLayerIndex])])

print( "")
print( "The network is fully connected")
print( "")
input( "Press enter to begin training, this takes about 34 seconds on my machine using cpython 3.10:")

def propagateForward( pattern: tuple[ tuple, tuple]):
	# Reset netInput
	for neuronLayer in neuronLayers:
		for neuron in neuronLayer:
			neuron.netInput= 0.0
	
	# Calculate outputs for input layer
	for index, neuron in enumerate( neuronLayers[ 0]):
		neuron.calculatedOutput= neuron.activationFunction( pattern[ 0][ index]+ neuron.threshold)

	# Loop through weight layers as they hold both neurons either side in memory and calculate the outputs
	for weightLayerIndex, weightLayer in enumerate( weightLayers):
		for weight in weightLayer:
			# Add to the netInput of the neuron that comes after us
			weight.toNeuron.netInput+= weight.fromNeuron.calculatedOutput* weight.weightValue

		# Then run the function on them
		for neuron in neuronLayers[ weightLayerIndex+ 1]:
			neuron.calculatedOutput= neuron.activationFunction( neuron.netInput+ neuron.threshold)

fullTrainingDataCycles= 0

while True:

	epochError= 0.0

	for pattern in Globals.trainingData:

		propagateForward( pattern)

		patternError= 0.0
		# Here we find the error upon the output neurons, train their threshold and also calculate the error for the whole pattern
		for outputIndex, neuron in enumerate( neuronLayers[ -1]):
			outputError= pattern[ 1][ outputIndex]- neuron.calculatedOutput
			neuron.error= outputError* neuron.derivedActivationFunction( neuron.calculatedOutput)
			neuron.threshold+= Globals.trainingRate* outputError* neuron.derivedActivationFunction( neuron.calculatedOutput)
			patternError+= outputError** 2 

		patternError*= 0.5
		epochError+= patternError
		

		# Now calculate the error for each neuron by working backwards
		# So work backwards through the weight layers
		weightLayersLength= len( weightLayers)
		for reverseIndex in range( weightLayersLength):
			weightLayerIndex= weightLayersLength- ( reverseIndex+ 1)
			weightLayer= weightLayers[ weightLayerIndex]


			if weightLayerIndex!= 0:

				# Since we are fully connected we can sum up the error for every weights toNeuron and use this value for every neuron in the prior layer
				# Sum up the error on the following neuron* the weights value
				for weight in weightLayer:
					# Update each weights 
					momentumComponent= Globals.momentum* weight.previousWeightDelta
					weightDelta= Globals.trainingRate* weight.toNeuron.error* weight.fromNeuron.calculatedOutput+ momentumComponent
					weight.previousWeightDelta= weightDelta
					weight.weightValue+= weightDelta


					# "Part of back prop is that the new weights be calculated first and then used in adjustments to earlier units"
					# upon the fromNeuron layer accumulate the weight values * the new error from the connected toNeuron
					weight.fromNeuron.bPHelper0+= weight.weightValue* weight.toNeuron.error


				# Calculate the error for the prior neuron layer
				for neuron in neuronLayers[ weightLayerIndex]:
					# INSTEAD OF USING TOTALFROMERROR I NEED TO CALCULATE THE SUM OF ERRORK*WEIGHTJK
					# WHERE K IS EACH CONNECTED NEURON AFTER THE CURRENT
					neuron.error= neuron.bPHelper0* neuron.derivedActivationFunction( neuron.calculatedOutput)

					neuron.threshold+= neuron.bPHelper0* neuron.derivedActivationFunction( neuron.calculatedOutput)

					# Reset accumulated sum fot next step~s usage
					neuron.bPHelper0= 0.0

			else:
				# We only calculate deltas on first weight layer and dont calculate any errors
				for weight in weightLayer:
					# Update each weights 
					weightDelta= Globals.trainingRate* weight.toNeuron.error* weight.fromNeuron.calculatedOutput
					weight.weightValue+= weightDelta
			
		
	
	print( "\rEPOCH: ", fullTrainingDataCycles, "| ERROR: ", epochError, end= "")

	if epochError< Globals.acceptableErrorThreshold:
		break

	if fullTrainingDataCycles>= 9999999:
		break

	fullTrainingDataCycles+= 1


print( "\n")
print( "Training complete")
print( "")
input( "Press enter to view the results")


for pattern in Globals.trainingData:

	print( "")
	print( logicGateNamesOrdered[ pattern[ 0][ 0]], pattern[ 0])
	
	print( "EXPECTED: ", end= "")
	print( pattern[ 1], end= "")
	print( " | RESULT: ", end= "")
	propagateForward( pattern)
	print( tuple( [ round( neuron.calculatedOutput) for neuron in neuronLayers[ -1]]))

~~~~~~
\normalsize

---

  \
  \
  \
  \
  \
  \
  \
  
LP And Stochastic
---
The linear programming portion is a implementation which steps around the boundary of the feasible solution region shape.  
{ less than or equal to, greater than or equal to, equal to} constraints are supported with the two stage large number solution used.  
  
The stochastic programming portion allows optimisation in a scenario where an arbitrary number of stages are involved.  
At each stage, a variable must be decided upon whilst only knowing the probability of different future branching and not a definite knowledge.  
The implementation here generates an input which can be used in the linear programming function.  
  

---

\normalsize
~~~~~{.python .numberLines}
"""
`optimise` is the point of entry for linear programming
this calls `optimiseSystem`
`stochasticProgram` generates input for `optimise`
"""
import time
import pprint
from enum import IntFlag, auto
from dataclasses import dataclass
from typing import(
	Any,
	TypedDict,
	Union,
)
from collections.abc import(
	Sequence,
	Collection,
)

@dataclass
class Constraint:
	class Type( IntFlag):
		LTE= auto()
		GTE= auto()
		EQ= auto()

	type: Type
	coefficients: list[ float]
	constant: float
	def copy(
		self,
	):
		return Constraint(
			type= self.type,
			coefficients= self.coefficients.copy(),
			constant= self.constant,
		)


class OptimisationResponseType( IntFlag):
	SOLVED= auto()
	UNCONSTRAINED= auto()
	NO_FEASIBLE_SOLUTION= auto()

class SystemState( TypedDict):
	function: Constraint
	constraints: dict[
		int,
		Constraint,
	]

class OptimisationResponse0( TypedDict):
	type: OptimisationResponseType
	system: Union[
		SystemState,
		None,
	]
	detectedUnconstrainedVariableIndex: Union[
		None,
		int,
	]
class OptimisationResponse1( OptimisationResponse0):
	variableValues: list[ float]| None

class OptimisationRequest( IntFlag):
	MAXIMISE= auto()
	MINIMISE= auto()

def optimise(
	optimisationRequest: OptimisationRequest,
	optimisedFunctionCoefficients: list[ float],
	constraints: list[ Constraint],
)-> OptimisationResponse1:
	if optimisationRequest== OptimisationRequest.MINIMISE:
		optimisedFunctionCoefficients= [
			coefficient* -1
			for coefficient in optimisedFunctionCoefficients
		]

	# ensure all coefficient lengths match
	# deduce variable size
	inputVariableLength= len( optimisedFunctionCoefficients)
	for constraint in constraints:
		if len( constraint.coefficients)!= inputVariableLength:
			raise Exception( "Co-efficient length of constraints does not match")
	
	newConstraints= dict()
	newVariablesRequiringInitialSolution: list[ int]= list()
	totalVariableLength= inputVariableLength
	for constraint in constraints:
		existingLen= len( constraint.coefficients)
		variableLengthDifference= totalVariableLength- existingLen
		basicVariableIndex= totalVariableLength
		if constraint.type== Constraint.Type.EQ:
			newCoefficients= [ 1.0,]
			newVariablesRequiringInitialSolution.append( basicVariableIndex)
		elif constraint.type== Constraint.Type.LTE:
			newCoefficients= [ 1.0,]
		elif constraint.type== Constraint.Type.GTE:
			newCoefficients= [ -1.0, 1.0,]
			basicVariableIndex+= 1
			newVariablesRequiringInitialSolution.append( basicVariableIndex)
		else:
			raise NotImplementedError
		newConstraints[ basicVariableIndex]= Constraint(
			type= Constraint.Type.EQ,
			# pad any missing new coefficienct as 0.0
			coefficients= constraint.coefficients.copy()+ [ 0.0 for _ in range( variableLengthDifference)]+ newCoefficients,
			constant= constraint.constant,
		)
		totalVariableLength+= len( newCoefficients)
	# pad any new constraint-s
	for basicVariable in newConstraints:
		newConstraints[ basicVariable].coefficients+= [ 0.0 for _ in range( totalVariableLength- len( newConstraints[ basicVariable].coefficients))]

	optimisedFunction= Constraint(
		type= Constraint.Type.EQ,
		coefficients= [
			functionCoefficient* -1 # when subtracting variables from equation they are inverted
			for functionCoefficient in optimisedFunctionCoefficients
		]+ [
			0.0
			for _ in range( totalVariableLength- len( optimisedFunctionCoefficients))
		],
		constant= 0.0,
	)

	if len( newVariablesRequiringInitialSolution)> 0:
		# Solve for initial solution
		solveVariablesFunction= optimisedFunction.copy()
		# set all coefficients but the ones solved for in the function to 0
		solveVariablesSet= set( newVariablesRequiringInitialSolution)
		solveVariablesFunction.coefficients= [
			1.0 if index in solveVariablesSet else 0.0
			for ( index, _) in enumerate( solveVariablesFunction.coefficients)
		]

		for solveVariableIndex in newVariablesRequiringInitialSolution:
			constraint= newConstraints[ solveVariableIndex]
			solveVariablesFunction.constant-= constraint.constant
			solveVariablesFunction.coefficients= [
				solveVariablesFunction.coefficients[ coefficientIndex]- constraint.coefficients[ coefficientIndex]
				for coefficientIndex in range( len( solveVariablesFunction.coefficients))
			]

		initialFeasibleOptimisationResult= optimiseSystem(
			system= {
				"function": solveVariablesFunction,
				"constraints": newConstraints,
			},
		)
		for solveVariableIndex in newVariablesRequiringInitialSolution:
			if solveVariableIndex in initialFeasibleOptimisationResult[ "system"][ "constraints"]:
				# if initialFeasibleOptimisationResult[ "system"][ "constraints"][ solveVariableIndex]> 0:
				raise Exception( "UNSURE IF ALL ARTIFICIAL SHOULD BE UN PRESENT")
				return{
					"type": OptimisationResponseType.NO_FEASIBLE_SOLUTION,
					"system": None,
					"detectedUnconstrainedVariableIndex": None,
				}

		initialFeasibleSystem= initialFeasibleOptimisationResult[ "system"]
					
		for solveVariableIndex in reversed( sorted( newVariablesRequiringInitialSolution)):
			for basicVariableIndex in initialFeasibleSystem[ "constraints"]:
				initialFeasibleSystem[ "constraints"][ basicVariableIndex].coefficients.pop( solveVariableIndex)
			optimisedFunction.coefficients.pop( solveVariableIndex)

		# eliminate the input variable coefficients on the optimisation function by subtracting ( the corrosponding coefficient row for the variable* the size of the existing value)
		for inputFunctionCoefficientIndex in range( len( optimisedFunctionCoefficients)):
			if inputFunctionCoefficientIndex not in initialFeasibleSystem[ "constraints"]:
				continue
				raise Exception( "UNSURE IF THIS IS REQUIREMENT")
			if optimisedFunction.coefficients[ inputFunctionCoefficientIndex]!= 0:
				# find corrosponding equation
				eliminationEquation= initialFeasibleSystem[ "constraints"][ inputFunctionCoefficientIndex]
				multNumber= optimisedFunction.coefficients[ inputFunctionCoefficientIndex]
				# corrosponding value for row should be 1 to comply with elimination
				for coefficientIndex in range( len( optimisedFunction.coefficients)):
					if coefficientIndex== inputFunctionCoefficientIndex:
						continue
					optimisedFunction.coefficients[ coefficientIndex]-= ( eliminationEquation.coefficients[ coefficientIndex]* multNumber)
				optimisedFunction.constant-= ( eliminationEquation.constant* multNumber)

				optimisedFunction.coefficients[ inputFunctionCoefficientIndex]= 0.0

		optimisationResponse= optimiseSystem(
			system= {
				"function": optimisedFunction,
				"constraints": initialFeasibleSystem[ "constraints"],
			}
		)

	else:
		optimisationResponse= optimiseSystem(
			system= {
				"function": optimisedFunction,
				"constraints": newConstraints,
			},
		)
	if optimisationResponse[ "system"]!= None:
		optimisationResponse[ "variableValues"]= [
			optimisationResponse[ "system"][ "constraints"][ variableNumber].constant if variableNumber in optimisationResponse[ "system"][ "constraints"] else 0.0
			for variableNumber in range( inputVariableLength)
		]
	else:
		optimisationResponse[ "variableValues"]= None
	return optimisationResponse



def removeConstraint(
	removeFrom: Constraint,
	remove: Constraint,
	index: int,
):
	"""
	mutates "removeFrom" constraint
	find ( constraint value of index* -1) in current constraint and add the coefficients of the "remove" constraint to the coefficients of the "removeFrom" constraint
	"""
	pivotValue= removeFrom.coefficients[ index]
	if pivotValue== 0:
		return

	pivotValue*= -1
	for coefficientIndex in range( len( removeFrom.coefficients)):
		if coefficientIndex!= index:
			if remove.coefficients[ coefficientIndex]!= 0.0:
				removeFrom.coefficients[ coefficientIndex]+= remove.coefficients[ coefficientIndex]* pivotValue
		else:
			removeFrom.coefficients[ coefficientIndex]= 0.0

	if remove.constant!= 0.0:
		removeFrom.constant+= remove.constant* pivotValue

def optimiseSystem(
	system: SystemState,
)-> OptimisationResponse0:
	while True:
		# Check for optimality
		# The variable with the smallest maximisation coefficient will be the most optimal to alter
		smallestCentralFunctionCoefficient= 0.0
		newlyBasicCoefficientIndex= -1
		for ( optimisationCoefficientIndex, optimisationCoefficient) in enumerate( system[ "function"].coefficients):
			if optimisationCoefficient< smallestCentralFunctionCoefficient:
				smallestCentralFunctionCoefficient= optimisationCoefficient
				newlyBasicCoefficientIndex= optimisationCoefficientIndex

		if smallestCentralFunctionCoefficient>= 0.0:
			return{
				"type": OptimisationResponseType.SOLVED,
				"system": system,
				"detectedUnconstrainedVariableIndex": None,
			}
		# ties for smallest are broken arbitrarily
	
		# Transfer the state to the next most optimal polygon corner

		# Find which variable to increase
		# for each constraint, if the coefficient of the index of the newly basic index is above 0 then compare that to the value of the constant to find the ratio
		# the one with the smallest ratio, it~s basic variable is newlyNonBasic
		minimumConstraintBasicVariableIndex= None
		first= True
		coefficientToConstantRatio= 0.0
		for basicVariableIndex in system[ "constraints"]:
			constraint= system[ "constraints"][ basicVariableIndex]
			if constraint.coefficients[ newlyBasicCoefficientIndex]> 0:
				coefficientToConstantRatio= constraint.constant/ constraint.coefficients[ newlyBasicCoefficientIndex]
				if first:
					first= False
					smallestCoefficientToConstantRatio= coefficientToConstantRatio
					minimumConstraintBasicVariableIndex= basicVariableIndex
				else:
					if coefficientToConstantRatio< smallestCoefficientToConstantRatio:
						smallestCoefficientToConstantRatio= coefficientToConstantRatio
						minimumConstraintBasicVariableIndex= basicVariableIndex
		if first:
			return{
				"type": OptimisationResponseType.UNCONSTRAINED,
				"system": system,
				"detectedUnconstrainedVariableIndex": newlyBasicCoefficientIndex,
			}
		# ties for smallest ratio are broken arbitrarily

		# ensure the constraint associated with the newly non basic variable has a coefficient of 1 for the newly basic variable
		# minimumConstraint= system[ "constraints"][ minimumConstraintIndex][ 1]
		minimumConstraint= system[ "constraints"][ minimumConstraintBasicVariableIndex]
		pivotCoefficient= minimumConstraint.coefficients[ newlyBasicCoefficientIndex]
		if pivotCoefficient!= 1.0:
			for coefficientIndex in range( len( minimumConstraint.coefficients)):
				if coefficientIndex!= newlyBasicCoefficientIndex:
					minimumConstraint.coefficients[ coefficientIndex]/= pivotCoefficient
			minimumConstraint.constant/= pivotCoefficient
			minimumConstraint.coefficients[ newlyBasicCoefficientIndex]= 1.0

		# ensure that every other constraint in the system has a coefficient of 0
		# achieve this by adding the constraint row associated with the newly non basic variable
		removeConstraint(
			removeFrom= system[ "function"],
			remove= minimumConstraint,
			index= newlyBasicCoefficientIndex,
		)
		for basicVariableIndex in system[ "constraints"]:
			if basicVariableIndex== minimumConstraintBasicVariableIndex:
				continue
			removeConstraint(
				removeFrom= system[ "constraints"][ basicVariableIndex],
				remove= minimumConstraint,
				index= newlyBasicCoefficientIndex,
			)

		# associate the pivot row with the newly basic variable
		system[ "constraints"][ newlyBasicCoefficientIndex]= system[ "constraints"].pop( minimumConstraintBasicVariableIndex)


def printSystem(
	optimisationRequest,
	optimisedFunctionCoefficients,
	constraints,
):
	def numberString( number):
		result= str( number)
		if len( result)> 4:
			try:
				index= result.index( ".")
				return result[ : index]
			except:
				return result
		else:
			return result
	print( optimisationRequest.name)
	indexLine= ""
	functionLine= ""
	constraintLines= [ "" for _ in constraints]
	first= True
	for index in range( len( optimisedFunctionCoefficients)):
		if first:
			first= False
			sep= ""
		else:
			sep= " | "
		indexString= numberString( index)
		funcString= numberString( optimisedFunctionCoefficients[ index])
		constraintStrings= [ numberString( constraint.coefficients[ index]) for constraint in constraints]
		maxLen= max(
			map(
				lambda item: len( item),
				constraintStrings+ [ funcString, indexString],
			)
		)
		indexLine+= sep+ indexString.ljust( maxLen)
		functionLine+= sep+ funcString.ljust( maxLen)
		for constraintIndex in range( len( constraints)):
			constraintLines[ constraintIndex]+= sep+ constraintStrings[ constraintIndex].ljust( maxLen)

	equalitySymbols= {
		Constraint.Type.EQ: "=",
		Constraint.Type.GTE: ">=",
		Constraint.Type.LTE: "<=",
	}
	maxSymbolLen= max(
		map(
			lambda item: len( item),
			equalitySymbols.values(),
		)
	)
	for constraintIndex in range( len( constraints)):
		constraint= constraints[ constraintIndex]
		constraintLines[ constraintIndex]+= f" { equalitySymbols[ constraint.type].ljust( maxSymbolLen)} "+ str( constraint.constant)

	
	print( "INDEXS:")
	print( indexLine)
	print( "FUNCTION COEFFICIENTS:")
	print( functionLine)
	print( "CONSTRAINTS:")
	print( "\n".join( constraintLines))
		


print( "\033c")
def br():
	input( "Press enter to continue:")


wydnorGlass= {
	"optimisationRequest": OptimisationRequest.MAXIMISE,
	"optimisedFunctionCoefficients": [ 3.0, 5.0],
	"constraints": [
		Constraint(
			type= Constraint.Type.LTE,
			coefficients= [ 1.0, 0.0],
			constant= 4,
		),
		Constraint(
			type= Constraint.Type.LTE,
			coefficients= [ 0.0, 2.0],
			constant= 12,
		),
		Constraint(
			type= Constraint.Type.LTE,
			coefficients= [ 3.0, 2.0],
			constant= 18,
		),
	],
}
print( "First optimise the wyndsor glass example from the textbook by Hillier and Lieberman: Introduction to operations research:")
print( "")
printSystem( **wydnorGlass)
br()
result= optimise( **wydnorGlass)
print( "\nVARIABLE VALUES:")
print( result[ "variableValues"])

br()

radiationTherapy= {
	"optimisationRequest": OptimisationRequest.MINIMISE,
	"optimisedFunctionCoefficients": [ 0.4, 0.5],
	"constraints": [
		Constraint(
			type= Constraint.Type.LTE,
			coefficients= [ 0.3, 0.1],
			constant= 2.7,
		),
		Constraint(
			type= Constraint.Type.EQ,
			coefficients= [ 0.5, 0.5],
			constant= 6,
		),
		Constraint(
			type= Constraint.Type.GTE,
			coefficients= [ 0.6, 0.4],
			constant= 6,
		),
	],
}
print( "\n\n")
print( "Then optimise the radiation therapy example from the same book:")
print( "")
printSystem( **radiationTherapy)
br()
result= optimise( **radiationTherapy)
print( "\nVARIABLE VALUES:")
print( result[ "variableValues"])

br()


ConstraintIndex= int
StageVariableIndex= int
class Possibility( TypedDict):
	probabilityWeight: float
	constraintValues= Sequence[
		Sequence[ float], # as long as the stage variables
	] # as long as constraints

class Stage( TypedDict):
	possibilities: Collection[ Possibility]
	optimisationCoefficientsOfVariables: Sequence[ float]

def stochasticProgram(
	stages: Sequence[ Stage],
	constraints: Sequence[
		tuple[
			Constraint.Type,
			float,
		],
	],
):
	optimisedFunctionCoefficients= list()
	constraintCoefficients= [
		[ [ ]]
		for _ in range( len( constraints))
	]
	stageLen= len( stages)
	maxStageIndex= stageLen- 1
	def w(
		stageIndex: int,
		currentOptimisationFunctionMultiplier: float,
		currentConstraintRowIndex: int,
	):
		nonlocal optimisedFunctionCoefficients, constraintCoefficients
		nextStageIndex= stageIndex+ 1
		stage= stages[ stageIndex]
		stageVariableLen= len( stage[ "optimisationCoefficientsOfVariables"])
		blankPossibilityEntrys= [ 0.0 for _ in range( stageVariableLen)]

		optimisedFunctionCoefficients+= [
			opCoef* currentOptimisationFunctionMultiplier
			for opCoef in stage[ "optimisationCoefficientsOfVariables"]
		]
		
		possibilities= stage[ "possibilities"]
		possibilityLen= len( possibilities)
		totalProbabilityWeight= sum(
			map(
				lambda possibility: possibility[ "probabilityWeight"],
				possibilities,
			),
~~~~~~
~~~~~{.python .numberLines}
		)
		if possibilityLen> 1:
			addedRows= possibilityLen- 1
			for _ in range( addedRows):
				for row in constraintCoefficients:
					row.insert(
						currentConstraintRowIndex,
						row[ currentConstraintRowIndex].copy()
					)
		else:
			addedRows= 0

		for possibilityNumber in range( possibilityLen):
			
			for constraintNumber in range( len( constraintCoefficients)):
				constraintCoefficients[ constraintNumber][ currentConstraintRowIndex+ possibilityNumber]+= list( possibilities[ possibilityNumber][ "constraintValues"][ constraintNumber])
		blankSubsequentStageCoefficients= None
		previousConstraintRowIndexs= list()
		for possibility in possibilities:

			relativeProbability= possibility[ "probabilityWeight"]/ totalProbabilityWeight
			if blankSubsequentStageCoefficients== None:
				beforeIndexLength= len( constraintCoefficients[ 0][ currentConstraintRowIndex])
			if nextStageIndex<= maxStageIndex:
				nextAddedRows= w(
					stageIndex= nextStageIndex,
					currentOptimisationFunctionMultiplier= currentOptimisationFunctionMultiplier* relativeProbability,
					currentConstraintRowIndex= currentConstraintRowIndex,
				)
			else:
				nextAddedRows= 0
			if blankSubsequentStageCoefficients== None:
				subsequentStageCoefficientLength= len( constraintCoefficients[ 0][ currentConstraintRowIndex])- beforeIndexLength
				blankSubsequentStageCoefficients= [ 0.0 for _ in range( subsequentStageCoefficientLength)]
			nextConstraintRowIndex= currentConstraintRowIndex+ ( 1+ nextAddedRows)
			for nonCurrentConstraintRowIndex in previousConstraintRowIndexs+ list(
				range(
					nextConstraintRowIndex,
					nextConstraintRowIndex+ ( ( possibilityLen- 1)- len( previousConstraintRowIndexs))
				),
			):
				for constraintNumber in range( len( constraintCoefficients)):
					constraintCoefficients[ constraintNumber][ nonCurrentConstraintRowIndex]+= blankSubsequentStageCoefficients.copy()#blankPossibilityEntrys.copy()
			
			previousConstraintRowIndexs.append( currentConstraintRowIndex)
			currentConstraintRowIndex= nextConstraintRowIndex

		return addedRows


	w(
		stageIndex= 0,
		currentOptimisationFunctionMultiplier= 1.0,
		currentConstraintRowIndex= 0,
	)

	return {
		"optimisedFunctionCoefficients": optimisedFunctionCoefficients,
		"constraints": [
			Constraint(
				type= constraintType,
				coefficients= coefficientRow,
				constant= constraintConstant,
			)
			for ( constraintNumber, ( constraintType, constraintConstant)) in enumerate( constraints)
			for coefficientRow in constraintCoefficients[ constraintNumber]
		],
	}



print( "\n\nThen obtain a linear programming input from the following problem, in which there is a single stage of uncertainty")
print( "This example is taken from the following video: https://www.youtube.com/watch?v=mBxnwpsifjc, but also supports arbitrary stages of uncertainty")
print( "One must decide what crops to plant before knowing what the yields will be")
print( "One must then decide how much of each crop to sell and how much to buy in order to feed the cows")
print( "So we therefore have a decision variable for the distribution of the crops, and then multiple decision variables for the purchasing and selling of each crop depending on which yield event actually occured")
br()
print( "")

optimisationInput= stochasticProgram(
	stages= [
		{
			"possibilities": [
				{
					"probabilityWeight": 1,
					"constraintValues": [
						( 1, 1, 1,),
						( 3, 0, 0,),
						( 0, 3.6, 0,),
						( 0, 0, -24,),
						( 0, 0, 0,),
					],
				},
				{
					"probabilityWeight": 1,
					"constraintValues": [
						( 1, 1, 1,),
						( 2.5, 0, 0,),
						( 0, 3, 0,),
						( 0, 0, -20,),
						( 0, 0, 0,),
					],
				},
				{
					"probabilityWeight": 1,
					"constraintValues": [
						( 1, 1, 1,),
						( 2, 0, 0,),
						( 0, 2.4, 0,),
						( 0, 0, -16,),
						( 0, 0, 0,),
					],
				},
			],
			"optimisationCoefficientsOfVariables": ( 150, 230, 260,),
		},
		{
			"possibilities": [
				{
					"probabilityWeight": 1,
					"constraintValues": [
						( 0, 0, 0, 0, 0, 0,),
						( 1, 0, -1, 0, 0, 0,),
						( 0, 1, 0, -1, 0, 0,),
						( 0, 0, 0, 0, 1, 1,),
						( 0, 0, 0, 0, 1, 0,),
					],
				},
			],
			"optimisationCoefficientsOfVariables": ( 238, 210, -170, -150, -36, -10),
		},
	],
	constraints= [
		( Constraint.Type.LTE, 500,),
		( Constraint.Type.GTE, 200,),
		( Constraint.Type.GTE, 240,),
		( Constraint.Type.LTE, 0,),
		( Constraint.Type.LTE, 6000,),
	]
)

optimisationInput[ "optimisationRequest"]= OptimisationRequest.MINIMISE
print( "The resulting optimisation problem is:")
printSystem( **optimisationInput)

br()
print( "")
print( "The resulting variable inputs are:")
result= optimise(
	**optimisationInput,
)

print( result[ "variableValues"])
print( "")
print( "Therefore the decider can now know the optimal crops to plant given the likelyhood of each yield scenario, and can also know ahead of time what the most optimal decisions are ")


~~~~~~
\normalsize

---

  \
  \
  \
  \
  \
  \
  \
  
Optimisation Differenciation
---
This was designed as a test scenario for a problem requiring opimisation.  
I wish to show off the symbolic differenciation aspect  
  

---

\normalsize
~~~~~{.python .numberLines}
"""
This is designed to emulate a scenario in which one is locked inside a room with wheel-s on the wall, a number display on the wall.
It is known that, should the number reach the { highest, lowest, value closest to another} value it possibly can, then the door will open and they will be free.
The wheel-s affected the number on the wall
If one can also observe the mechanism-s linking the wheel-s to the number display then how can they escape the room?

To emulate this, there exists a `System` which has a sequence of `OperationStack`-s ( the mechanism between the wheel and the door) which are indexed by integer-s ( the index of the wheel). The system can be queried with `System.getRating` ( the number display).

an `Operation` in a stack can either be a `SingleOperation` which simply takes the previous result in the stack as input or `ElementOperation` which can take either the base value of an input or the result of a stack.

it is of note that the result-s of the utility portion of the system do not contribute to the system rating

Part of the exploratory effort was to develop a system which can compute the first derivative of the system in respect to a singular input
some call this symbolic differentiation, i believe partial differentiation
it will give way to a unifyed equation system using arbitrary nested function decleration
symbolic integration is seemingly much harder and using an existing implementation may be the way
"""
import pprint
from io import(
	StringIO,
)
from mAAP.DataSpecification import(
	specificationed,
	specValue,
)
from copy import(
	deepcopy,
)
import numpy
from gi.repository import(
	Gtk,
	Adw,
)
from mAAP.Widgets import(
	NicerPaned,
	ListViewPopover,
	DataVisualiser,
	DropDown,
)
from mAAP.Utils import(
	vecLen,
	ImplementationError,
	fitRange,
	lerp,
	NonGObjectGioListStore,
	MutableContainer,
)
from mAAP.CustomEnum import(
	IntFlag,
	auto,
)
from collections.abc import(
	Iterator,
)
from mAAP.DataForTypes import(
	getEditingWidgetList,
)
from typing import(
	Protocol,
	Self,
	TypedDict,
	ForwardRef,
	Union,
	Literal,
	ClassVar,
	Any,
)
# One thing of note is that certain configuration-s can cause some/ all value-s to become unstable, the derived ruleset-s no longer apply when every result is so random, so the ability to ( remember a stable state, to re approach a stable state, to never leave a stable state) is useful in combating these scenario-s
# Never leaving a stable state seems undesirable, often new configuration-s are found in a chaotic state

# Maybe also allow picking from predefined function
import random
import math

def safeDiv( q, w):
	if w== 0:
		return math.inf
	else:
		return q/ w
def safeLog( q, base):
	if q<= 0:
		return 0.0
	else:
		return math.log( q, base)
def safePow( q, w):
	if q== 0:
		if w< 0:
			return 0.0
	elif q< 0:
		( decimalComponent, _)= math.modf( w)
		if decimalComponent!= 0:
			return 0.0
	try:
		return math.pow( q, w)
	except Exception as e:
		print( "Q:", q)
		print( "W:", w)
		raise e


ElementIndex= int
OperationIndex= int
IterationId= int

@specificationed
class Operation():
	_stack: "OperationStack"= specValue( init= False)
	_stackPosition: int= specValue( init= False)

	_resultCache: dict[ IterationId, float]
	_derivativeCache: dict[ IterationId, dict[ ElementIndex, float]]

	# Operation-s are instanciated with their unique required data
	def __init__( self,):
		self._resultCache= dict()
		self._derivativeCache= dict()

	def _getAdditionalInfluencingElements(
		self,
	)-> set[ ElementIndex]:
		"""
		Retrieve all element-s which influence the result of this operation
		Do not include element-s which influences the result which is input into this operation
		This should only include element-s which influence this operation by means of special function of this operation
		"""
		return set()

	def _getResultFromInput(
		self,
		inputResult: float,
	)-> float:
		raise NotImplementedError

	def _getDerivativeFromInputs(
		self,
		freeElementIndex: ElementIndex,
		inputDerivative: float,
		inputResult: float,
	)-> float:
		# Imp-s may find the free element index useful
		raise NotImplementedError

	def cacheResult(
		self,
		result: float,
	):
		self._resultCache[ hash( self._stack._system._systemStateSummary)]= result

	def cacheDerivative(
		self,
		result: float,
		freeElementIndex: ElementIndex,
	):
		systemStateHash= hash( self._stack._system._systemStateSummary)
		if systemStateHash in self._derivativeCache:
			self._derivativeCache[ systemStateHash][ freeElementIndex]= result
		else:
			self._derivativeCache[ systemStateHash]= {
				freeElementIndex: result,
			}

	def getLowerOperation(
		self
	):
		"""Onwy to be cawwed buy wesposible cawwes who know they r not ayt the bowtom"""
		return self._stack.getOperationAtIndex( self._stackPosition- 1)

	def getInputResult(
		self,
	)-> float:
		if self._stackPosition== 0:
			inputResult= self._stack._system.getElementBaseValue( self._stack.rootElementIndex)
		else:
			inputResult= self.getLowerOperation().getResult()
		return inputResult

	def getResult(
		self,
	)-> float:
		# Look for cache result
		systemStateHash= hash( self._stack._system._systemStateSummary)
		if systemStateHash in self._resultCache:
			return self._resultCache[ systemStateHash]

		# Get result from element below in the stack
		inputResult= self.getInputResult()

		# Only take real component as imaginary number generation can occur for an unknown reason
		result= self._getResultFromInput(
			inputResult= inputResult,
		).real
		# Add result to cache
		self.cacheResult(
			result= result,
		)
		return result 
			
	def getInputDerivative(
		self,
		freeElementIndex: ElementIndex,
	):
		"""Returns None if the calling aspect should not calculate the derivative as it is known to be 0.0"""
		# If free is at the root then step down and get the deriv of that below, if at bottom then it is 1.0
		# If free is not at the root then 
		#   If free is not in stack-s influenced elements then getDerivative should return 0.0
		#   If it is then if this op~s index is the one which if the recorded ( first index of influence) then the input is 0.0
		#   If it is not the bottom recorded then get the deriv of that below
		if freeElementIndex== self._stack._rootElementIndex:
			if self._stackPosition== 0:
				return 1.0
			else:
				return self.getLowerOperation().getDerivative( freeElementIndex)
		else:
			if freeElementIndex not in self._stack._influencingElements:
				return None
			indexOfFreeElementInfluenceStart= self._stack._influencingElements[ freeElementIndex]
			if self._stackPosition> indexOfFreeElementInfluenceStart:
				return self.getLowerOperation().getDerivative( freeElementIndex)
			elif self._stackPosition== indexOfFreeElementInfluenceStart:
				return 0.0
			else:
				# Then it is below the occurence of influence of this stack
				# The sensitivity is 0.0, but with current design, this should never be called
				raise ImplementationError
				return 0.0
	
	def getDerivative(
		self,
		freeElementIndex: ElementIndex,
	):
		systemStateHash= ( self._stack._system._systemStateSummary)
		if systemStateHash in self._derivativeCache:
			if freeElementIndex in self._derivativeCache[ systemStateHash]:
				return self._derivativeCache[ systemStateHash][ freeElementIndex]

		inputDerivative= self.getInputDerivative( freeElementIndex)
		if inputDerivative== None:
			return 0.0
		inputResult= self.getInputResult()
		# Only take real component as imaginary number generation can occur for an unknown reason
		result= self._getDerivativeFromInputs(
			freeElementIndex= freeElementIndex,
			inputDerivative= inputDerivative,
			inputResult= inputResult,
		).real
		self.cacheDerivative(
			result= result,
			freeElementIndex= freeElementIndex,
		)
		return result


class StackSpace( IntFlag):
	SYSTEM_RATING_AFFECTOR= auto()
	UTILITY= auto()
StackSpaceSpecifier= Union[
	Literal[ StackSpace.SYSTEM_RATING_AFFECTOR],
	tuple[ Literal[ StackSpace.UTILITY], int],
]
StackSpecifier= tuple[ StackSpaceSpecifier, ElementIndex]

@specificationed
class OperationStack:

	_system: "System"= specValue( init= False)
	_rootElementIndex: ElementIndex= specValue( init= False)
	_influencingElements: dict[ ElementIndex, OperationIndex]= specValue( init= False)
	_influencingElementsFunctionState: int| None= specValue( init= False)
	_operations: list[ Operation]

	@property
	def rootElementIndex( self): return self._rootElementIndex

	def __init__(
		self,
	):
		self._operations= list()
		self._influencingElements= None
		self._influencingElementsFunctionState= None

	def iterOperations(
		self,
	)-> Iterator[ Operation]:
		return iter( self._operations)

	def __len__( self):
		return len( self._operations)
	
	def appendOperation(
		self,
		operation: Operation,
	):
		operation._stack= self
		operation._stackPosition= len( self._operations)
		# Register operation cache-s with session
		self._system._registerOperationCaches( operation)
		self._operations.append( operation)

	def getOperationAtIndex(
		self,
		index: int,
	)-> Operation:
		return self._operations[ index]

	def determineInfluencingElements(
		self,
	):
		if ( self._influencingElements!= None)and ( self._influencingElementsFunctionState== self._system._systemStateSummary[ 1]):
			return self._influencingElements
		# Construct self._influencingElements as dict[ ElementId, OperationIndexOfFirstOccurence]
		influencingElements= dict()
		for ( operationIndex, operation) in enumerate( self._operations):
			operationInfluencingElements= operation._getAdditionalInfluencingElements()
			operationInfluencingElements.difference_update( set( influencingElements.keys()))
			if self._rootElementIndex in operationInfluencingElements:
				operationInfluencingElements.remove( self._rootElementIndex)
			for newInfluencer in operationInfluencingElements:
				influencingElements[ newInfluencer]= operationIndex

		self._influencingElements= influencingElements
		self._influencingElementsFunctionState= self._system._systemStateSummary[ 1]
		return self._influencingElements

@specificationed
class System():

	Space= dict[ ElementIndex, OperationStack]
	BaseSpaceType= dict[ ElementIndex, list[ Operation]]
	BaseFunctionSpecification= tuple[
		BaseSpaceType,
		list[ BaseSpaceType],
	]

	elements: list[ float]
	# _iterationId: IterationId
	_systemStateSummary: tuple[
		int, # Incrmented value representing the element-s state
		int, # Incrmented value representing the function state
	]
	_sessionCache: list[ dict[ IterationId, Any]]
	function: list[
		Space,
		list[ Space],
	]

	# @property
	# def iterationId( self): return self._iterationId

	def __init__(
		self,
		# utilitySpaceNumber: int= 0,
		elementValues: list[ float]= None,
		baseFunction: BaseFunctionSpecification= None,
	):
		# self._iterationId= 0
		self._systemStateSummary= (
			0,
			0,
		)
		self._sessionCache= list()

		if elementValues== None:
			elementValues= []
		self.elements= elementValues
		if baseFunction!= None:
			self.setFunctionFromBase( baseFunction)
		else:
			self.function= (
				dict(),
				[],
			)

	def setFunctionFromBase(
		self,
		baseFunction: BaseFunctionSpecification,
		clearCurrentOperationCaches: bool= True,
	):
		self._sessionCache= list()
		self.function= (
			dict(),
			[],
		)
		def constructSpace(
			baseSpace,
			spaceSpecifier,
		):
			for elementIndex in baseSpace:
				stack= OperationStack()
				# Set system before append-ing
				stack._system= self
				baseOpList= baseSpace[ elementIndex]
				for operationInstance in baseOpList:
					stack.appendOperation( operationInstance)
				self.appendOperationStack(
					elementIndex= elementIndex,
					stack= stack,
					stackSpaceSpecifier= spaceSpecifier,
				)
			
		constructSpace(
			baseSpace= baseFunction[ 0],
			spaceSpecifier= StackSpace.SYSTEM_RATING_AFFECTOR,
		)
		for ( utilitySpaceIndex, utilitySpace) in enumerate( baseFunction[ 1]):
			self.function[ 1].append( dict())
			constructSpace(
				baseSpace= utilitySpace,
				spaceSpecifier= ( StackSpace.UTILITY, utilitySpaceIndex)
			)


		if clearCurrentOperationCaches== True:
			self.clearOperationCache( hash( self._systemStateSummary))

		self._systemStateSummary= (
			self._systemStateSummary[ 0],
			self._systemStateSummary[ 1]+ 1,
		)

		self.analyseFunction()

	def analyseFunction(
		self,
	):
		for space in self.iterSpaces():
			for rootElementIndex in space:
				operationStack= space[ rootElementIndex]
				operationStack.determineInfluencingElements()

	def iterSpaces(
		self,
	)-> Iterator[ Space]:
		yield self.function[ 0]
		for space in self.function[ 1]:
			yield space

	def setElementValue(
		self,
		index: int,
		value: float,
		clearCurrentOperationCaches: bool= True,
	):
		self.elements[ index]= value
		if clearCurrentOperationCaches== True:
			self.clearOperationCache( hash( self._systemStateSummary))

		self._systemStateSummary= (
			self._systemStateSummary[ 0]+ 1,
			self._systemStateSummary[ 1],
		)

	def setElements(
		self,
		elements: list[ float],
		clearCurrentOperationCaches: bool= True,
	):
		self.elements= elements
		if clearCurrentOperationCaches== True:
			self.clearOperationCache( hash( self._systemStateSummary))

		self._systemStateSummary= (
			self._systemStateSummary[ 0]+ 1,
			self._systemStateSummary[ 1],
		)

	def clearOperationCache(
		self,
		systemStateHash: int,
	):
		for operationCache in self._sessionCache:
			if systemStateHash in operationCache:
				operationCache.pop( systemStateHash)

	def _registerOperationCaches(
		self,
		operation: Operation,
	):
		self._sessionCache.append( operation._derivativeCache)
		self._sessionCache.append( operation._resultCache)
	
	def getElementBaseValue(
		self,
		elementIndex: ElementIndex,
	):
		return self.elements[ elementIndex]

	def alterUtilitySpaceNumber(
		self,
		newNumber: int,
	):
		currentLen= len( self.function[ 1])
		if newNumber== currentLen:
			return
		if newNumber> currentLen:
			self.function[ 1]+= [ dict() for newIndex in range( newNumber- currentLen)]
		if newNumber< currentLen:
			self.function= (
				self.function[ 0],
				self.function[ 1][ : newNumber],
			)
~~~~~~
~~~~~{.python .numberLines}
	
	def getSensitivityToElement(
		self,
		freeElementIndex: ElementIndex,
	)-> float:
		totalResult= 0.0
		for stackRootElementIndex in self.function[ 0]:
			stack= self.function[ 0][ stackRootElementIndex]
			if len( stack)> 0:
				totalResult+= stack.getOperationAtIndex( -1).getDerivative( freeElementIndex)
		return totalResult
	def getRating(
		self,
	)-> float:
		totalResult= 0.0
		for stackRootElementIndex in self.function[ 0]:
			stack= self.function[ 0][ stackRootElementIndex]
			if len( stack)> 0:
				totalResult+= stack.getOperationAtIndex( -1).getResult()
		return totalResult

	def appendOperationStack(
		self,
		elementIndex: ElementIndex,
		stack: OperationStack,
		stackSpaceSpecifier: StackSpaceSpecifier,
	):
		stack._system= self
		stack._rootElementIndex= elementIndex
		stackSpace= self.getStackSpace( stackSpaceSpecifier)
		stackSpace[ elementIndex]= stack
		
	def getStackSpace(
		self,
		stackSpaceSpecifier: StackSpaceSpecifier,
	)-> Space:
		if isinstance( stackSpaceSpecifier, tuple):
			stackSpace= self.function[ 1][ stackSpaceSpecifier[ 1]]
		else:
			stackSpace= self.function[ 0]
		return stackSpace

	def getOperationStack(
		self,
		stackSpecifier: StackSpecifier,
	)-> OperationStack:
		( stackSpaceSpecifier, elementIndex)= stackSpecifier
		stackSpace= self.getStackSpace( stackSpaceSpecifier)
		return stackSpace[ elementIndex]

class ElementValueSourceType( IntFlag):
	SYSTEM_RATING_AFFECTOR= StackSpace.SYSTEM_RATING_AFFECTOR
	UTILITY= StackSpace.UTILITY
	BASE_VALUE= auto()
elementValueSourceSpecifierData= {
	ElementValueSourceType.SYSTEM_RATING_AFFECTOR: ElementValueSourceType.SYSTEM_RATING_AFFECTOR,
	ElementValueSourceType.UTILITY: tuple[ ElementValueSourceType.UTILITY, int],
	ElementValueSourceType.BASE_VALUE: ElementValueSourceType.BASE_VALUE,
}
ElementValueSourceSpecifier= Union[ *elementValueSourceSpecifierData.values()]


class SingleOperation( Operation):
	def __init__(
		self,
	):
		Operation.__init__( self)

	def _getResultFromInput(
		self,
		inputResult: float,
	)-> float:
		return self.calculateResult(
			calculatedResult= inputResult,
		)

	def _getDerivativeFromInputs(
		self,
		freeElementIndex: ElementIndex,
		inputDerivative: float,
		inputResult: float,
	)-> float:
		return self.calculateDerivative(
			calculatedDerivative= inputDerivative,
			calculatedResult= inputResult,
		)
	
	@staticmethod
	def calculateResult(
		calculatedResult: float
	):
		raise NotImplementedError
	@staticmethod
	def calculateDerivative(
		calculatedDerivative: float,
		calculatedResult: float,
	):
		raise NotImplementedError

class ElementOperation( Operation):

	def __init__(
		self,
		combinedElementIndex: ElementIndex,
		combinedElementValueSource: ElementValueSourceSpecifier,
	):
		self.combinedElementIndex= combinedElementIndex
		self.combinedElementValueSource= combinedElementValueSource
		Operation.__init__( self)

	def _getAdditionalInfluencingElements(
		self,
	):
		if self.combinedElementValueSource== ElementValueSourceType.BASE_VALUE:
			return { self.combinedElementIndex}
		else:
			otherStack= self._stack._system.getOperationStack(
				stackSpecifier= ( self.combinedElementValueSource, self.combinedElementIndex),
			)
			influencingElements= set( otherStack.determineInfluencingElements().keys())
			influencingElements.add( self.combinedElementIndex)
			return influencingElements

	def _getResultFromInput(
		self,
		inputResult: float,
	)-> float:
		if self.combinedElementValueSource== ElementValueSourceType.BASE_VALUE:
			otherElementValue= self._stack._system.getElementBaseValue( self.combinedElementIndex)
		else:
			otherStack= self._stack._system.getOperationStack(
				stackSpecifier= ( self.combinedElementValueSource, self.combinedElementIndex),
			)
			otherElementValue= otherStack.getOperationAtIndex( -1).getResult()

		return self.calculateResult(
			calculatedResult= inputResult,
			otherElementResult= otherElementValue,
		)

	def _getDerivativeFromInputs(
		self,
		freeElementIndex: ElementIndex,
		inputDerivative: float,
		inputResult: float,
	)-> float:
		# Only need the result value of the other element
		# If that element is the result of a function
		# If that element is:
		#        a function of another stack which is the free element
		#    Then pass the other element result
		# If that element is:
		#        the free element base value
		#        the base value of a not free element
		#        a function of another stack, which is not influenced by the free element
		#    Then no pass the other element result

		# Maybe just pass always anyway
		# The result should already be cached to be honest so the overhead is just cache retrieval and transfer
		# This discussion is important

		if self.combinedElementValueSource== ElementValueSourceType.BASE_VALUE:
			otherElementResultValue= self._stack._system.getElementBaseValue( self.combinedElementIndex)
			if freeElementIndex== self.combinedElementIndex:
				otherElementDerivativeValue= 1.0
			else:
				otherElementDerivativeValue= 0.0
		else:
			otherStack= self._stack._system.getOperationStack(
				stackSpecifier= ( self.combinedElementValueSource, self.combinedElementIndex),
			)
			otherStackLastOp= otherStack.getOperationAtIndex( -1)
			otherElementResultValue= otherStackLastOp.getResult()
			otherElementDerivativeValue= otherStackLastOp.getDerivative( freeElementIndex)

		return self.calculateDerivative(
			calculatedDerivative= inputDerivative,
			calculatedResult= inputResult,
			otherElementIsFreeElement= freeElementIndex== self.combinedElementIndex,
			otherElementCalculatedDerivative= otherElementDerivativeValue,
			otherElementCalculatedResult= otherElementResultValue,
		)

	@staticmethod
	def calculateResult(
		calculatedResult: float,
		otherElementResult: float,
	):
		raise NotImplementedError

	@staticmethod
	def calculateDerivative(
		calculatedDerivative: float,
		calculatedResult: float,
		otherElementIsFreeElement: bool,
		otherElementCalculatedDerivative: float,
		otherElementCalculatedResult: float,
	):
		raise NotImplementedError 

class Container( type):
	def __iter__( self):
		for thing in dir( self):
			if thing.startswith( "__")== False:
				yield getattr( self, thing)

class Operations:
	class Single( metaclass= Container):
		# def multWithRandNum( element):
		# 	randNum= fitRange( random.random(), 0, 1, -3.4, 6.12)
		# 	print( randNum)
		# 	return element* randNum
		# Bit too many wave-s for now, was dominating every stack
		# def cos( element):
		# 	return math.cos( element)
		# def tan( element):
		# 	return math.tan( element)
		# def powWithRandNum( element):
		# 	if element== 0:
		# 		return 0
		# 	else:
		# 		randNum= fitRange( random.random(), 0, 1, -5.2, 2.02)
		# 		print( randNum)
		# 		return element** randNum
		class MultWithNegOne( SingleOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
			)-> float:
				return calculatedResult* -1
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
			)-> float:
				return calculatedDerivative* -1
		class Sin( SingleOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
			)-> float:
				return math.sin( calculatedResult)
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
			)-> float:
				# TODO: Possible issue with system
				# This is abstracting the calculated component of the sin function and using the incremental rule for sensitivity calculation
				# It is posisble that this should not be performed when the calculated component of sin is not a composite of different function-s and is simply the free element ( when this is at index 0 of the stack)
				return math.cos( calculatedResult)* calculatedDerivative
	class Element( metaclass= Container):
		class Add( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				return calculatedResult+ otherElementResult
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	return calculatedDerivative+ 1.0
				# else:
				# 	return calculatedDerivative
				return calculatedDerivative+ otherElementCalculatedDerivative
		class Subtract( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				return calculatedResult- otherElementResult
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	return calculatedDerivative- 1.0
				# else:
				# 	return calculatedDerivative
				return calculatedDerivative- otherElementCalculatedDerivative
		class Mult( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				return calculatedResult* otherElementResult
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	# Sensitivity of output to solely element1 is 1.0
				# 	# Sensitivity of output to other multiplied number is `calculatedDerivative` 
				# 	# Result of element1 is element1
				# 	# Result of other multiplied number is 
				# 	# Using product rule
				# 	# TODO: understand how relationships come to form the product rule
				# 	return ( otherElementCalculatedDerivative* calculatedDerivative)+ ( calculatedResult* 1.0)
				# else:
				# 	return calculatedDerivative* otherElementCalculatedDerivative
				return ( calculatedResult* otherElementCalculatedDerivative)+ ( otherElementCalculatedResult* calculatedDerivative)

		class Divide( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				if otherElementResult== 0:
					return 0
				else:
					return calculatedResult/ otherElementResult
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	# See comment in Multiply.calculateDerivative
				# 	return safeDiv( ( ( otherElementCalculatedDerivative* calculatedDerivative)- ( calculatedResult* 1.0)), math.pow( otherElementCalculatedDerivative, 2))
				# else:
				# 	return safeDiv( calculatedDerivative, otherElementCalculatedDerivative)
				return safeDiv( ( ( otherElementCalculatedResult* calculatedDerivative)- ( calculatedResult* otherElementCalculatedDerivative)), math.pow( otherElementCalculatedResult, 2))
		class VecLen2D( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				return vecLen( ( calculatedResult, otherElementResult))
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	return ( 0.5* safePow( math.pow( calculatedResult, 2)+ math.pow( otherElementCalculatedDerivative, 2), -0.5))* ( ( 2* calculatedResult)+ ( 2* otherElementCalculatedDerivative))* calculatedDerivative

				# else:
				# 	return ( 0.5* safePow( math.pow( calculatedResult, 2)+ math.pow( otherElementCalculatedDerivative, 2), -0.5))* ( 2* calculatedResult)* calculatedDerivative
				return ( 0.5* safePow( math.pow( calculatedResult, 2)+ math.pow( otherElementCalculatedResult, 2), -0.5))* ( ( 2* calculatedResult* calculatedDerivative)+ ( 2* otherElementCalculatedResult* otherElementCalculatedDerivative))
		class Minimum( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				return min( calculatedResult, otherElementResult)
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if calculatedResult> otherElementCalculatedDerivative:
				# 	if otherElementIsFreeElement== True:
				# 		return 1.0
				# 	else:
				# 		return 0.0
				# elif calculatedResult== otherElementCalculatedDerivative:
				# 	if otherElementIsFreeElement== True:
				# 		return 1.0+ ( ( calculatedDerivative- 1.0)/ 2.0)
				# 	else:
				# 		return calculatedDerivative/ 2.0
				# else:
				# 	return calculatedDerivative
				if calculatedResult< otherElementCalculatedResult:
					return calculatedDerivative
				elif otherElementCalculatedResult< calculatedResult:
					return otherElementCalculatedDerivative
				else: # equal
					return lerp( calculatedDerivative, otherElementCalculatedDerivative, 0.5)
		class Pow( ElementOperation):
			@staticmethod
			def calculateResult(
				calculatedResult: float,
				otherElementResult: float,
			)-> float:
				if calculatedResult== 0:
					if otherElementResult< 0:
						return 0.0
				elif calculatedResult< 0:
					( decimalComponent, _)= math.modf( otherElementResult)
					if decimalComponent!= 0.0:
						return 0.0
				return math.pow( calculatedResult, otherElementResult)
			@staticmethod
			def calculateDerivative(
				calculatedDerivative: float,
				calculatedResult: float,
				otherElementIsFreeElement: bool,
				otherElementCalculatedDerivative: float,
				otherElementCalculatedResult: float,
			)-> float:
				# if otherElementIsFreeElement== True:
				# 	return ( math.pow( math.e, otherElementCalculatedDerivative* safeLog( calculatedResult, math.e)))* ( otherElementCalculatedDerivative* ( safeDiv( 1, calculatedResult)* calculatedDerivative)+ safeLog( calculatedResult, math.e))
				# else:
				# 	return ( otherElementCalculatedDerivative* safePow( calculatedResult, otherElementCalculatedDerivative- 1.0))* calculatedDerivative

				# New pow calc may be faster than cache retrieval
				if calculatedResult== 0:
					if otherElementCalculatedResult< 0:
						return 0.0
				elif calculatedResult< 0:
					( decimalComponent, _)= math.modf( otherElementCalculatedResult)
					if decimalComponent!= 0.0:
						return 0.0
				return math.pow( calculatedResult, otherElementCalculatedResult)* ( ( otherElementCalculatedResult* safeDiv( 1, calculatedResult)* calculatedDerivative)+ ( safeLog( calculatedResult, math.e)* otherElementCalculatedDerivative))


premadeFunctions= {
	"tt": ( 3, (
		{
			0: [ Operations.Element.VecLen2D(
				combinedElementIndex= 1,
				combinedElementValueSource= ( ElementValueSourceType.UTILITY, 0),
			)],
		},
		[
			{
			# 	2: [ Operations.Element.Mult(
			# 		combinedElementIndex= 1,
			# 		combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
			# 	)],
				1: [ Operations.Element.Mult(
					combinedElementIndex= 2,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				)],
			},
		],
	)),
	"Looping multiplication": ( 3, (
		{
			0: [ Operations.Element.Mult(
				combinedElementIndex= 1,
				combinedElementValueSource= ( ElementValueSourceType.UTILITY, 0),
			)],
		},
		[
			{
				2: [ Operations.Element.Mult(
					combinedElementIndex= 1,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				)],
				1: [ Operations.Element.Mult(
					combinedElementIndex= 2,
					combinedElementValueSource= ( ElementValueSourceType.UTILITY, 0),
				)],
			},
		],
	)),
	"Bwokie": ( 3, (
		{
			1: [
				Operations.Single.Sin(),
				Operations.Element.Mult(
					combinedElementIndex= 0,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				),
			],
			2: [
				Operations.Element.Minimum(
					combinedElementIndex= 1,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				),
				Operations.Element.Pow(
					combinedElementIndex= 1,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				),
			],
		},
		[],
	)),
}

~~~~~~
~~~~~{.python .numberLines}
def createRandomBaseFunction(
	elementNumber: int,
)-> System.BaseFunctionSpecification:
	systemAffectors= dict()
	utilitySpaces= list()

	startingElementSize= random.randrange( 2, elementNumber)
	startingElementPool= set()
	nonStartingElementPool= set( range( elementNumber))
	for _ in list( range( startingElementSize)):
		startingElement= random.choice( list( nonStartingElementPool))
		nonStartingElementPool.remove( startingElement)
		startingElementPool.add( startingElement)

	startingElements= list( startingElementPool)
	# Order does not matter if adding all starting tower-s,
	# If order does matter can use SequenceAndMappingContainer
	function= dict()
	for startingElement in startingElements:
		singleOperations= random.randrange( 0, 5)
		elementOperations= random.randrange( 0, 8)
		choices= list()
		if singleOperations> 0:
			choices.append( Operations.Single)
		if elementOperations> 0:
			choices.append( Operations.Element)
		operationSequence= list()
		while ( singleOperations+ elementOperations)> 0:
			choice= random.choice( choices)
			if choice== Operations.Single:
				operationSequence.append( random.choice( list( Operations.Single))())
				singleOperations-= 1
				if singleOperations== 0:
					choices.remove( Operations.Single)
			elif choice== Operations.Element:
				if len( nonStartingElementPool)> 0:
					otherElementIndex= random.choice( list( nonStartingElementPool))
					nonStartingElementPool.remove( otherElementIndex)
					startingElementPool.add( otherElementIndex)
				else:
					otherElementIndex= random.choice( list( startingElementPool))

				operationSequence.append( random.choice( list( Operations.Element))(
					combinedElementIndex= otherElementIndex,
					combinedElementValueSource= ElementValueSourceType.BASE_VALUE,
				))
				# operationSequence.append( ( OperationType.ELEMENT, ( random.choice( list( Operations.Element)), otherElementIndex)))
				elementOperations-= 1
				if elementOperations== 0:
					choices.remove( Operations.Element)
		function[ startingElement]= operationSequence

	systemAffectors= function

	return (
		systemAffectors,
		utilitySpaces,
	)



# def applyFunction(
# 	elementValues: list[ float],
# 	function: ConstructedFunction,
# )-> float:
# 	totalValue= 0.0
# 	for startingElementIndex in function:
# 		startingValue= elementValues[ startingElementIndex]
# 		for ( opType, opData) in function[ startingElementIndex]:
# 			try:
# 				match opType:
# 					case OperationType.SINGLE:
# 						op= opData
# 						startingValue= op.getResult(
# 							element= startingValue,
# 						).real
# 					case OperationType.ELEMENT:
# 						( op, otherElementIndex)= opData
# 						otherElementValue= elementValues[ otherElementIndex]
# 						startingValue= op.getResult(
# 							element0= startingValue,
# 							element1= otherElementValue,
# 						).real
# 			except Exception as e:
# 				print( f"Error raised on application with { startingValue} as an input")
# 				raise e
# 		totalValue+= startingValue
# 	return totalValue

# def deriveFunction(
# 	freeElementIndex: int,
# 	elementValues: list[ float],
# 	function: ConstructedFunction,
# )-> float:
# 	otherStacks= set( function.keys())
# 	if freeElementIndex in function:
# 		ownStack= function[ freeElementIndex]
# 		otherStacks.remove( freeElementIndex)
# 		# Loop over own stack
# 		calcResult= elementValues[ freeElementIndex]
# 		calcDeriv= 1.0
# 		for ( opType, opData) in ownStack:
# 			match opType:
# 				case OperationType.SINGLE:
# 					op= opData
# 					try:
# 						calcDeriv= op.getDerivative(
# 							calculatedDerivative= calcDeriv,
# 							calculatedResult= calcResult,
# 						)
# 						calcResult= op.getResult(
# 							element= calcResult,
# 						).real
# 					except Exception as e:
# 						print( f"Error with { op.__name__} with\ncalcDeriv: { calcDeriv}\ncalcResult: { calcResult}")
# 						raise e
# 				case OperationType.ELEMENT:
# 					( op, otherElementIndex)= opData
# 					otherElementValue= elementValues[ otherElementIndex]
# 					try:
# 						calcDeriv= op.getDerivative(
# 							calculatedDerivative= calcDeriv,
# 							calculatedResult= calcResult,
# 							element1IsSelf= otherElementIndex== freeElementIndex,
# 							element1= otherElementValue,
# 						)
# 						calcResult= op.getResult(
# 							element0= calcResult,
# 							element1= otherElementValue,
# 						).real
# 					except Exception as e:
# 						print( f"Error with { op.__name__} with\ncalcDeriv: { calcDeriv}\ncalcResult: { calcResult}\notherElementValue: { otherElementValue}\notherElementIsSelf: { otherElementIndex== freeElementIndex}")
# 						raise e
# 	else:
# 		calcDeriv= 0.0
	
# 	# Loop over other stack-s
# 	for otherStackElementIndex in otherStacks:
# 		stack= function[ otherStackElementIndex]
# 		freeElementContributes= False
# 		for ( stackIndex, ( opType, opData)) in enumerate( stack):
# 			if opType== OperationType.ELEMENT:
# 				if opData[ 1]== freeElementIndex:
# 					freeElementContributes= True
# 					startStackIndex= stackIndex
# 					break
# 		if freeElementContributes== False:
# 			continue
# 		# Only calculate added calculation component-s if they are influenced by the free element

# 		stackCalcDeriv= 0.0
# 		# Calculate result up to stack start
# 		stackCalcResult= elementValues[ otherStackElementIndex]
# 		for ( opType, opData) in stack[ : startStackIndex]:
# 			match opType:
# 				case OperationType.SINGLE:
# 					op= opData
# 					stackCalcResult= op.getResult(
# 						element= stackCalcResult,
# 					).real
# 				case OperationType.ELEMENT:
# 					( op, otherElementIndex)= opData
# 					otherElementValue= elementValues[ otherElementIndex]
# 					stackCalcResult= op.getResult(
# 						element0= stackCalcResult,
# 						element1= otherElementValue,
# 					).real

# 		for ( opType, opData) in stack[ startStackIndex: ]:
# 			match opType:
# 				case OperationType.SINGLE:
# 					op= opData
# 					try:
# 						stackCalcDeriv= op.getDerivative(
# 							calculatedDerivative= stackCalcDeriv,
# 							calculatedResult= stackCalcResult,
# 						)
# 						stackCalcResult= op.getResult(
# 							element= stackCalcResult,
# 						).real
# 					except Exception as e:
# 						print( f"Error with { op.__name__} with\ncalcDeriv: { stackCalcDeriv}\ncalcResult: { stackCalcResult}")
# 						raise e
# 				case OperationType.ELEMENT:
# 					( op, otherElementIndex)= opData
# 					otherElementValue= elementValues[ otherElementIndex]
# 					try:
# 						stackCalcDeriv= op.getDerivative(
# 							calculatedDerivative= stackCalcDeriv,
# 							calculatedResult= stackCalcResult,
# 							element1IsSelf= otherElementIndex== freeElementIndex,
# 							element1= otherElementValue,
# 						)
# 						stackCalcResult= op.getResult(
# 							element0= stackCalcResult,
# 							element1= otherElementValue,
# 						).real
# 					except Exception as e:
# 						print( f"Error with { op.__name__} with\ncalcDeriv: { stackCalcDeriv}\ncalcResult: { stackCalcResult}\notherElementValue: { otherElementValue}\notherElementIsSelf: { otherElementIndex== freeElementIndex}")
# 						raise e

# 		calcDeriv+= stackCalcDeriv
# 	return calcDeriv

def displayResult(
	system: System,
	sessionState: dict,
):
	result= system.getRating()
	sessionState[ "label-s"][ "System rating"].set_label( str( result))
	for elementIndex in range( len( system.elements)):
		outputSensitivity= system.getSensitivityToElement( elementIndex)
		sessionState[ "label-s"][ "Output sensitivity to element-s"][ elementIndex].set_label( str( outputSensitivity))

def getNewResultsDisplay(
	system: System,
	sessionState: dict,
)-> Gtk.Widget:
	vbox0= Gtk.Box.new( Gtk.Orientation.VERTICAL, 7)
	resultHBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 7)
	resultHeading= Gtk.Label.new( "System rating:")
	resultHeading.set_halign( Gtk.Align.START)
	resultHeading.add_css_class( "caption-heading")
	resultHBox.append( resultHeading)
	resultLabel= Gtk.Label.new()
	resultLabel.set_halign( Gtk.Align.END)
	resultLabel.set_hexpand( True)
	resultLabel.add_css_class( "caption")
	resultLabel.add_css_class( "dim-label")
	resultHBox.append( resultLabel)
	vbox0.append( resultHBox)
	vbox0.append( Gtk.Separator.new( Gtk.Orientation.VERTICAL))
	sensHeading= Gtk.Label.new( "System rating sensitivity to each element:")
	sensHeading.set_halign( Gtk.Align.START)
	sensHeading.add_css_class( "caption-heading")
	vbox0.append( sensHeading)
	sensVBox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 3)
	indexMapping= dict()
	for elementIndex in range( len( system.elements)):
		sensHBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 3)
		sensIndexLabel= Gtk.Label.new( str( elementIndex)+ ":")
		sensIndexLabel.set_halign( Gtk.Align.START)
		sensIndexLabel.add_css_class( "caption")
		sensHBox.append( sensIndexLabel)
		sensitivityLabel= Gtk.Label.new()
		sensitivityLabel.set_halign( Gtk.Align.END)
		sensitivityLabel.set_hexpand( True)
		sensitivityLabel.add_css_class( "caption")
		sensitivityLabel.add_css_class( "dim-label")
		sensHBox.append( sensitivityLabel)
		sensVBox.append( sensHBox)
		indexMapping[ elementIndex]= sensitivityLabel
	vbox0.append( sensVBox)
	sessionState[ "label-s"][ "System rating"]= resultLabel
	sessionState[ "label-s"][ "Output sensitivity to element-s"]= indexMapping

	return vbox0
def getNewFunctionDisplay(
	system: System,
)-> Gtk.Widget:

	def populateSpace(
		space: System.Space,
	):
		elementsVBox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 10)
		firstElement= True
		for rootElementIndex in space:
			if firstElement== True:
				firstElement= False
			else:
				elementSep= Gtk.Separator.new( Gtk.Orientation.HORIZONTAL)
				elementsVBox.append( elementSep)
			operationStack: OperationStack= space[ rootElementIndex]
			elementHBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 5)
			elementIndexLabel= Gtk.Label.new( str( rootElementIndex))
			elementIndexLabel.set_halign( Gtk.Align.CENTER)
			elementIndexLabel.set_valign( Gtk.Align.START)
			elementHBox.append( elementIndexLabel)
			indexStackSep= Gtk.Separator.new( Gtk.Orientation.VERTICAL)
			elementHBox.append( indexStackSep)

			stackOpsVbox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 1)
			firstOp= True
			for operation in operationStack.iterOperations():
				if firstOp== True:
					firstOp= False
				else:
					opSep= Gtk.Separator.new( Gtk.Orientation.HORIZONTAL)
					stackOpsVbox.append( opSep)
				opHBox0= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 4)
				opTypeLabelBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 2)
				opTypeTypeLabel= Gtk.Label.new( "opType:")
				opTypeTypeLabel.add_css_class( "caption")
				opTypeTypeLabel.add_css_class( "dim-label")
				opTypeLabelBox.append( opTypeTypeLabel)
				opTypeLabel= Gtk.Label.new( operation.__class__.__base__.__name__)
				opTypeLabel.add_css_class( "caption-heading")
				opTypeLabel.add_css_class( "dim-label")
				opTypeLabel.set_halign( Gtk.Align.START)
				opTypeLabelBox.append( opTypeLabel)
				opHBox0.append( opTypeLabelBox)
				opHBoxSep= Gtk.Separator.new( Gtk.Orientation.HORIZONTAL)
				opHBox0.append( opHBoxSep)
				opHBox1= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 15)
				# opHBox1.set_halign( Gtk.Align.CENTER)
				opHBox1.set_hexpand( True)
				# opHBox1.set_homogeneous( True)
				if isinstance( operation, SingleOperation):
					opClass= operation.__class__
					opClassLabelBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 2)
					opClassTypeLabel= Gtk.Label.new( "op:")
					opClassTypeLabel.add_css_class( "caption")
					opClassTypeLabel.add_css_class( "dim-label")
					opClassLabelBox.append( opClassTypeLabel)
					opClassLabel= Gtk.Label.new( opClass.__name__)
					opClassLabel.add_css_class( "caption-heading")
					opClassLabel.add_css_class( "dim-label")
					opClassLabelBox.append( opClassLabel)
					opHBox1.append( opClassLabelBox)
				if isinstance( operation, ElementOperation):
					operation: ElementOperation
					opClass= operation.__class__
					opClassLabelBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 2)
					opClassTypeLabel= Gtk.Label.new( "op:")
					opClassTypeLabel.add_css_class( "caption")
					opClassTypeLabel.add_css_class( "dim-label")
					opClassLabelBox.append( opClassTypeLabel)
					opClassLabel= Gtk.Label.new( opClass.__name__)
					opClassLabel.add_css_class( "caption-heading")
					opClassLabel.add_css_class( "dim-label")
					opClassLabelBox.append( opClassLabel)
					opHBox1.append( opClassLabelBox)
					otherElementIndexLabelBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 2)
					otherElementIndexTypeLabel= Gtk.Label.new( "otherElementIndex:")
					otherElementIndexTypeLabel.add_css_class( "caption")
					otherElementIndexTypeLabel.add_css_class( "dim-label")
					otherElementIndexLabelBox.append( otherElementIndexTypeLabel)
					otherElementIndexLabel= Gtk.Label.new( str( operation.combinedElementIndex))
					otherElementIndexLabel.add_css_class( "caption-heading")
					otherElementIndexLabel.add_css_class( "dim-label")
					otherElementIndexLabelBox.append( otherElementIndexLabel)
					opHBox1.append( otherElementIndexLabelBox)
					otherElementLocationLabelBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 2)
					otherElementLocationTypeLabel= Gtk.Label.new( "otherElementLocation:")
					otherElementLocationTypeLabel.add_css_class( "caption")
					otherElementLocationTypeLabel.add_css_class( "dim-label")
					otherElementLocationLabelBox.append( otherElementLocationTypeLabel)
					if ( operation.combinedElementValueSource== ElementValueSourceType.SYSTEM_RATING_AFFECTOR)or ( operation.combinedElementValueSource== ElementValueSourceType.BASE_VALUE):
						otherElementSourceStr= operation.combinedElementValueSource.name
					else:
						otherElementSourceStr= f"( { operation.combinedElementValueSource[ 0].name}, { str( operation.combinedElementValueSource[ 1])})"
					otherElementLocationLabel= Gtk.Label.new( otherElementSourceStr)
					otherElementLocationLabel.add_css_class( "caption-heading")
					otherElementLocationLabel.add_css_class( "dim-label")
					otherElementLocationLabelBox.append( otherElementLocationLabel)
					opHBox1.append( otherElementLocationLabelBox)
				opHBox0.append( opHBox1)
				stackOpsVbox.append( opHBox0)
			elementHBox.append( stackOpsVbox)
			elementsVBox.append( elementHBox)
		return elementsVBox
	stackBox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 5)
	systemContribTitle= Gtk.Label.new( "Stack contributing to system rating:")
	systemContribTitle.add_css_class( "heading")
	systemContribTitle.set_halign( Gtk.Align.START)
	stackBox.append( systemContribTitle)
	systemContribWidget= populateSpace( system.function[ 0])
	stackBox.append( systemContribWidget)
	for ( utilSpaceIndex, utilSpace) in enumerate( system.function[ 1]):
		utilSpaceTitle= Gtk.Label.new( f"Util space { utilSpaceIndex}:")
		utilSpaceTitle.add_css_class( "heading")
		utilSpaceTitle.set_halign( Gtk.Align.START)
		stackBox.append( utilSpaceTitle)
		utilSpaceWidget= populateSpace( utilSpace)
		stackBox.append( utilSpaceWidget)
	return stackBox


def elementNumberUpdated(
	elementNumberWidget,
	elementNumber: int,
	system: System,
	elementValueEditor,
	resultsBin: Adw.Bin,
	resultsValueDisplays: dict,
):
	# Construct new result-s pane
	# Alter element-s in system
	existingValues= system.elements.copy()
	currentElementNumber= len( existingValues)
	if currentElementNumber!= elementNumber:
		if currentElementNumber> elementNumber:
			newValues= existingValues[ : elementNumber]
		else:
			newValues= existingValues+ [ 0.0 for _ in elementNumber- currentElementNumber]
		system.setElements( newValues)
		elementValueEditor.setValue( newValues)
		widget= getNewResultsDisplay( system, resultsValueDisplays)
		resultsBin.set_child( widget)

def updateElementNumber(
	newElementNumber: int,
	oldElementValues: list[ float],
)-> list[ float]:
	oldELementNumber= len( oldElementValues)
	if oldELementNumber> newElementNumber:
		newValues= oldElementValues[ : newElementNumber]
	else:
		newValues= oldElementValues+ [ 0.0 for _ in range( newElementNumber- oldELementNumber)]
	return newValues
	

def calculateNewGraphDisplay(
	system: System,
	sessionState: "SessionState",
	desiredElementIndex: int,
):
	# Display:
	# - System result
	# - System sensitivity to element ( calculated)
	# - System sensitivity to element ( estimated ( central difference))

	currentElementValue= system.elements[ desiredElementIndex]
	settings= sessionState[ "graphSetting-s"]
	displayAmount= settings.displayAmount
	displayDivisions= settings.displayDivisions
	# calculate start and then after each step
	calculationStart= currentElementValue- ( displayAmount/ 2)
	calculationStepCount= displayDivisions+ 1
	calculationStep= displayAmount/ calculationStepCount
	differenceApproxSize= calculationStep* 2

	locationArray= list()
	valueArray= list()
	for calculationIndex in range( 1+ calculationStepCount):
		distanceFromStart= calculationIndex* calculationStep
		calculationLocation= calculationStart+ distanceFromStart
		locationArray.append( calculationLocation)

		system.setElementValue(
			index= desiredElementIndex,
			value= calculationLocation,
		)
		systemRating= system.getRating()
		sRSensitivity= system.getSensitivityToElement(
			freeElementIndex= desiredElementIndex,
		)
		if calculationIndex> 1:
			# calculate central difference of that behind the current
			difference= ( systemRating- valueArray[ calculationIndex- 2][ 0])/ differenceApproxSize
			valueArray[ calculationIndex- 1][ 2]= difference

		valueArray.append( [ systemRating, sRSensitivity, 0.0])
	# Add on central difference to array for first and last
	if len( valueArray)> 2:
		valueArray[ 0][ 2]= valueArray[ 1][ 2]
		valueArray[ -1][ 2]= valueArray[ -2][ 2]
	valueNPArray= numpy.array( valueArray)
	sessionState[ "graph"].axis.clear()
	( sRLine, sRSensLineCalc, sRSensLineApprox)= sessionState[ "graph"].axis.plot( locationArray, valueNPArray)
	sessionState[ "graph"].axis.legend( ( sRLine, sRSensLineCalc, sRSensLineApprox), ( "System rating", "Rating sensitivity ( calculated)", "Rating sensitivity ( approximated)"))
	sessionState[ "graph"].figureCanvas.draw()

	system.setElementValue(
		index= desiredElementIndex,
		value= currentElementValue
	)

def graphInvalidation(
	system: System,
	sessionState: "SessionState",
):
	desiredElementIndex= sessionState[ "graphSetting-s"].viewedElement
	if desiredElementIndex!= None:
		calculateNewGraphDisplay(
			system= system,
			sessionState= sessionState,
			desiredElementIndex= desiredElementIndex,
		)

@specificationed
class GraphSettings:
	displayAmount: float= 28.12102
	displayDivisions: int= 400
	viewedElement: int| None= 0


LabelResultState= TypedDict( "LabelResultState", {
	"System rating": Gtk.Label,
	"Output sensitivity to element-s": list[ Gtk.Label],
})
SessionState= TypedDict( "SessionState", {
	"label-s": LabelResultState,
	"graph": DataVisualiser,
	"graphSetting-s": GraphSettings,
})

def unconstainedLeversUiGen()-> Gtk.Widget:
	global premadeFunctions
	hpane0= NicerPaned( Gtk.Orientation.HORIZONTAL)
~~~~~~
~~~~~{.python .numberLines}
	hpane0.set_shrink_end_child( False)
	hpane0.set_shrink_start_child( False)
	vbox0= Gtk.Box.new( Gtk.Orientation.VERTICAL, 8)
	# elementNumberPicker= Gtk.SpinButton.new( None, 1.0, 1)
	elementsTitle= Gtk.Label.new( "Number of element-s")
	elementsTitle.add_css_class( "caption")
	elementsTitle.add_css_class( "dim-label")
	elementsTitle.set_halign( Gtk.Align.START)
	vbox0.append( elementsTitle)
	elementNumberPicker= getEditingWidgetList( int)[ 1][ 0]()
	elementNumberPicker.setMinimum( 3)
	vbox0.append( elementNumberPicker)
	# alterElementNumberButton= Gtk.Button.new_with_label( "Alter element number")
	# vbox0.append( alterElementNumberButton)
	vbox0.append( Gtk.Separator.new( Gtk.Orientation.HORIZONTAL))
	elementValueTitle= Gtk.Label.new( "Value of element-s")
	elementValueTitle.add_css_class( "caption")
	elementValueTitle.add_css_class( "dim-label")
	elementValueTitle.set_halign( Gtk.Align.START)
	vbox0.append( elementValueTitle)
	elementValueEditor= getEditingWidgetList( tuple[ float])[ 1][ 0]()
	elementValueEditor.set_vexpand( True)
	# elementValuesPlaceholderBin= Gtk.ScrolledWindow.new()
	# elementValuesPlaceholderBin.set_vexpand( True)
	vbox0.append( elementValueEditor)
	hpane0.set_start_child( vbox0)
	hpane1= NicerPaned( Gtk.Orientation.HORIZONTAL)
	hpane1.set_shrink_end_child( False)
	hpane1.set_shrink_start_child( False)
	functionVbox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 7)
	functionCreationBox= Gtk.Box.new( Gtk.Orientation.HORIZONTAL, 4)
	premadeFunctionSelectionButton= Gtk.MenuButton.new()
	premadeFunctionSelectionButton.set_label( "Select premade function")
	premadeFunctionSelectionToggleButton= premadeFunctionSelectionButton.get_first_child()
	premadeFunctionSelectionToggleButton.add_css_class( "pill")
	premadeFunctionSelectionToggleButton.add_css_class( "thinner")
	premadeFunctionSelectionToggleButton.add_css_class( "caption")
	premadeFunctionSelectionButton.set_halign( Gtk.Align.START)
	premadeFunctionSelectionButton.set_valign( Gtk.Align.CENTER)
	premadeFunctionSelection= ListViewPopover()
	premadeFunctionSelection.setItems( premadeFunctions.keys())
	premadeFunctionSelectionButton.set_popover( premadeFunctionSelection)
	functionCreationBox.append( premadeFunctionSelectionButton)
	...
	orSep= Gtk.Box.new( Gtk.Orientation.VERTICAL, 3)
	orSepSep0= Gtk.Separator.new( Gtk.Orientation.VERTICAL)
	orSepSep0.set_halign( Gtk.Align.CENTER)
	orSepSep0.set_size_request( -1, 22)
	orSep.append( orSepSep0)
	orSepLabel= Gtk.Label.new( "or")
	orSepLabel.add_css_class( "caption")
	orSepLabel.add_css_class( "dim-label")
	orSep.append( orSepLabel)
	orSepSep1= Gtk.Separator.new( Gtk.Orientation.VERTICAL)
	orSepSep1.set_halign( Gtk.Align.CENTER)
	orSepSep1.set_size_request( -1, 22)
	orSep.append( orSepSep1)
	orSep.set_valign( Gtk.Align.CENTER)
	functionCreationBox.append( orSep)
	randomFunctionButton= Gtk.Button.new_with_label( "Create random function")
	randomFunctionButton.add_css_class( "pill")
	randomFunctionButton.add_css_class( "thinner")
	randomFunctionButton.add_css_class( "caption")
	randomFunctionButton.set_halign( Gtk.Align.START)
	randomFunctionButton.set_valign( Gtk.Align.CENTER)
	functionCreationBox.append( randomFunctionButton)
	functionCreationBox.set_halign( Gtk.Align.CENTER)
	functionCreationBox.set_valign( Gtk.Align.CENTER)
	functionVbox.append( functionCreationBox)
	functionVbox.append( Gtk.Separator.new( Gtk.Orientation.VERTICAL))
	beep= Gtk.Label.new( "Complex function viewing and editing is disabled")
	beep.add_css_class( "caption")
	beep.add_css_class( "dim-label")
	functionVbox.append( beep)
	# functionViewer= getEditingWidgetList( dict[ int, Any])[ 1][ 0]()
	# functionViewer.set_sensitive( False)
	# functionViewer= Gtk.Label.new()
	# functionViewer.add_css_class( "caption")
	# functionViewer.add_css_class( "dim-label")
	functionViewScroll= Gtk.ScrolledWindow.new()
	functionViewScroll.set_vexpand( True)
	functionViewScroll.set_propagate_natural_width( True)
	functionVbox.append( functionViewScroll)
	hpane1.set_start_child( functionVbox)
	...
	resultVbox= Gtk.Box.new( Gtk.Orientation.VERTICAL, 8)
	resultBin= Adw.Bin.new()
	resultVbox.append( resultBin)
	graph= DataVisualiser()
	resultVbox.append( graph)
	gSButton= Gtk.MenuButton.new()
	gSPopover= Gtk.Popover.new()
	graphSettingsEditor= getEditingWidgetList( GraphSettings)[ 1][ 0]()
	initialGraphSettings= GraphSettings()
	graphSettingsEditor.setValue( initialGraphSettings)
	gSPopover.set_child( graphSettingsEditor)
	gSButton.set_popover( gSPopover)
	resultVbox.append( gSButton)
	hpane1.set_end_child( resultVbox)
	hpane0.set_end_child( hpane1)

	# Upon initialisation
	#    set system elements, set system function, set element number editor, set element value editor, create func widget, create results widget, update displayed result-s
	# When element number updated:
	#    update element value editor, update system elements, re calculate the result widget, recalculate the displayed results, increment system iteration( for cache purposes)
	# When element values update-ed
	#    recalculate the displayed result-s, update system elements, increment system iteration
	# when new function created
	#    if updating element number
	#        set system elements, set system function, change element num editor, change element value editor, recreate results widget, recreate function widget, recalculate display, increment system iteration
	#    else
	#        set system function, recreate function widget, recalculate display, increment system iteration
	

	sessionState= {
		"label-s": dict(),
		"graph": graph,
		"graphSetting-s": initialGraphSettings,
	}
	# Default setup, 3 element-s, no function
	defaultElementNumber= 3
	elementNumberPicker.setValue( defaultElementNumber)
	elements= [ 0.0 for _ in range( defaultElementNumber)]
	system= System(
		elementValues= elements,
		baseFunction= ( {}, []),
	)
	elementValueEditor.setValue( elements)
	updateFunctionView= lambda: functionViewScroll.set_child( getNewFunctionDisplay( system))
	updateFunctionView()
	updateResultDisplay= lambda: resultBin.set_child( getNewResultsDisplay(
		system= system,
		sessionState= sessionState,
	))
	updateResultDisplay()
	displayResult(
		system= system,
		sessionState= sessionState,
	)


	def elementNumberPickerUpdated( picker, newNumber):
		nonlocal system, sessionState, elementValueEditor
		currentNumber= len( system.elements)
		if currentNumber== newNumber:
			return
		newValues= updateElementNumber(
			newElementNumber= newNumber,
			oldElementValues= system.elements
		)
		elementValueEditor.setValue( newValues)
		system.setElements( newValues)
		updateResultDisplay()
		displayResult(
			system= system,
			sessionState= sessionState,
		)
		graphInvalidation(
			system= system,
			sessionState= sessionState,
		)

	elementNumberPicker.connect( "valueUpdated", elementNumberPickerUpdated)
	def elementValueEditorUpdated( editor, newValuesTuple: tuple[ float]):
		nonlocal system, sessionState
		newValues= list( newValuesTuple)
		graphViewedElement= sessionState[ "graphSetting-s"].viewedElement
		if graphViewedElement!= None:
			for elementIndex in range( len( system.elements)):
				if newValues[ elementIndex]!= system.elements[ elementIndex]:
					if elementIndex!= graphViewedElement:
						# Graph is invalid
						graphInvalidation(
							system= system,
							sessionState= sessionState,
						)
						break
		system.setElements( newValues)
		displayResult(
			system= system,
			sessionState= sessionState,
		)
	elementValueEditor.connect( "valueUpdated", elementValueEditorUpdated)

	def randomFunctionOnClick( button):
		nonlocal elementNumberPicker, system, sessionState
		newBaseFunc= createRandomBaseFunction(
			elementNumber= elementNumberPicker.getValue(),
		)
		system.setFunctionFromBase( newBaseFunc)
		print( system.function)
		updateFunctionView()
		displayResult(
			system= system,
			sessionState= sessionState,
		)
		graphInvalidation(
			system= system,
			sessionState= sessionState,
		)
	randomFunctionButton.connect( "clicked", randomFunctionOnClick)

	def premadeFunctionOnClick( selectedItem):
		nonlocal system, elementValueEditor, elementNumberPicker
		( desiredElementNumber, newBaseFunc)= premadeFunctions[ selectedItem]
		currentElementNumber= elementNumberPicker.getValue()
		if desiredElementNumber!= currentElementNumber:
			newValues= updateElementNumber(
				newElementNumber= desiredElementNumber,
				oldElementValues= system.elements,
			)
			elementNumberPicker.setValue( desiredElementNumber)
			elementValueEditor.setValue( newValues)
			system.setElements( newValues)
			updateResultDisplay()
		newBaseFunc= deepcopy( newBaseFunc)
		system.setFunctionFromBase( newBaseFunc)
		updateFunctionView()
		displayResult(
			system= system,
			sessionState= sessionState,
		)
		graphInvalidation(
			system= system,
			sessionState= sessionState,
		)
	premadeFunctionSelection.connectSignalWithFunction( "itemChosen", premadeFunctionOnClick)

	def graphSettingsUpdated(
		editor,
		newValue: GraphSettings,
	):
		nonlocal sessionState, system
		sessionState[ "graphSetting-s"]= newValue
		graphInvalidation(
			system= system,
			sessionState= sessionState,
		)
	graphSettingsEditor.connect( "valueUpdated", graphSettingsUpdated)

	return hpane0
~~~~~~
\normalsize

---

  \
  \
  \
  \
  \
  \
  \
  
Probability Concepts
---
This implements probability theory concepts starting from a probability space with common set operations such as the power set,  
and then expanding to define concepts such as { distributions, random variables, product spaces, expectation, variance, independance, conditionality, stochastic processes, markov chains, markov chain classification, steady state probabilities}  
It also includes a Matrix definition which includes inversion and equation system solving  
  

---

\normalsize
~~~~~{.python .numberLines}
"""
Whilst initial support was given to continuous spaces, they were abandoned until a full implementation can be attended to
For now only finite discrete spaces are fully supported
"""
from __future__ import annotations
import os
from pprint import pprint
import random
from typing import(
	Any,
	ForwardRef,
	TypedDict,
	TypeVar,
	Protocol,
	Union,
	# Self,
	ClassVar,
)
from collections.abc import(
	Callable,
	Collection,
	Sequence,
)
from dataclasses import(
	dataclass,
	field,
)
import math
from functools import(
	partial,
)

SpaceDataType= TypeVar( "SpaceDataType")
CountableSampleSpace= set[ SpaceDataType]
SampleSpaceSubset= CountableSampleSpace

SequenceType= TypeVar( "SequenceType")
def mult(
	q,
	w,
):
	return q* w
def add(
	q,
	w,
):
	return q+ w
def sub(
	q,
	w,
):
	return q- w

def apply(
	function: Callable[
		[
			SequenceType,
			SequenceType,
		],
		SequenceType,
	],
	sequence: Sequence,
)-> Union[
	None,
	SequenceType,
]:
	if len( sequence)== 0:
		return None
	currentItem= sequence[ 0]
	for item in sequence[ 1: ]:
		currentItem= function(
			currentItem,
			item,
		)
	return currentItem

def setUnion(
	q: set,
	w: set,
)-> set:
	for inputSet in ( q, w,):
		if isinstance(
			inputSet,
			(
				OpenInterval,
				UncountableProductSampleSpace,
			),
		):
			raise Exception( "Union between non discrete sets is not supported")
	return q.union( w)
	
def setIntersection(
	q: set,
	w: set,
)-> set:
	for inputSet in ( q, w,):
		if isinstance(
			inputSet,
			(
				OpenInterval,
				UncountableProductSampleSpace,
			),
		):
			raise Exception( "Union between non discrete sets is not supported")
	return q.intersection( w)

def integrateOverInterval(
	function,
	interval: ForwardRef( "OpenInterval"),
):
	integrationSteps= 20
	sum= 0.0
	offset= 0.00000000001
	step= ( ( interval.end- interval.start)- offset)/ integrationSteps
	location= offset
	for _ in range( integrationSteps):
		sum+= step* function( location)
		location+= step
	return sum

ValueType= TypeVar( "ValueType")
def cartesianSetProduct(
	*sets: Sequence[
		set[ ValueType]
	]
)-> set[
	tuple[
		ValueType,
		...,
	],
]:
	setLen= len( sets)
	def w(
		currentIndex: int,
		inputTuple: tuple[ ValueType],
	):
		currentSet= sets[ currentIndex]
		nextIndex= currentIndex+ 1
		for value in currentSet:
			currentTuple= inputTuple+ ( value,)
			if nextIndex== setLen:
				yield currentTuple
			else:
				for result in w(
					currentIndex= nextIndex,
					inputTuple= currentTuple
				):
					yield result

	return set(
		w(
			currentIndex= 0,
			inputTuple= tuple(),
		),
	)


InputType= TypeVar( "InputType")
def powerSet( inputSet: set[ InputType])-> set[
	frozenset[ InputType]
]:
	inputLen= len( inputSet)
	inputList= list( inputSet)
	powerSet= list()
	totalLen= int( math.pow( 2, inputLen))
	for iterationNumber in range(
		0,
		totalLen,
	):
		addedSet= list()
		binRep= bin( iterationNumber)
		binRepLen= len( binRep)
		for binRepNumber in range( binRepLen- 2):
			index= -1- binRepNumber
			if binRep[ index]== "1":
				addedSet.append( inputList[ index])
		powerSet.append( frozenset( addedSet))
	return set( powerSet)


def smallestSigmaAlgebra(
	# typing is wrong
	sampleSpace: CountableSampleSpace,
	inputSet: set[ SampleSpaceSubset],
):
	sigmaAlgebra= {
		frozenset(),
	}
	if isinstance(
		sampleSpace,
		(
			OpenInterval,
			UncountableProductSampleSpace,
		),
	):
		raise NotImplementedError( "Finding the smallest sigma algebra using a continuous sample space is not currently supported due to the calculation of the complement of items of the input set not being implemented")
		sigmaAlgebra.add( sampleSpace)
	else:
		sigmaAlgebra.add(
			frozenset( sampleSpace),
		)
	sigmaAlgebra= sigmaAlgebra.union( inputSet)
	inputSetUnions= set()
	for subSet in inputSet:
		if isinstance( subSet, OpenInterval):
			raise NotImplementedError( "Finding the smallest sigma algebra from a set which contains continuous members is not currently supported due to the calculation of ( the union between two continuous sets and the union between a continuous and non continuous set) not being implemented")
		subSet= frozenset( subSet)
		for existingSet in inputSetUnions.copy():
			inputSetUnions.add( existingSet.union( subSet))
		inputSetUnions.add( subSet)

	# add the complements
	for existingSet in inputSetUnions.copy():
		inputSetUnions.add(
			frozenset( sampleSpace.difference( existingSet))
		)
	return sigmaAlgebra.union(
		inputSetUnions,
	)

def chancesOfKOccurencesInNUniformTrials(
	n: int,
	k: int,
)-> int:
	return math.factorial( n)/ ( math.factorial( k)* math.factorial( n- k))

def greatestCommonDivisor( numbers: Collection[ int])-> int:
	numbers= list( numbers)
	numbersLen= len( numbers)
	if numbersLen< 2:
		if numbersLen== 1:
			return numbers[ 0]
		else:
			raise ValueError( "Input collection is empty")
	def greatestCommonDivisor2(
		q: int,
		w: int,
	)-> int:

		( largest, smallest)= ( q, w) if q> w else ( w, q)
		while True:
			result= largest% smallest
			if result== 0:
				return smallest
			largest= smallest
			smallest= result
	result= numbers[ 0]
	for number in numbers[ 1:]:
		result= greatestCommonDivisor2(
			q= result,
			w= number,
		)
	return result

class Matrix:
	@classmethod
	def identity(
		cls,
		size: int,
	):
		return cls(
			content= [
				[
					1.0 if ( column== row) else 0.0
					for column in range( size)
				]
				for row in range( size)
			]
		)
	def __init__(
		self,
		content: Sequence[
			Sequence[ float],
		], # sequence of sequence of column values, so sequence of rows
	):
		self.content= content
		self.rows= len( content)
		if self.rows> 0:
			self.columns= len( content[ 0])
		else:
			self.columns= 0

	def copy( self)-> Self:
		return Matrix(
			content= [
				row.copy()
				for row in self.content
			]
		)
	def __getitem__(
		self,
		index: int,
	)-> float:
		return self.content[ index]


	@staticmethod
	def mutatingAddition(
		q: Self,
		w: Any,
		subtract: bool,
	):
		if subtract:
			op= sub
		else:
			op= add
		if isinstance(
			w,
			(
				float,
				int,
			),
		):
			for rowIndex in range( q.rows):
				for columnIndex in range( q.columns):
					q.content[ rowIndex][ columnIndex]= op(
						q.content[ rowIndex][ columnIndex],
						w,
					)
		elif isinstance(
			w,
			Matrix,
		):
			if ( q.rows, q.columns)!= ( w.rows, w.columns):
				raise NotImplementedError
			
			for rowIndex in range( q.rows):
				for columnIndex in range( q.columns):
					q.content[ rowIndex][ columnIndex]= op(
						q.content[ rowIndex][ columnIndex],
						w.content[ rowIndex][ columnIndex],
					)
		else:
			raise NotImplementedError
	def __add__(
		self,
		other,
	):
		new= self.copy()
		self.mutatingAddition(
			q= new,
			w= other,
			subtract= False,
		)
		return new
	def __iadd__(
		self,
		other,
	):
		self.mutatingAddition(
			q= self,
			w= other,
			subtract= False,
		)
	def __sub__(
		self,
		other,
	):
		new= self.copy()
		self.mutatingAddition(
			q= new,
			w= other,
			subtract= True,
		)
		return new
	def __isub__(
		self,
		other,
	):
		self.mutatingAddition(
			q= self,
			w= other,
			subtract= True,
		)

	@staticmethod
	def mutatingMultiplication(
		q: Self,
		w: Any,
	):
		if isinstance(
			w,
			(
				float,
				int,
			),
		):
			for rowIndex in range( q.rows):
				for columnIndex in range( q.columns):
					q.content[ rowIndex][ columnIndex]*= w
		elif isinstance(
			w,
			Matrix,
		):
			if q.columns!= w.rows:
				raise NotImplementedError
			else:
				indexs= list( range( q.columns))
				rows= list()
				for rowIndex in range( q.rows):
					row= list()
					for columnIndex in range( w.columns):
						row.append(
							sum(
								map(
									lambda index: q.content[ rowIndex][ index]* w.content[ index][ columnIndex],
									indexs,
								)
							)
						)
					rows.append( row)
				q.content= rows
				q.rows= q.rows
				q.columns= w.columns
		else:
			raise NotImplementedError
	def __mul__(
		self,
		other,
	):
		new= self.copy()
		self.mutatingMultiplication(
			q= new,
			w= other,
		)
		return new
	def __imul__(
		self,
		other,
	):
		self.mutatingMultiplication(
			q= self,
			w= other,
		)


	@staticmethod
	def mutatingElimination(
		matrix: Self,
		equalRowNumbers: list[ float],
	):
		if len( equalRowNumbers)!= matrix.rows:
			raise ValueError( "Row numbers must match")

		if matrix.rows!= matrix.columns:
			raise NotImplementedError( "Only square matrix-s are currently supported")

		def move(
			source: int,
			sink: int,
		):
			nonlocal matrix, equalRowNumbers
			matrix.content.insert(
				sink,
				matrix.content.pop( source),
			)
			equalRowNumbers.insert(
				sink,
				equalRowNumbers.pop( source),
			)

		for unknownIndex in range( matrix.columns):
			currentPivotCoefficient= matrix.content[ unknownIndex][ unknownIndex]
			if currentPivotCoefficient== 0:
				foundRow= False
				for equationNumber in range(
					unknownIndex+ 1,
					matrix.rows,
				):
					currentPivotCoefficient= matrix.content[ equationNumber][ unknownIndex]
					if currentPivotCoefficient!= 0:
						foundRow= True
						break
				if not foundRow:
					raise NotImplementedError( "Column re-order-ing is not supported")
				move(
					source= equationNumber,
					sink= unknownIndex,
				)

			for equationNumber in range(
				unknownIndex+ 1,
				matrix.rows,
			):
				belowUnknownCoefficient= matrix.content[ equationNumber][ unknownIndex]
				if belowUnknownCoefficient!= 0:
					currentEquationMult= belowUnknownCoefficient/ currentPivotCoefficient
					for currentEquationUnknownIndex in range( matrix.columns):
						if currentEquationUnknownIndex== unknownIndex:
							continue
						matrix.content[ equationNumber][ currentEquationUnknownIndex]-= ( matrix.content[ unknownIndex][ currentEquationUnknownIndex]* currentEquationMult)
					equalRowNumbers[ equationNumber]-= ( equalRowNumbers[ unknownIndex]* currentEquationMult)

					matrix.content[ equationNumber][ unknownIndex]= 0.0

		knownValues= list()
		for unknownNumber in range( matrix.rows):
			unknownIndex= equationIndex= ( matrix.rows- 1)- unknownNumber
			knownCoefficientTotal= 0.0
			for knownCoefficientBackIndex in range(
~~~~~~
~~~~~{.python .numberLines}
				unknownNumber* -1,
				0,
			):
				knownCoefficientTotal+= matrix.content[ equationIndex][ knownCoefficientBackIndex]* knownValues[ knownCoefficientBackIndex]
			equalRowNumbers[ equationIndex]-= knownCoefficientTotal
			equalRowNumbers[ equationIndex]/= matrix.content[ equationIndex][ unknownIndex]
			knownValues.insert(
				0,
				equalRowNumbers[ equationIndex],
			)
		return knownValues

	def solveEquations(
		self,
		constants: list[ float],
	)-> list[ float]:
		return self.mutatingElimination(
			matrix= self.copy(),
			equalRowNumbers= constants.copy(),
		)


	@staticmethod
	def mutatingInversion( matrix: Self):
		"""
		Instead of duplicating work, one could make a general form converger function
		and then elimination uses the yield-(.) to modify a equality constant matrix
		inversion uses the yield-(.) to modify the identity
		"""
		if matrix.rows!= matrix.columns:
			raise ValueError( "Matrix must be square")
		size= matrix.rows
		identity= Matrix.identity( size)

		def move(
			source: int,
			sink: int,
		):
			nonlocal matrix, identity
			matrix.content.insert(
				sink,
				matrix.content.pop( source),
			)
			identity.content.insert(
				sink,
				identity.content.pop( source),
			)

		for index in range( size):
			currentPivotValue= matrix.content[ index][ index]
			if currentPivotValue== 0:
				foundRow= False
				for rowNumber in range(
					index+ 1,
					size,
				):
					currentPivotValue= matrix.content[ rowNumber][ index]
					if currentPivotValue!= 0:
						foundRow= True
						break
				if not foundRow:
					raise NotImplementedError( "Column re-order-ing is not supported")
				move(
					source= rowNumber,
					sink= index,
				)

			for rowNumber in range( size):
				if rowNumber== index:
					continue
				otherValue= matrix.content[ rowNumber][ index]
				if otherValue!= 0:
					currentRowMult= otherValue/ currentPivotValue
					for columnNumber in range( size):
						if columnNumber== index:
							matrix.content[ rowNumber][ columnNumber]= 0.0
						else:
							matrix.content[ rowNumber][ columnNumber]-= ( matrix.content[ index][ columnNumber]* currentRowMult)

						identity.content[ rowNumber][ columnNumber]-= ( identity.content[ index][ columnNumber] * currentRowMult)

		for index in range( size):
			value= matrix.content[ index][ index]
			identity.content[ index]= [
				columnValue/ value
				for columnValue in identity.content[ index]
			]
		return identity

	def getInverse( self)-> Self:
		return self.mutatingInversion(
			matrix= self.copy(),
		)

	def transpose( self)-> Self:
		return Matrix(
			content= [
				[
					self[ rowIndex][ columnIndex]
					for rowIndex in range( self.rows)
				]
				for columnIndex in range( self.columns)
			]
		)


def choose(
	choices: dict[
		Any,
		float,
	],
)-> Any:
	number= random.random()
	total= 0.0
	for choice in choices:
		total+= choices[ choice]
		if total> number:
			return choice




ProbabilityFunction= Callable[
	[
		SampleSpaceSubset,
	],
	float,
]

@dataclass
class OpenInterval:
	start: Union[
		math.inf,
		float,
	]
	end: Union[
		math.inf,
		float,
	]
	def __hash__( self):
		return hash(
			(
				self.start,
				self.end,
			),
		)

class UncountableProductSampleSpace():
	def __init__(
		self,
		base: Union[
			CountableSampleSpace,
			OpenInterval,
		],
	):
		self.base= base
		self.multipliedSpace= list()

	def multiply(
		self,
		newSpaces: list[
			Union[
				CountableSampleSpace,
				OpenInterval,
			]
		],
	):
		self.multipliedSpace+= newSpaces
	def __hash__( self):
		return hash(
			(
				self.base,
				tuple( self.multipliedSpace),
			),
		)

SampleSpace= Union[
	CountableSampleSpace,
	OpenInterval,
	UncountableProductSampleSpace,
]
EventType= Union[
	SampleSpaceSubset,
	OpenInterval,
]
SigmaAlgebra= set[ EventType]

class ProbabilitySequenceData( TypedDict):
	previousValues: list[
		list[ SpaceDataType],
	]
	sequencePosition: Union[
		int,
		None,
	]

ProbabilityMassOrDensityFunction= Callable[
	[
		SpaceDataType,
		SampleSpace,
		ProbabilitySequenceData,
	],
	float,
]

AbstractSpaceDataType= TypeVar( "AbstractSpaceDataType")
@dataclass
class RandomVariable:
# @dataclass
# class RandomVariable[ AbstractSpaceDataType]:
	abstractToRandom: Callable[
		[
			AbstractSpaceDataType,
		],
		float,
	]
	randomToAbstract: Callable[
		[
			float,
		],
		AbstractSpaceDataType,
	]
	probabilityMassFunction: Union[
		ProbabilityMassOrDensityFunction,
		None,
	]= None
	probabilityDensityFunction: Union[
		ProbabilityMassOrDensityFunction,
		None,
	]= None

RandomVariableViabilityVerifier= Callable[
	[
		ForwardRef( "ProbabilitySpace")
	],
	bool,
]

@dataclass
class SequenceialProbabilityConfiguration:
	previousValues: list[ slice]= field(
		default_factory= list,
	)
	affectedByMutatingClosure: bool= True
	affectedBySequencePosition: bool= False

class MarkovStationaryGroupType( TypedDict):
	members: set[ int]
	recurrance: bool
	period: Union[
		None,
		int,
	]

@dataclass(
	init= False,
)
class ProbabilitySpace:
	sampleSpace: CountableSampleSpace
	sigmaAlgebra: SigmaAlgebra
	probabilityMassOrDensityFunction: ProbabilityMassOrDensityFunction
	
	continuous: bool
	knownSampleSpaceRealStatus: Union[
		bool,
		None,
	]


	def __init__(
		self,
		sampleSpace: SampleSpace,
		sigmaAlgebra: SigmaAlgebra,
		probabilityMassOrDensityFunction: Union[
			ProbabilityMassOrDensityFunction,
			None,
		],
		knownSampleSpaceRealStatus: Union[
			bool,
			None,
		]= None,
		sequenceConfiguration: Union[
			SequenceialProbabilityConfiguration,
			None,
		]= None,
	):
		if sequenceConfiguration== None:
			sequenceConfiguration= SequenceialProbabilityConfiguration()
		self.continuous= isinstance(
			sampleSpace,
			(
				OpenInterval,
				UncountableProductSampleSpace,
			)
		)
		self.sampleSpace= sampleSpace
		self.sigmaAlgebra= sigmaAlgebra
		self.probabilityMassOrDensityFunction= probabilityMassOrDensityFunction
		self.knownSampleSpaceRealStatus= knownSampleSpaceRealStatus
		self.instanceDefinedRandomVariables= dict()
		self.propertys= dict()
		self.propertysUnderValidation= set()
		self.propertysUnderRetrieval= set()
		self.sequenceConfiguration= sequenceConfiguration
		self.markovStationaryClassifications= None

	class PropertyDefinitionSpace:
		class Validation:
			alwaysPresent= lambda space: True
			markovPositionUnrelated= lambda space: space.getPropertyValue( "markovian") and ( not space.getPropertyValue( "probabilityRelatedToSequencePosition"))
			markovPositionUnrelatedIndexs= lambda space: ProbabilitySpace.PropertyDefinitionSpace.Validation.markovPositionUnrelated( space) and space.getPropertyValue( "integerIndexSampleSpace")
			@staticmethod
			def irreducableMarkov( space):
				if ProbabilitySpace.PropertyDefinitionSpace.Validation.markovPositionUnrelatedIndexs( space):
					classes= space.getPropertyValue( "groups")
					if len( classes):
						return True
				return False

		class Retrieval:
			@staticmethod
			def sampleSpaceRealStatusRetrieval( space):
				spaceType= type( space.sampleSpace)
				if spaceType== OpenInterval:
					realStatus= True
				elif spaceType== UncountableProductSampleSpace:
					raise Exception
					realStatus= False
				else:
					realStatus= True
					for member in space.sampleSpace:
						if not isinstance(
							member,
							(
								int,
								float,
							),
						):
							realStatus= False
							break
				return realStatus

			integerSampleSpace= lambda space: frozenset( space.sampleSpace)== frozenset( range( len( space.sampleSpace)))

			@staticmethod
			def markovian( space):
				previousValues= space.sequenceConfiguration.previousValues
				if len( previousValues)== 0:
					return True
				else:
					if len( previousValues)== 1:
						if previousValues[ 0].start== -1:
							if previousValues[ 0].stop== None:
								return True
				return False

			@staticmethod
			def probabilityRelatedToSequencePosition( space):
				return space.sequenceConfiguration.affectedBySequencePosition or space.sequenceConfiguration.affectedByMutatingClosure

			@staticmethod
			def dependsOnSequence( space):
				return space.sequenceConfiguration.affectedBySequencePosition or ( len( space.sequenceConfiguration.previousValues)> 0)


			getTransitionProbabilities= lambda space: Matrix(
				content= [
					[
						space.probabilityMassOrDensityFunction(
							nextStateColumnIndex,
							space.sampleSpace,
							{
								"previousValues": [ [ currentStateRowIndex]] if ( len( space.sequenceConfiguration.previousValues)== 1) else [],
								"sequencePosition": None,
							},
						)
						for nextStateColumnIndex in range(
							len( space.sampleSpace)
						)
					]
					for currentStateRowIndex in range(
						len( space.sampleSpace)
					)
				]
			)

			@staticmethod
			def getAccessibilities( space):
				transitionMatrix: Matrix= space.getPropertyValue( "transitionProbabilities")
				accessibilities= dict()
				locatorDependencies= dict()
				locatorDependants= dict()
				currentChain= set()
				def ensurePrescenceAndAdd(
					dictionary: dict,
					locator,
					value,
				):
					if locator in dictionary:
						dictionary[ locator].add( value)
					else:
						dictionary[ locator]= { value,}
				def copyDependencies(
					old,
					new,
				):
					nonlocal locatorDependencies, locatorDependants
					dependencies= locatorDependencies[ old].copy()
					if new in dependencies:
						dependencies.remove( new)
					for dependency in dependencies:
						ensurePrescenceAndAdd(
							locatorDependants,
							dependency,
							new,
						)
					if new not in locatorDependencies:
						locatorDependencies[ new]= dependencies
					else:
						locatorDependencies[ new]= locatorDependencies[ new].union( dependencies)
				def elementAccessibilities( element: int)-> set[ SpaceDataType]:
					nonlocal accessibilities, locatorDependencies, locatorDependants, currentChain
					currentChain.add( element)
					accessibilities[ element]= { element,}
					# elementAccessibles= { element,}
					for connected in range( len( space.sampleSpace)):
						if connected== element:
							continue
						if transitionMatrix[ element][ connected]!= 0.0:
							if connected in currentChain:
								ensurePrescenceAndAdd(
									locatorDependencies,
									element,
									connected,
								)
								ensurePrescenceAndAdd(
									locatorDependants,
									connected,
									element,
								)
							else:
								if connected in locatorDependencies:
									accessibilities[ element]= accessibilities[ element].union( accessibilities[ connected])
									copyDependencies(
										old= connected,
										new= element,
									)
								else:
									accessibilities[ element].add( connected)
									if connected in accessibilities:
										connectedAccessibles= accessibilities[ connected]
									else:
										connectedAccessibles= elementAccessibilities( connected)
									accessibilities[ element]= accessibilities[ element].union( connectedAccessibles)
									if connected in locatorDependencies:
										copyDependencies(
											old= connected,
											new= element,
										)
									
					if element in locatorDependants:
						for dependant in locatorDependants[ element]:
							accessibilities[ dependant]= accessibilities[ dependant].union( accessibilities[ element])
							copyDependencies(
								old= element,
								new= dependant,
							)
							locatorDependencies[ dependant].remove( element)
						locatorDependants.pop( element)

					currentChain.remove( element)
					return accessibilities[ element]

				for element in space.sampleSpace:
					if element not in accessibilities:
						elementAccessibilities( element)

				return accessibilities

			@staticmethod
			def getGroups( space):
				def walkPeriod(
					start: int,
					current: int,
					group: set[ int],
					iteration: int,
					transitionMatrix: Matrix,
					encountered: set[ int],
					periods: set[ int],
				):
					iteration+= 1
					encountered.add( current)
					for ( connectedIndex, connectedValue) in enumerate( transitionMatrix[ current]):
						if connectedValue> 0.0:
							if connectedIndex in group:
								if connectedIndex== start:
									periods.add( iteration)
								else:
									if connectedIndex!= current:
										if connectedIndex not in encountered:
~~~~~~
~~~~~{.python .numberLines}
											walkPeriod(
												start= start,
												current= connectedIndex,
												group= group,
												iteration= iteration,
												transitionMatrix= transitionMatrix,
												encountered= encountered.copy(),
												periods= periods,
											)

				transitionMatrix= space.getPropertyValue( "transitionProbabilities")
				accessibles= space.getPropertyValue( "accessibilities")
				remaining= set( range( transitionMatrix.rows))
				taken= set()
				groups= list()
				for element in remaining:
					if element in taken:
						continue
					group= { element,}
					nonMemberContactable= False
					for accessible in accessibles[ element]:
						if element in accessibles[ accessible]:
							group.add( accessible)
							taken.add( accessible)
						else:
							nonMemberContactable= True

					recurrance= nonMemberContactable== False
					if recurrance:
						periods= set()
						walkPeriod(
							start= element,
							current= element,
							group= group,
							iteration= 0,
							transitionMatrix= transitionMatrix,
							encountered= set(),
							periods= periods,
						)
						period= greatestCommonDivisor( periods) 
					else:
						period= None
					groups.append(
						{
							"members": group,
							"recurrance": recurrance,
							"period": period,
						},
					)

				return groups

			@staticmethod
			def getTransientStatesMeanTimes( space):
				groups= space.getPropertyValue( "groups")
				transitionMatrix= space.getPropertyValue( "transitionProbabilities")
				transientStates= set()
				for group in groups:
					if group[ "recurrance"]== False:
						transientStates|= group[ "members"]
				# reduce transition matrix to transient states
				transientTransitionMatrix= Matrix(
					content= [
						[
							transitionMatrix[ rowIndex][ columnIndex]
							for columnIndex in range( transitionMatrix.columns)
							if columnIndex in transientStates
						]
						for rowIndex in range( transitionMatrix.rows)
						if rowIndex in transientStates
					]
				)
				return (
					Matrix.identity(
						size= transientTransitionMatrix.columns,
					)- transientTransitionMatrix
				).getInverse()

			@staticmethod
			def getSteadyStateTransitionProbabilities( space):
				transitionMatrix= space.getPropertyValue( "transitionProbabilities")
				equations= transitionMatrix.transpose()- Matrix.identity( transitionMatrix.rows)
				equations.content[ 0]= [ 1.0 for _ in range( equations.columns)]
				constants= [ 0.0 for _ in range( equations.rows)]
				constants[ 0]= 1.0
				steadyStateProbabilities= equations.solveEquations(
					constants= constants,
				)
				return steadyStateProbabilities

			@staticmethod
			def getReversedTransitionProbabilities( space):
				transitionMatrix= space.getPropertyValue( "transitionProbabilities")
				steadyStateProbabilities= space.getPropertyValue( "steadyStateTransitionProbabilities")

				rows= list()
				for forwardColumnIndex in range( transitionMatrix.columns):
					ownProbability= steadyStateProbabilities[ forwardColumnIndex]
					if ownProbability== 0.0:
						accumulateTotal= True
						total= 0.0
					else:
						accumulateTotal= False
					row= list()
					for forwardRowIndex in range( transitionMatrix.rows):
						transitionLikelyhood= steadyStateProbabilities[ forwardRowIndex]* transitionMatrix[ forwardRowIndex][ forwardColumnIndex]
						if accumulateTotal:
							total+= transitionLikelyhood
							row.append( transitionLikelyhood)
						else:
							row.append( transitionLikelyhood/ ownProbability)

					if accumulateTotal:
						row= list(
							map(
								lambda likelyhood: likelyhood/ total,
								row,
							)
						)
					rows.append( row)
				return Matrix(
					content= rows,
				)

		definitions= {
			"sampleSpaceRealStatus": (
				bool,
				Validation.alwaysPresent,
				Retrieval.sampleSpaceRealStatusRetrieval,
			),
			"markovian": (
				bool,
				Validation.alwaysPresent,
				Retrieval.markovian,
			),
			"probabilityRelatedToSequencePosition": (
				bool,
				Validation.alwaysPresent,
				Retrieval.probabilityRelatedToSequencePosition,
			),
			"dependsOnSequence": (
				bool,
				Validation.alwaysPresent,
				Retrieval.dependsOnSequence,
			),
			"integerIndexSampleSpace": (
				bool,
				Validation.alwaysPresent,
				Retrieval.integerSampleSpace,
			),
			"transitionProbabilities": (
				Matrix,
				Validation.markovPositionUnrelatedIndexs,
				Retrieval.getTransitionProbabilities,
			),
			"accessibilities": (
				dict[ SpaceDataType, set[ SpaceDataType]],
				Validation.markovPositionUnrelated,
				Retrieval.getAccessibilities,
			),
			"groups": (
				list[ MarkovStationaryGroupType],
				Validation.markovPositionUnrelated,
				Retrieval.getGroups,
			),
			"transientStatesMeanTimes": (
				Matrix,
				Validation.markovPositionUnrelatedIndexs,
				Retrieval.getTransientStatesMeanTimes,
			),
			"steadyStateTransitionProbabilities": (
				list[ float],
				Validation.markovPositionUnrelated,
				Retrieval.getSteadyStateTransitionProbabilities,
			),
			"reversedTransitionProbabilities": (
				Matrix,
				Validation.irreducableMarkov,
				Retrieval.getReversedTransitionProbabilities,
			),
		}
	class PropertyNotDefined( ValueError):pass
	class PropertyNotValidForInstance( ValueError):pass

	@classmethod
	def getPropertyDefinition(
		cls,
		propertyName: str,
	):
		if propertyName in cls.PropertyDefinitionSpace.definitions:
			return cls.PropertyDefinitionSpace.definitions[ propertyName]
		else:
			raise cls.PropertyNotDefined( propertyName)
	def getPropertyValue(
		self,
		propertyName: str,
	):
		if propertyName in self.propertys:
			value= self.propertys[ propertyName]
			if isinstance(
				value,
				self.PropertyNotValidForInstance,
			):
				raise value
			else:
				return value
		else:
			(
				_,
				validityFunction,
				retrievalFunction,
			)= self.getPropertyDefinition( propertyName)
			self.propertysUnderValidation.add( propertyName)
			valid= validityFunction( self)
			self.propertysUnderValidation.remove( propertyName)
			if not valid:
				error= self.PropertyNotValidForInstance( propertyName)
				self.propertys[ propertyName]= error
				raise error

			self.propertysUnderRetrieval.add( propertyName)
			value= retrievalFunction( self)
			self.propertysUnderRetrieval.remove( propertyName)
			self.propertys[ propertyName]= value
			return value

	def hasProperty(
		self,
		propertyName: str,
	)-> bool:
		if propertyName in self.propertys:
			return not isinstance(
				self.propertys[ propertyName],
				self.PropertyNotValidForInstance,
			)
		else:
			(
				_,
				validityFunction,
				_,
			)= self.getPropertyDefinition( propertyName)
			return validityFunction( self)

	def eventProbability(
		self,
		event: EventType
	)-> float:
		if self.sigmaAlgebra!= None: # allowing None due to computation constraint
			assert event in self.sigmaAlgebra
		if self.getPropertyValue( "dependsOnSequence"):
			raise Exception( "Space depends on sequence and can not be called outside of that context")

		if self.continuous:
			if not isinstance(
				event,
				OpenInterval,
			):
				# discrete points have a probability of 0.0
				return 0.0
			return integrateOverInterval(
				function= lambda location: self.probabilityMassOrDensityFunction(
					location,
					self.sampleSpace,
					{
						"previousValues": [],
						"sequencePosition": None,
					}
				),
				interval= event,
			)
		else:
			if isinstance(
				event,
				OpenInterval,
			):
				raise Exception( "Interval events for discrete probability spaces is not supported")
			sum= 0.0
			for member in event:
				sum+= self.probabilityMassOrDensityFunction(
					member,
					self.sampleSpace,
					{
						"previousValues": [],
						"sequencePosition": None,
					}
				)
			return sum

	@staticmethod
	def getProductSpace(
		spaces: Sequence[ Self],
	)-> Self:
		discreteSpaces= list()
		intervalSpaces= list()
		combinedSpaces= list()
		for space in spaces:
			if isinstance( space.sampleSpace, OpenInterval):
				intervalSpaces.append( space)
			elif isinstance( space.sampleSpace, UncountableProductSampleSpace):
				combinedSpaces.append( space)
			else:
				discreteSpaces.append( space)
		# discreteLen= len( discreteSpaces)
		intervalLen= len( intervalSpaces)
		combinedLen= len( combinedSpaces)
		if ( intervalLen+ combinedLen)== 0:
			sampleSpace= cartesianSetProduct(
				*[
					space.sampleSpace
					for space in discreteSpaces
				]
			)
			spaceLen= len( spaces)
			def w(
				currentIndex: int,
				inputSubSetSequence: tuple,
			):
				currentSpace= spaces[ currentIndex]
				nextIndex= currentIndex+ 1
				for subSet in currentSpace.sigmaAlgebra:
				# for value in currentSet:
					currentSubSetSequence= inputSubSetSequence+ ( subSet,)
					if nextIndex== spaceLen:
						yield frozenset(
							cartesianSetProduct(
								*currentSubSetSequence
							)
						)
					else:
						for result in w(
							currentIndex= nextIndex,
							inputSubSetSequence= currentSubSetSequence
						):
							yield result
			sigmaAlgebra= smallestSigmaAlgebra(
				sampleSpace= sampleSpace,
				inputSet= set(
					w(
						currentIndex= 0,
						inputSubSetSequence= tuple(),
					)
				),
			)
			# need to creat
		else:
			base= None
			multiplications= list()
			for space in spaces:
				if base== None:
					base= UncountableProductSampleSpace(
						base= space,
					)
				else:
					multiplications.append( space)
			base.multiply(
				newSpaces= multiplications,
			)
			sampleSpace= base
			# wrong i believe
			sigmaAlgebra= smallestSigmaAlgebra(
				sampleSpace= sampleSpace,
				inputSet= cartesianSetProduct(
					*[
						space.sigmaAlgebra
						for space in discreteSpaces
					]
				),
			)

		
		def f( value, sampleSpace):
			apply(
				mult,
				[
					spaces[ index].probabilityMassOrDensityFunction(
						constituantValue,
						spaces[ index].sampleSpace,
						{
							"previousValues": [],
							"sequencePosition": None,
						}
					)
					for ( index, constituantValue) in value
				]
			)
		return ProbabilitySpace(
			sampleSpace= sampleSpace,
			sigmaAlgebra= sigmaAlgebra,
			probabilityMassOrDensityFunction= lambda value, sampleSpace, sequenceData: apply(
				mult,
				[
					spaces[ index].probabilityMassOrDensityFunction(
						constituantValue,
						spaces[ index].sampleSpace,
						{
							"previousValues": [],
							"sequencePosition": None,
						}
					)
					for ( index, constituantValue) in enumerate( value)
				]
			),
		)

	def __mul__(
		self,
		other,
	):
		return self.getProductSpace(
			spaces= [
				self,
				other,
			],
		)
	
	classDefinedRandomVariables: ClassVar[
		dict[
			str,
			tuple[
				RandomVariableViabilityVerifier,
				RandomVariable,
			],
		]
	]= {
	}
	instanceDefinedRandomVariables: dict[
		str,
		RandomVariable,
	]

	def defineRandomVariable(
		self,
		name: str,
		randomVariable: RandomVariable,
	):
		self.instanceDefinedRandomVariables[ name]= randomVariable


	def getRandomVariableSampleSpace(
		self,
		name: str,
	)-> Self:
		sampleSpaceType= type( self.sampleSpace)
		randomVariable: RandomVariable= self.instanceDefinedRandomVariables[ name]
		if self.continuous:
			raise NotImplementedError( "Transformations of continuous spaces are not implemented")
		else:
			if randomVariable.probabilityMassFunction!= None:
				massFunction= randomVariable.probabilityMassFunction
			else:
				def massFunction(
					element,
					sampleSpace: SampleSpace,
					sequenceData: ProbabilitySequenceData,
				)-> float:
					return self.probabilityMassOrDensityFunction(
						randomVariable.randomToAbstract( element),
						self.sampleSpace,
						{
							"previousValues": [],
							"sequencePosition": None,
						},
					)
			space= ProbabilitySpace(
				sampleSpace= {
					randomVariable.abstractToRandom( element)
					for element in self.sampleSpace
				},
				sigmaAlgebra= {
					frozenset(
						{
							randomVariable.abstractToRandom( element)
							for element in sampleSubSet
						}
					)
					for sampleSubSet in self.sigmaAlgebra
				},
				probabilityMassOrDensityFunction= massFunction,
				knownSampleSpaceRealStatus= True,
			)

		return space

	def getExpectedValue(
		self,
		functionOnSelf: Union[
			Callable[
				[ float,],
				float
			],
			None,
		]= None
	)-> float:
		assert self.getPropertyValue( "sampleSpaceRealStatus")
		if functionOnSelf== None:
			iterationFunction= lambda value: value* self.probabilityMassOrDensityFunction(
				value,
				self.sampleSpace,
				{
					"previousValues": [],
~~~~~~
~~~~~{.python .numberLines}
					"sequencePosition": None,
				},
			)
		else:
			iterationFunction= lambda value: functionOnSelf( value)* self.probabilityMassOrDensityFunction(
				value,
				self.sampleSpace,
				{
					"previousValues": [],
					"sequencePosition": None,
				},
			)
		if self.continuous:
			if isinstance(
				self.sampleSpace,
				OpenInterval,
			):
				return integrateOverInterval(
					function= iterationFunction,
					interval= self.sampleSpace,
				)
			else:
				raise NotImplementedError( "Integration over arbitrary continuous domain not implemented")
		else:
			return sum(
				map(
					iterationFunction,
					self.sampleSpace,
				)
			)
	def getVariance( self)-> float:
		return self.getExpectedValue(
			functionOnSelf= lambda element: element** 2,
		)- ( self.getExpectedValue()** 2)

	def getStandardDeviation( self)-> float:
		return math.sqrt( self.getVariance())

	def conditionalProbability(
		self,
		events: Sequence,
	):
		return self.eventProbability(
			setIntersection(
				events[ 0],
				events[ 1],
			),
		)/ self.eventProbability( events[ 0])

	def testEventIndependance(
		self,
		events: Collection[ set],
	)-> bool:
		eventsLen= len( events)
		if eventsLen< 2:
			if eventsLen< 1:
				raise ValueError( "Length of events must be 1 or more")
			else:
				return True

		return math.isclose(
			self.eventProbability(
				apply(
					setIntersection,
					events,
				),
			),
			apply(
				mult,
				list(
					map(
						self.eventProbability,
						events,
					)
				),
			),
		)

		
	def getBinomialDistributionSpace(
		self,
		sampleNumber: int,
		sucsessEvent: set,
	):
		assert sampleNumber> 0
		assert not self.continuous
		sampleSpace= set(
			range( sampleNumber+ 1),
		)
		sucsessProbability= self.eventProbability( sucsessEvent)
		failureProbability= 1- sucsessProbability
		def probabilityMassFunction(
			trialNumber: int,
			sampleSpace: SampleSpace,
			sequenceData: ProbabilitySequenceData,
		):
			return chancesOfKOccurencesInNUniformTrials(
				n= sampleNumber,
				k= trialNumber,
			)* math.pow(
				sucsessProbability,
				trialNumber,
			)* math.pow(
				failureProbability,
				sampleNumber- trialNumber,
			)

		return ProbabilitySpace(
			sampleSpace= sampleSpace,
			sigmaAlgebra= smallestSigmaAlgebra(
				sampleSpace= sampleSpace,
				inputSet= {
					frozenset( { element})
					for element in sampleSpace
				},
			),
			probabilityMassOrDensityFunction= probabilityMassFunction,
			knownSampleSpaceRealStatus= True,
		)

	@classmethod
	def getHyperGeometricDistribution(
		cls,
		sampleNumber: int,
		typeNumbers: tuple[ int],
	):
		totalNumber= sum( typeNumbers)
		assert sampleNumber<= totalNumber

		setLen= len( typeNumbers)
		sets= [
			set( range( max+ 1))
			for max in typeNumbers
		]
		def w(
			currentIndex: int,
			inputTuple: tuple[ ValueType],
			total: int,
		):
			currentSet= sets[ currentIndex]
			nextIndex= currentIndex+ 1
			remaining= sampleNumber- total
			for value in currentSet:
				if value> remaining:
					continue
				currentTuple= inputTuple+ ( value,)
				if nextIndex== setLen:
					if sum( currentTuple)== sampleNumber:
						yield currentTuple
				else:
					for result in w(
						currentIndex= nextIndex,
						inputTuple= currentTuple,
						total= total+ value,
					):
						yield result

		sampleSpace= set(
			w(
				currentIndex= 0,
				inputTuple= tuple(),
				total= 0,
			),
		)
		def massFunction(
			element,
			sampleSpace,
			sequenceData,
		):
			assert sum( element)== sampleNumber
			assert len( element)== setLen
			i= 0
			for typeNumber in element:
				assert typeNumber<= typeNumbers[ i]
				i+= 1
			return apply(
				mult,
				list(
					map(
						lambda typeInfo: chancesOfKOccurencesInNUniformTrials(
							n= typeNumbers[ typeInfo[ 0]],
							k= typeInfo[ 1],
						),
						list(
							enumerate( element),
						),
					),
				)
			)/ chancesOfKOccurencesInNUniformTrials(
				n= totalNumber,
				k= sampleNumber,
			)

		return ProbabilitySpace(
			sampleSpace= sampleSpace,
			# sigmaAlgebra= smallestSigmaAlgebra(
			# 	sampleSpace= sampleSpace,
			# 	inputSet= {
			# 		frozenset( { element})
			# 		for element in sampleSpace
			# 	}
			# ),
			probabilityMassOrDensityFunction= massFunction,
			sigmaAlgebra= None, # computation issue
			knownSampleSpaceRealStatus= False,
		)
	

	# so there is backward forward or joint search mechanisms to find the probability of a single state given a known signal
	# given a sequence of signals and predicting the matching state sequence, one can optimise for the most correct states or for the most acurate whole sequence prediction
	# they both use similar concepts of forward and backward
	def getHiddenSampleSpace(
		self,
		emmisionProbabilitys: Matrix,
		emmisionRealisation: Sequence[ int],
		initialProbabilitys: list[ float]| None= None,
	):
		if initialProbabilitys== None:
			initialProbabilitys= self.getPropertyValue( "steadyStateTransitionProbabilities")
		transitionMatrix= self.getPropertyValue( "transitionProbabilities")
		sequenceLen= len( emmisionRealisation)
		eventProbabilitys= dict()
		def w(
			currentIndex: int,
			inputTuple: tuple[ ValueType],
			currentProbability: float,
		):
			nonlocal eventProbabilitys
			nextIndex= currentIndex+ 1
			for state in self.sampleSpace:
				if currentIndex== 0:
					stateProbability= initialProbabilitys[ state]
				else:
					previousState= inputTuple[ -1]
					stateProbability= transitionMatrix[ previousState][ state]
				currentTuple= inputTuple+ ( state,)
				currentStateProbability= currentProbability* stateProbability* emmisionProbabilitys[ state][ emmisionRealisation[ currentIndex]]
				if currentStateProbability!= 0.0:
					if nextIndex== sequenceLen:
						eventProbabilitys[ currentTuple]= currentStateProbability
					else:
						w(
							currentIndex= nextIndex,
							inputTuple= currentTuple,
							currentProbability= currentStateProbability,
						)

		w(
			currentIndex= 0,
			inputTuple= tuple(),
			currentProbability= 1.0,
		)
		sampleSpace= set( eventProbabilitys.keys())

		def probabilityMassFunction(
			realisation,
			sampleSpace,
			sequenceData,
		):
			if realisation in eventProbabilitys:
				return eventProbabilitys[ realisation]
			else:
				return 0.0

		return ProbabilitySpace(
			sampleSpace= sampleSpace,
			# sigmaAlgebra= smallestSigmaAlgebra(
			# 	sampleSpace= sampleSpace,
			# 	inputSet= { frozenset( sampleSpace)},
			# ),
			sigmaAlgebra= None,
			probabilityMassOrDensityFunction= probabilityMassFunction,
			knownSampleSpaceRealStatus= False,
			sequenceConfiguration= SequenceialProbabilityConfiguration(
				previousValues= [],
				affectedByMutatingClosure= False,
				affectedBySequencePosition= False,
			),
		)


	def getNStepProbability(
		self,
		initialState: SpaceDataType,
		desiredState: SpaceDataType,
		stepNumber: int= 1,
		initialSequencePosition: int= 0,
	):
		if stepNumber== 0:
			return initialState== desiredState
		else:
			if stepNumber== -1:
				transitionMatrix= self.getPropertyValue( "reversedTransitionProbabilities")
			elif stepNumber== 1:
				transitionMatrix= self.getPropertyValue( "transitionProbabilities")
			elif stepNumber> 1:
				transitionMatrix= self.getPropertyValue( "transitionProbabilities").copy()
				for _ in range( stepNumber- 1):
					transitionMatrix*= transitionMatrix
			else:
				transitionMatrix= self.getPropertyValue( "reversedTransitionProbabilities")
				for _ in range( stepNumber- 1):
					transitionMatrix*= transitionMatrix
			return transitionMatrix[ initialState][ desiredState]

	# given a start state, go forward and back, obvious with markovian, what about non?, return sequence, then process for display
	# if i was fancy, i would allow calculation of the reversed transition matrix from using n step probabilitys from some point and not just using the steady state-(.)
	def realiseSequence(
		self,
		initialState: SpaceDataType,
		initialSequencePosition: int= 0,
		forwardSteps: int= 0,
		backwardSteps: int= 0,
		includeInitial: bool= True,
		join: bool= True,
	)-> tuple[
		list[ SpaceDataType],
		list[ SpaceDataType],
		list[ SpaceDataType],
	]:
		backwards= list()
		forwards= list()
		# So return 
		if ( backwardSteps> 0):
			# only supporting backward form steady state
			groups= self.getPropertyValue( "groups")
			for group in groups:
				if initialState in group[ "members"]:
					if group[ "recurrance"]== False:
						raise ValueError( "Can not calculate backwards from a state which is not feasible in a steady state")
					break
			reversedProbabilitys= self.getPropertyValue( "reversedTransitionProbabilities")
			previous= initialState
			for _ in range( backwardSteps):
				previous= choose(
					choices= {
						index: reversedProbabilitys[ previous][ index]
						for index in range( reversedProbabilitys.columns)
					},
				)
				backwards.append( previous)
			backwards.reverse()
		if ( forwardSteps> 0):
			if self.hasProperty( "transitionProbabilities"):
				transitionMatrix= self.getPropertyValue( "transitionProbabilities")
				previous= initialState
				for _ in range( forwardSteps):
					previous= choose(
						choices= {
							index: transitionMatrix[ previous][ index]
							for index in range( transitionMatrix.columns)
						},
					)
					forwards.append( previous)
			else:
				for sequencePosition in range(
					initialSequencePosition+ 1,
					initialSequencePosition+ 1+ forwardSteps,
				):
					currentSequence= backwards+ [ initialState]+ forwards
					iterationPreviousValues= [
						currentSequence[ slicer]
						for slicer in self.sequenceConfiguration.previousValues
					]
					choices= {
						element: self.probabilityMassOrDensityFunction(
							element,
							self.sampleSpace,
							{
								"previousValues": iterationPreviousValues,
								"sequencePosition": sequencePosition if self.sequenceConfiguration.affectedBySequencePosition else None,
							},
						)
						for element in self.sampleSpace
					}
					choice= choose( choices)
					forwards.append( choice)

		middle= [ initialState] if includeInitial else []
		if join:
			return backwards+ middle+ forwards
		else:
			return (
				backwards,
				middle,
				forwards,
			)


print( "\033c")

try:
	import matplotlib.pyplot as pyplot
	pyplot.switch_backend( "QtAgg")
	plotting= True
	figure= pyplot.figure()
	currentAxis= figure.add_axes( [ 0.15, 0.1, 0.8, 0.8], visible= False)
	def addAxis():
		global currentAxis
		currentAxis.set_visible( False)
		currentAxis= figure.add_axes( [ 0.15, 0.1, 0.8, 0.8], visible= False)
		currentAxis.set_visible( True)
	pyplot.show( block= False)
	print( "A plotting window is opened, this is used to draw charts to, it will not re open if closed\n")
except (
	ModuleNotFoundError,
	ImportError,
):
	plotting= False

def br():
	print( "")
	input( "Press enter to continue:")
	if plotting:
		currentAxis.set_visible( False)
		figure.canvas.draw()
	( width, _)= os.get_terminal_size()
	print( "")
	print( "".join( [ "=" for _ in range( width)]))
	print( "")


if not plotting:
	print( "matplotlib and pyqt are not present, therefore plotting is disabled")
	br()
	print( "")


diceSampleSpace= { 1, 2, 3, 4, 5, 6}
print( "A finite discrete sample can be chosen: ", diceSampleSpace)
diceSigmaAlgebra= powerSet( diceSampleSpace)
print( "The power set can be taken as the sigma algebra: ", diceSigmaAlgebra)
print( "The probability mass function can be the uniform distribution")
diceProbabilitySpace= ProbabilitySpace(
	sampleSpace= diceSampleSpace,
	sigmaAlgebra= diceSigmaAlgebra,
	probabilityMassOrDensityFunction= lambda item, sampleSpace, sequenceData: 1/ len( sampleSpace),
)
event= { 1, 2}
print( f"The probability of the event { event} is therefore { diceProbabilitySpace.eventProbability( event)}")
br()


# lineSampleSpace= OpenInterval( -1, 1)
# lineTopology= {
# 	frozenset( { -0.3}),
# 	frozenset( { -0.2, 0.63}),
# }
# lineProbabilitySpace= ProbabilitySpace(
# 	sampleSpace= lineSampleSpace,
# 	sigmaAlgebra= smallestSigmaAlgebra(
# 		sampleSpace= lineSampleSpace,
# 		inputSet= lineTopology,
# 	),
# 	probabilityMassOrDensityFunction= lambda item, sampleSpace: 0.5,
# )

otherSampleSpace= { "q", "w"}
print( "Another finite discrete sample can be chosen: ", otherSampleSpace)
otherSigmaAlgebra= powerSet( otherSampleSpace)
print( "The power set can be taken as the sigma algebra: ", otherSigmaAlgebra)
print( "The probability mass function can also be the uniform distribution")

otherProbabilitySpace= ProbabilitySpace(
	sampleSpace= otherSampleSpace,
	sigmaAlgebra= powerSet( otherSampleSpace),
	probabilityMassOrDensityFunction= lambda item, sampleSpace, sequenceData: 1/ len( sampleSpace),
)

br()

print( "The product space can then be taken")
combinedSpace= diceProbabilitySpace* otherProbabilitySpace
print( "The sample space is therefore", combinedSpace.sampleSpace)
event= { ( 5, "q")}
print( f"The probability of the event { event} is therefore { combinedSpace.eventProbability( event)}")
event= { ( 2, "w"), ( 2, "q")}
print( f"The probability of the event { event} is therefore { combinedSpace.eventProbability( event)}")
br()

def to( element):
	return element[ 0]+ ord( element[ 1])

def fromz( element):
	for char in otherProbabilitySpace.sampleSpace:
		asciiInt= ord( char)
		complement= element- asciiInt
		if complement in diceProbabilitySpace.sampleSpace:
			return ( complement, char,)
print( "A real valued random variable can be taken on this combined space which adds the number to the ascii number of the character")
combinedSpace.defineRandomVariable(
	name= "addition",
	randomVariable= RandomVariable(
		abstractToRandom= to,
		randomToAbstract= fromz,
	),
)


randomSpace= combinedSpace.getRandomVariableSampleSpace( "addition")
~~~~~~
~~~~~{.python .numberLines}
print( "The random variable sample space is therefore: ", randomSpace.sampleSpace)
print( "The expected value can be calculated as: ", randomSpace.getExpectedValue())
print( "The variance can be calculated as: ", randomSpace.getVariance())
print( "The standard deviation can be calculated as: ", randomSpace.getStandardDeviation())
print( "")
multipleOf2Event= {
	number
	for number in randomSpace.sampleSpace
	if ( number% 2)== 0
}
multipleOf3Event= {
	number
	for number in randomSpace.sampleSpace
	if ( number% 3)== 0
	}
lessThanMeanEvent= {
	number
	for number in randomSpace.sampleSpace
	if number< randomSpace.getExpectedValue()
}
events= [ multipleOf2Event, lessThanMeanEvent]
print( "The conditional probability of ( the event of elements less than the expected value) given ( the event of the multiples of 2) can be found")
print( f"P( { events[ 1]} | { events[ 0]}) = ", randomSpace.conditionalProbability( events))
print( "Independance: ", randomSpace.testEventIndependance( events))
print( "")
events= [ multipleOf2Event, multipleOf3Event]
print( "The conditional probability of ( the event of the multiples of 3) given ( the event of the multiples of 2) can be found")
print( f"P( { events[ 1]} | { events[ 0]}) = ", randomSpace.conditionalProbability( events))
print( "Independance: ", randomSpace.testEventIndependance( events))

br()

independantSampleSpace= { 0, 1, 2}
independantSpace= ProbabilitySpace(
	sampleSpace= independantSampleSpace,
	sigmaAlgebra= powerSet( independantSampleSpace),
	probabilityMassOrDensityFunction= lambda element, space, sequenceData: 1/ len( space),
)
print( "The independance of events can be tested")

events= [ { 0, 1}, { 1, 2}]
print( "sample space: ", independantSampleSpace)
print( "uniform distribution")
print( "events: ", events)
print( "independance: ", independantSpace.testEventIndependance( events= events,))
print( "")


independantSampleSpace0= { 1, 2, 3, 4, 5, 6}
independantSampleSpace1= cartesianSetProduct( independantSampleSpace0, independantSampleSpace0)
qEvent= {
	( firstThrow, secondThrow)
	for ( firstThrow, secondThrow) in independantSampleSpace1
	if firstThrow== 6
}
wEvent= {
	( firstThrow, secondThrow)
	for ( firstThrow, secondThrow) in independantSampleSpace1
	if ( firstThrow+ secondThrow)== 7
}
independantSpace= ProbabilitySpace(
	sampleSpace= independantSampleSpace1,
	sigmaAlgebra= smallestSigmaAlgebra( independantSampleSpace1, { frozenset( qEvent), frozenset( wEvent), frozenset( qEvent.intersection( wEvent))}),
	probabilityMassOrDensityFunction= lambda element, space, sequenceData: 1/ len( space),
)
events= [ qEvent, wEvent]
print( "Two dice throws")
print( "sample space: ", independantSampleSpace1)
print( "uniform distribution")
print( "events, the first throw is 6 or the sum is 7: ", events)
print( "independance: ", independantSpace.testEventIndependance( events= events, ))

br()

print( "Given the space of the two dice throws, one can take the binomial distribution, where a success case is rolling the same number on both dice")
successEvent= {
	( firstThrow, secondThrow)
	for ( firstThrow, secondThrow) in independantSpace.sampleSpace
	if firstThrow== secondThrow
}
# computing the power set is not feasible here
independantSpace.sigmaAlgebra= smallestSigmaAlgebra(
	sampleSpace= independantSpace.sampleSpace,
	inputSet= { frozenset( successEvent)},
)
sampleNumber= 10
binomialSpace= independantSpace.getBinomialDistributionSpace(
	sampleNumber= sampleNumber,
	sucsessEvent= successEvent,
)

if plotting:
	addAxis()
	currentAxis.bar(
		list( range( sampleNumber+ 1)),
		[
			binomialSpace.eventProbability( { occurenceNumber})
			for occurenceNumber in range( sampleNumber+ 1)
		],
	)
	pyplot.xlabel( f"Matching dice numbers in { sampleNumber} repetitions")
	pyplot.ylabel( "Probability")
	figure.canvas.draw()
else:
	print( f"Given { sampleNumber} repetitions")
	for occurenceNumber in range( sampleNumber+ 1):
		print( f"The probability of { occurenceNumber} matching dice numbers is { binomialSpace.eventProbability( { occurenceNumber})}")



br()

# Given a sample space of types which add up to the same value
# with a withdrawal of a specific size, what is the probability of a specific withdrawal event
sampleNumber= 7
typeNumbers= ( 3, 7, 3, 6)
print( "Hyper geometric distribution")
print( f"Given a situation where { typeNumbers} different types are present and they are encountered { sampleNumber} times")
hyperGeoSpace= ProbabilitySpace.getHyperGeometricDistribution(
	sampleNumber= sampleNumber,
	typeNumbers= typeNumbers,
)
print( f"All possible withdrawals are given by: { hyperGeoSpace.sampleSpace}")
event= { ( 1, 1, 1, 4)}
print( f"The probability of the event { event} can be taken: { hyperGeoSpace.eventProbability( { ( 1, 1, 1, 4)})}")


br()

def displayMarkovSpace( space):
	print( "- A markovian probability space can be defined")
	print( "- The transition matrix can be used:")
	transitionMatrix= space.getPropertyValue( "transitionProbabilities")
	pprint( transitionMatrix.content)
	classes= space.getPropertyValue( "groups")
	print( f"\n- The classes can be found: { classes}")
	steadyStates= space.getPropertyValue( "steadyStateTransitionProbabilities")
	print( f"\n- The probability of a state occuring far enough into the future for the probability to be irrespective of the current state can be found: { steadyStates}")
	transientMeanTimes= space.getPropertyValue( "transientStatesMeanTimes")
	print( "\n- The mean number of steps that a realisation will exist in a transient state before leaving the transient class can be found:")
	pprint( transientMeanTimes.content)
	backwardsProbabilities= space.getPropertyValue( "reversedTransitionProbabilities")
	print( "\n- The matrix of probabilitys of the current state having been another state in the previous sequence step can be found:")
	pprint( backwardsProbabilities.content)

	transientState= None
	for group in classes:
		if not group[ "recurrance"]:
			transientState= list( group[ "members"])[ 0]
			break
	if transientState!= None:
		realisation= space.realiseSequence(
			initialState= transientState,
			forwardSteps= 12,
			backwardSteps= 0,
			join= False,
		)
		print( f"\n- The sequence can be realised into the future when starting from a transient state { transientState}:")
		print( realisation)
	for group in classes:
		if group[ "recurrance"]:
			recurrantState= list( group[ "members"])[ 0]
			break
	realisation= space.realiseSequence(
		initialState= recurrantState,
		forwardSteps= 12,
		backwardSteps= 7,
		join= False,
	)
	print( f"\n- The sequence can be realised into the future and past when starting from a recurrant state { recurrantState}:")
	print( realisation)

	if plotting:
		print( "\nA realisation is plotted")
		addAxis()
		steps= 100
		realisation= space.realiseSequence(
			initialState= 0,
			forwardSteps= steps,
			backwardSteps= 0,
			join= True,
		)
		currentAxis.plot(
			list( range( steps+ 1)),
			realisation,
		)
		currentAxis.grid(
			axis= "y",
		)
		pyplot.xlabel( "Sequence position")
		pyplot.ylabel( "State")
		figure.canvas.draw()

testSampleSpace= { 0, 1, 2, 3, 4, 5, 6}
testTransitionMatrix= Matrix(
	content= [
		[ 0.0, 0.3, 0.0, 0.7, 0.0, 0.0, 0.0],
		[ 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		[ 0.0, 0.2, 0.0, 0.0, 0.4, 0.4, 0.0],
		[ 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
		[ 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
		[ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0],
		# [ 0.8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2],
		[ 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
		# [ 0.0, 0.0, 0.0, 0.3, 0.5, 0.0, 0.0],
	]
)
test= ProbabilitySpace(
	sampleSpace= testSampleSpace,
	sigmaAlgebra= powerSet( testSampleSpace),
	probabilityMassOrDensityFunction= lambda element, sampleSpace, sequenceData: testTransitionMatrix[ sequenceData[ "previousValues"][ 0][ 0]][ element],
	sequenceConfiguration= SequenceialProbabilityConfiguration(
		previousValues= [ slice( -1, None)],
		affectedByMutatingClosure= False,
		affectedBySequencePosition= False,
	)
)
displayMarkovSpace( test)

br()

print( "\nagain\n")
testSampleSpace= { 0, 1, 2, 3}
testTransitionMatrix= Matrix(
	content= [
		[ 0.08, 0.632, 0.264, 0.08],
		[ 0.184, 0.368, 0.368, 0.184],
		[ 0.368, 0.0, 0.368, 0.368],
		[ 0.368, 0.0, 0.0, 0.368],
	]
).transpose()
test= ProbabilitySpace(
	sampleSpace= testSampleSpace,
	sigmaAlgebra= powerSet( testSampleSpace),
	probabilityMassOrDensityFunction= lambda element, sampleSpace, sequenceData: testTransitionMatrix[ sequenceData[ "previousValues"][ 0][ 0]][ element],
	sequenceConfiguration= SequenceialProbabilityConfiguration(
		previousValues= [ slice( -1, None)],
		affectedByMutatingClosure= False,
		affectedBySequencePosition= False,
	)
)

displayMarkovSpace( test)

if plotting:
	br()
	print( "A random walk realisation is plotted")
	addAxis()
	walkLength= 1000
	walkSpace= set(
		range(
			walkLength* -1,
			walkLength+ 1,
		)
	)
	def walkMassFunction(
		element,
		sampleSpace,
		sequenceData,
	):
		if len( sequenceData[ "previousValues"][ 0])== 0:
			previous= 0
		else:
			previous= sequenceData[ "previousValues"][ 0][ 0]
		if abs( element- previous)== 1:
			return 0.5
		else:
			return 0.0
	randomWalk= ProbabilitySpace(
		sampleSpace= walkSpace,
		sigmaAlgebra= { frozenset( {}), frozenset( walkSpace)},
		probabilityMassOrDensityFunction= walkMassFunction,
		sequenceConfiguration= SequenceialProbabilityConfiguration(
			previousValues= [ slice( -1, None)],
			affectedByMutatingClosure= False,
			affectedBySequencePosition= False,
		)
	)
	currentAxis.plot(
		list( range( walkLength)),
		randomWalk.realiseSequence(
			initialState= 0,
			forwardSteps= walkLength,
			includeInitial= False,
		)
	)
	pyplot.xlabel( "Sequence position")
	pyplot.ylabel( "State")
	figure.canvas.draw()

br()


testSampleSpace= { 0, 1, 2, 3, 4, 5, 6}
testTransitionMatrix= Matrix(
	content= [
		[ 0.0, 0.3, 0.0, 0.7, 0.0, 0.0, 0.0],
		[ 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		[ 0.0, 0.2, 0.0, 0.0, 0.4, 0.4, 0.0],
		[ 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
		[ 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
		[ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0],
		# [ 0.8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2],
		[ 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
		# [ 0.0, 0.0, 0.0, 0.3, 0.5, 0.0, 0.0],
	]
)
test= ProbabilitySpace(
	sampleSpace= testSampleSpace,
	sigmaAlgebra= powerSet( testSampleSpace),
	probabilityMassOrDensityFunction= lambda element, sampleSpace, sequenceData: testTransitionMatrix[ sequenceData[ "previousValues"][ 0][ 0]][ element],
	sequenceConfiguration= SequenceialProbabilityConfiguration(
		previousValues= [ slice( -1, None)],
		affectedByMutatingClosure= False,
		affectedBySequencePosition= False,
	)
)
emmissionMatrix= Matrix(
	content= [
		[ 0.7, 0.3, 0.0],
		[ 0.2, 0.3, 0.5],
		[ 0.1, 0.0, 0.9],
		[ 0.0, 0.7, 0.3],
		[ 0.7, 0.1, 0.2],
		[ 0.6, 0.2, 0.2],
		[ 0.7, 0.1, 0.2],
	],
)
emmissions= [ 2, 1, 1, 2, 2, 2, 1, 0, 0, 0, 0, 1]
steadyStateProbabilitys= test.getPropertyValue( "steadyStateTransitionProbabilities")
steadyHiddenSpace= test.getHiddenSampleSpace(
	emmisionProbabilitys= emmissionMatrix,
	emmisionRealisation= emmissions,
	initialProbabilitys= steadyStateProbabilitys,
)
steadyOrdered= list( steadyHiddenSpace.sampleSpace)
steadyOrdered.sort(
	key= lambda sequence: steadyHiddenSpace.probabilityMassOrDensityFunction( sequence, None, None),
	reverse= True,
)
equalStateProbabilitys= [ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
equalHiddenSpace= test.getHiddenSampleSpace(
	emmisionProbabilitys= emmissionMatrix,
	emmisionRealisation= emmissions,
	initialProbabilitys= equalStateProbabilitys,
)
equalOrdered= list( equalHiddenSpace.sampleSpace)
equalOrdered.sort(
	key= lambda sequence: equalHiddenSpace.probabilityMassOrDensityFunction( sequence, None, None),
	reverse= True,
)
print( "Given a markovian sequence with known transitions:")
pprint( testTransitionMatrix.content)
print( "\nAnd known chances of emmission:")
pprint( emmissionMatrix.content)
print( "\nAnd a sequence of emmissions:")
print( emmissions)
print( "\nUsing the steady state probabilitys:")
print( steadyStateProbabilitys)
print( "\nOne can construct a probability space of possible state sequence-s:")
print( steadyHiddenSpace.sampleSpace)
print( "\nAnd therefore determine order of likelyhood from most likely to least likely:")
print( steadyOrdered)

br()

print( "\nUsing the state probabilitys:")
print( equalStateProbabilitys)
print( "\nOne can construct a probability space of possible state sequence-s:")
print( equalHiddenSpace.sampleSpace)
print( "\nAnd therefore determine order of likelyhood from most likely to least likely:")
print( equalOrdered)

br()

if plotting:
	pyplot.close( figure)




~~~~~~
\normalsize

---

  \
  \
  \
  \
  \
  \
  \
  
Site Optimisation
---
This is a test to experiment with non linear optimisation of an arbitrary function tree. It only works with specific functions but I wish to demonstrate my explorations.  
  

---

\normalsize
~~~~~{.python .numberLines}
"""
Here the concept of a request is defined for each operation type
a addition( q, w) operation under request for maximisation will request the highest possible value from each of its contained operations
a sin( q) operation ( degrees) under request for maximisation will request an input which satisys: ( ( q- 90)% 360)== 0, as all satisfying input-(.) to sin will return 1
a max( q, w) operation under request for minimization will request an input q which satisfies: q<= w, as any value of q equal to or less than w will cause w to be returned

╭──────────────────────────────────────────────────────────────╮
│                              ╭────┬─╮                        │
│                              │mult│0│                        │
│                 ╭────────────┴────┴─┴───────╮                │
│                 │                           │                │
│                 │                           │                │
│                 ▼                           ▼                │
│               ╭────┬─╮                     ╭────┬─╮          │
│               │mult│1│                 ╭───┤mult│4│          │
│        ╭──────┴────┴─┴─╮               │   ╰────┴─┴───╮      │
│        │               │               ▼              │      │
│        │               │              ┌──┐            │      │
│        │               │              │-1│            │      │
│        ▼               ▼              └──┘            ▼      │
│    ╭─────┬─╮         ╭─────┬─╮                      ╭─────┬─╮│
│    │clamp│2│         │clamp│3│                      │clamp│5││
│ ╭──┴┬────┼─╯      ╭──┴┬────┼─╯                  ╭───┴┬────┼─╯│
│ │   │    │        │   │    │                    │    │    │  │
│ ▼   ▼    ▼        ▼   ▼    ▼                    ▼    ▼    ▼  │
│+--+ ┌──┐┌──┐    +--+  ┌──┐┌──┐                +--+  ┌───┐┌──┐│
│|x0| │-3││5 │    |x1|  │0 ││70│                |x0|  │-60││-1││
│+--+ └──┘└──┘    +--+  └──┘└──┘                +--+  └───┘└──┘│
╰──────────────────────────────────────────────────────────────╯

so as this is being done for testing purposes and no constraints are present, an assumption is made that upon recieving a maximisation request, a clamp will set the free contained value to the value of the maximium contained
in the above case, [ clamp| 2] would set x0 to 5


Implements a class `Operation` which subclasses the class `BaseOperation.Operation`
This implements `Operation.beginOptimisation`
the implementation initialises value-(.) on the operation-(.) in the tree and then runs `iterateThroughRemainingInputs`
`iterateThroughRemainingInputs` will loop through every input in the function tree
for each input it will loop through each site of that input
	for each site, it will loop through all optimisation request states that are generated for that site in the current function state
		for each request state, the current site will assign a value to the current input and then call `iterateThroughRemainingInputs` for all remaining inputs

what this means is that every possible combination of sites under every request can test out assignments and the one yielding the most optimal result is chosen

the downside is that all these choices are only informed by the path from the root to the site and not from the entire function
therefore when a function requires consideration of multiple areas of a function, the code here is not sufficient
"""
from mAAP.CustomEnum import(
	IntFlag,
	auto,
)
from typing import(
	TypedDict,
	ClassVar,
	ForwardRef,
)
from collections.abc import(
	Hashable,
	Iterator,
)
from math import(
	inf as infinity,
	sin,
)
negativeInfinity= infinity* -1
from mAAP.Tests.OptimisationTests.BaseOperation import(
	Operation as BaseOperation,
	Input,
	InputLocator,
	Constant,
	OperationType,
	OperationContainedIdentifier,
	InputValue,
	setSession,
)
from mAAP.NavigableNetwork import(
	NavigableNetwork,
	NetworkElementSpecifier,
	NetworkSubsets,
)
from mAAP.DataSpecification import(
	specificationed,
	specValue,
)

ProposedInputs= tuple[ float]
ResultDataValues= dict[ ProposedInputs, float]
class OptimizationRequests:

	class OptimizationRequest:
		def __hash__( self):
			return 0

		def decideBetweenResults(
			self,
			results: ResultDataValues,
		)-> ProposedInputs:
			"""
			expecting 1..* value in the results
			"""
			raise NotImplementedError

	class Maximization:
		def decideBetweenResults(
			self,
			results: ResultDataValues,
		)-> ProposedInputs:
			results= results.copy()
			( maximumProposed, maximumResult)= results.popitem()
			for proposedInput in results:
				if results[ proposedInput]> maximumResult:
					maximumResult= results[ proposedInput]
					maximumProposed= proposedInput
					
			return maximumProposed

	class Minimization:
		def decideBetweenResults(
			self,
			results: ResultDataValues,
		)-> ProposedInputs:
			results= results.copy()
			( minimumProposed, minimumResult)= results.popitem()
			for proposedInput in results:
				if results[ proposedInput]< minimumResult:
					minimumResult= results[ proposedInput]
					minimumProposed= proposedInput
					
			return minimumProposed

	@specificationed
	class ObtainValue:
		value: float
		def __hash__( self):
			return hash( ( OptimizationRequests.ObtainValue, self.value))

		def distance(
			self,
			value,
		):
			return abs( value- self.value)

		def decideBetweenResults(
			self,
			results: ResultDataValues,
		)-> ProposedInputs:
			results= results.copy()
			( closestProposed, initialResult)= results.popitem()
			closestDistance= self.distance( initialResult)
			for proposedInput in results:
				proposedDistance= self.distance( results[ proposedInput])
				if proposedDistance< closestDistance:
					closestDistance= proposedDistance
					closestProposed= proposedInput
					
			return closestProposed
	# class MaximizeAbsolute:
	# 	...
	# class MinimizeAbsolute:
	# 	...

@specificationed
class OptimizationRuntimeData:
	# decided: bool= True
	dependantOnInputs: set[ InputLocator]= specValue( default_factory= set)
	valueCalculable: bool= True
	setRequest: None| OptimizationRequests.OptimizationRequest= None

@specificationed
class SingleInputData:
	sites: set[ NetworkElementSpecifier]
	setValue: None| float= None
	dependantOperations: set[ ForwardRef( "Operation")]= specValue( default_factory= set)
InputData= dict[ InputLocator, SingleInputData]

@specificationed
class ResultData:
	order: tuple[ InputLocator]
	values: dict[ OptimizationRequests.OptimizationRequest, ResultDataValues]

def walkThroughRequestStates(
	inputOptimisationTypes,
	indexList,
	operationSpecifier: NetworkElementSpecifier[ ForwardRef( "Operation")],
	currentIndexListIndex= -1,
	previousOperationSpecifier= None,
)-> Iterator[ ForwardRef( "Operation")]:
	if operationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest== None:
		# Generate requests
		if previousOperationSpecifier== None:
			requests= inputOptimisationTypes
		else:
			previousIndexIntoCurrent= indexList[ currentIndexListIndex][ 1]
			requests= previousOperationSpecifier.element.generateRequests(
				request= previousOperationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest,
				desiredContained= { previousIndexIntoCurrent},
			)[ previousIndexIntoCurrent]

		currentIndexListIndex+= 1
		if currentIndexListIndex== len( indexList):
			# If last operation is not set then loop through request-(.) setting them and yielding them
			for request in requests:
				operationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= request
				yield operationSpecifier.element
				operationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= None
		else:
			( currentIndexIntoNextLinkageType, currentIndexIntoNextId)= indexList[ currentIndexListIndex]
			nextOperationSpecifier= operationSpecifier.getContainedSpecifier(
				linkageType= currentIndexIntoNextLinkageType,
				connectionId= currentIndexIntoNextId,
			)
			for request in requests:
				operationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= request
				# Walk next in index list
				# So need to get the lt and connId of contained
				for lastOperationState in walkThroughRequestStates(
					inputOptimisationTypes= None,
					indexList= indexList,
					operationSpecifier= nextOperationSpecifier,
					currentIndexListIndex= currentIndexListIndex,
					previousOperationSpecifier= operationSpecifier,
				):
					yield lastOperationState
				operationSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= None
	else:
		# if not at the end then walk to the next
		# if at the end then simply yield that
		currentIndexListIndex+= 1
		if currentIndexListIndex== len( indexList):
			yield operationSpecifier.element
		else:
			( currentIndexIntoNextLinkageType, currentIndexIntoNextId)= indexList[ currentIndexListIndex]
			nextOperationSpecifier= operationSpecifier.getContainedSpecifier(
				linkageType= currentIndexIntoNextLinkageType,
				connectionId= currentIndexIntoNextId,
			)
			for lastOperationState in walkThroughRequestStates(
				inputOptimisationTypes= None,
				indexList= indexList,
				operationSpecifier= nextOperationSpecifier,
				currentIndexListIndex= currentIndexListIndex,
				previousOperationSpecifier= operationSpecifier,
			):
				yield lastOperationState


def iterateThroughRemainingInputs(
	optimisationTypes: set[ OptimizationRequests.OptimizationRequest],
	inputData: InputData,
	remainingInputs: set[ InputLocator],
	rootOperation: ForwardRef( "Operation"),
	resultData: ResultData,
):
	for inputLocator in remainingInputs:
		for siteSpecifier in inputData[ inputLocator].sites:
			# Iterate over each request state
			# Retain state-(?) between iteration
			for lastOperationInState in walkThroughRequestStates(
				inputOptimisationTypes= optimisationTypes,
				indexList= siteSpecifier.indexList[ : -1],
				operationSpecifier= NetworkElementSpecifier(
					rootElement= rootOperation,
					element= rootOperation,
					indexList= [],
				),
				previousOperationSpecifier= None,
			):
				# Alway-s set the input
				# then
				# If there are remaining input-(.) then run through them
				# if not then calculate the final value of the function and record it
				# be sure to adjust the valueCalculable set-ing of operation-(.)
				inputValue= lastOperationInState.assignInputValue(
					containedOperation= siteSpecifier.indexList[ -1][ 1],
					request= lastOperationInState.runtimeData[ OperationType.OPTIMIZATION].setRequest,
				)
				if inputValue== None:
					continue

				# Set all affected
				setOperations= set()
				for dependantOperation in inputData[ inputLocator].dependantOperations:
					# Check if all other input dependencies are met
					dependenciesMet= True
					for dependantOnInput in dependantOperation.runtimeData[ OperationType.OPTIMIZATION].dependantOnInputs.difference( { inputLocator}):
						if inputData[ dependantOnInput].setValue== None:
							dependenciesMet= False

					if dependenciesMet:
						setOperations.add( dependantOperation)
						dependantOperation.runtimeData[ OperationType.OPTIMIZATION].valueCalculable= True

				inputData[ inputLocator].setValue= inputValue

				# If remaining input-(?) then run again with those
				# If no remaining input-(?) then calculate function
				newlyRemainingInputs= remainingInputs.copy()
				newlyRemainingInputs.remove( inputLocator)
				if len( newlyRemainingInputs)> 0:
					iterateThroughRemainingInputs(
						optimisationTypes= optimisationTypes,
						inputData= inputData,
						remainingInputs= newlyRemainingInputs,
						rootOperation= rootOperation,
						resultData= resultData,
					)
				else:
					resultData.values[ rootOperation.runtimeData[ OperationType.OPTIMIZATION].setRequest][ tuple( ( inputData[ orderedInputLocator].setValue for orderedInputLocator in resultData.order))]= rootOperation.calculateValue()

				inputData[ inputLocator].setValue= None

				for newlySetOperation in setOperations:
					newlySetOperation.runtimeData[ OperationType.OPTIMIZATION].valueCalculable= False
					# Clear any cache
					newlySetOperation.resultCache= None
					
				
				# Make sure to set the value calculable for any op-(.) that are altered by any set variable

			# So have request-(?) for the operation above the input with the network state properly configured
			# For each of these, the k

			# previousOperation= None
			# for descendingSpecifier in rootOperation.walkInNetworkSubset(
			# 	networkSubset= NetworkSubsets.RootToLocation(
			# 		location= siteSpecifier.getContainingSpecifier()[ 0],
			# 		walkType= NavigableNetwork.WalkType.FIRST_ROOT_TO_TIP,
			# 	),
			# ):
			# 	if descendingSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest== None:
			# 		if previousOperation== None:
			# 			# root
			# 			requests= optimisationTypes
			# 		else:
			# 			descendingOperationLocator= descendingSpecifier.indexList[ -1][ 1]
			# 			requests= previousOperation.generateRequests(
			# 				request= previousOperation.runtimeData[ OperationType.OPTIMIZATION].setRequest,
			# 				desiredContained= { descendingOperationLocator},
			# 			)[ descendingOperationLocator]
			# 		for request in requests:
			# 			descendingSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= request
			# 			...
			# 			descendingSpecifier.element.runtimeData[ OperationType.OPTIMIZATION].setRequest= None
			# 	previousOperation= descendingSpecifier.element

def cacheWhenCalculable(
	self,
	operation: BaseOperation,
)-> bool:
	return operation.runtimeData[ OperationType.OPTIMIZATION].valueCalculable


class Operation( BaseOperation):

	def beginOptimisation(
		self,
		optimisationTypes: set[ OptimizationRequests.OptimizationRequest],
	):
		"""
		Creates desired session data
		record all input-(.)
		the operation result caching structire relies on knowing whether a operation is dependant upon a specific input
		so for each operation, the input-(.) that it is dependant on are recorded, then when assigning value-(.) for input-(.) in various part-(.) of the process, caching should work well
		"""
		if self.sessionData== None:
			setSession(
				self,
			)

		self.sessionData.cachingBehaviour= cacheWhenCalculable
		inputData: dict[ InputLocator, SingleInputData]= dict()
		def retrieveInputValueFromSetValues(
			sessionData,
			inputLocator: InputLocator,
		)-> InputValue| None:
			return inputData[ inputLocator].setValue
		self.sessionData.retrieveInputValue= retrieveInputValueFromSetValues
		
		walker= self.walkFromSpecifiedRoot(
			walkType= NavigableNetwork.WalkType.FIRST_TIP_TO_ROOT,
			# walkedLinkageTypes= BaseOperation.ElementLinkageTypes.CONTAINED_OPERATION#| BaseOperation.ElementLinkageTypes.CONTAINED_INPUT,
		)
		for specifier in walker:
			specifier.element.runtimeData[ OperationType.OPTIMIZATION]= OptimizationRuntimeData()
			# decided= True
			if isinstance( specifier.element, Operation):
				inputs= specifier.element.getContainedNetworkElements( BaseOperation.ElementLinkageTypes.CONTAINED_INPUT)
				if len( inputs):
					specifier.element.runtimeData[ OperationType.OPTIMIZATION].valueCalculable= False
					for ( inputLocator, input,) in inputs.iter(
						locator= True,
						index= False,
						contained= True,
					):
						site= specifier.getContainedSpecifier(
							linkageType= BaseOperation.ElementLinkageTypes.CONTAINED_INPUT,
							connectionId= inputLocator,
						)
						if input.locator in inputData:
							inputData[ input.locator].sites.add( site)
							inputData[ input.locator].dependantOperations.add( specifier.element)
						else:
							inputData[ input.locator]= SingleInputData(
								sites= { site},
								dependantOperations= { specifier.element},
							)
						specifier.element.runtimeData[ OperationType.OPTIMIZATION].dependantOnInputs.add( input.locator)

					# decided= False
					# op

				# if decided:
				for ( operation,) in specifier.element.getContainedNetworkElements( BaseOperation.ElementLinkageTypes.CONTAINED_OPERATION).iter():
					if len( operation.runtimeData[ OperationType.OPTIMIZATION].dependantOnInputs)> 0:
						specifier.element.runtimeData[ OperationType.OPTIMIZATION].valueCalculable= False

						specifier.element.runtimeData[ OperationType.OPTIMIZATION].dependantOnInputs|= operation.runtimeData[ OperationType.OPTIMIZATION].dependantOnInputs
					# if operation.runtimeData[ OperationType.OPTIMIZATION][ OptimizationRuntimeData.DECIDED]== False:
						# decided= False
						# break

			# Calculate here, perhaps calculate can be recursive
			# So caluculate calls calculate of contained
			# So why use decided?
			# Well at the top operation, it prevents chaining down every time
			# So combine these two, implement a calculate on the operation type
			# runtime locator can be optimisaion string, or maybe general operation type enum

			# specifier.element.runtimeData[ OperationType.OPTIMIZATION][ OptimizationRuntimeData.DECIDED]= decided

		resultData= ResultData(
			order= tuple( inputData.keys()),
			values= { request: dict() for request in optimisationTypes},
		)
		iterateThroughRemainingInputs(
			optimisationTypes= optimisationTypes,
			inputData= inputData,
			remainingInputs= set( inputData.keys()),
			rootOperation= self,
			resultData= resultData,
		)
		return {
			request: request.decideBetweenResults( resultData.values[ request])
			for request in resultData.values
		}


	def generateRequests(
		self,
		request: OptimizationRequests.OptimizationRequest,
		desiredContained: None| set[ OperationContainedIdentifier],
	)-> dict[
		OperationContainedIdentifier,
		set[ OptimizationRequests.OptimizationRequest],
	]:
		raise NotImplementedError

	def assignInputValue(
		self,
		containedOperation: OperationContainedIdentifier,
		request: OptimizationRequests.OptimizationRequest,
	)-> InputValue| None:
		if containedOperation not in self.containedOperationIdentifiersSet:
			raise ValueError( "Operation not present")
		return self._assignInputValue(
			containedOperation= containedOperation,
			request= request,
		)

	def _assignInputValue(
		self,
		containedOperation: OperationContainedIdentifier,
		request: OptimizationRequests.OptimizationRequest,
	)-> InputValue| None:
		raise NotImplementedError


class Operations:
	Input= Input
	Constant= Constant

	class Multiplication( Operation):
		containedOperationIdentifiers: ClassVar[ set]= {
			"q",
			"w",
		}
		def __hash__( self): return id( self)

		def _calculateValue( self):
			return self.containedOperations[ "q"].calculateValue()* self.containedOperations[ "w"].calculateValue()
		
		def generateRequests(
			self,
			request: OptimizationRequests.OptimizationRequest,
			desiredContained: None| set[ OperationContainedIdentifier],
		)-> dict[
			OperationContainedIdentifier,
			set[ OptimizationRequests.OptimizationRequest],
		]:
			# Only two contained
~~~~~~
~~~~~{.python .numberLines}
			# Generate regardless of set status
			# for { q, w} if other value known then use that to request
			# should never be generating for op which is known
	 		# should never be generating for op which is set

			requestType= type( request)

			if isinstance( desiredContained, dict) and ( len( desiredContained)== 1):
				# Check if the other~s value is known
				requestedOp= list( desiredContained)[ 0]
				otherOpId= self.containedOperationIdentifiers.difference( desiredContained).pop()
				otherOp= self.containedOperations[ otherOpId]
				if otherOp.runtimeData[ OperationType.OPTIMIZATION].valueCalculable:
					otherValue= otherOp.calculateValue()
					requestType
					if requestType in { OptimizationRequests.Maximization, OptimizationRequests.Minimization}:
						otherValueNegative= otherValue< 0
						match ( otherValueNegative, requestType):
							case ( False, OptimizationRequests.Maximization):
								generatedRequests= { requestedOp: { OptimizationRequests.Maximization()}}
							case ( False, OptimizationRequests.Minimization):
								generatedRequests= { requestedOp: { OptimizationRequests.Minimization()}}
							case ( True, OptimizationRequests.Maximization):
								generatedRequests= { requestedOp: { OptimizationRequests.Minimization()}}
							case ( True, OptimizationRequests.Minimization):
								generatedRequests= { requestedOp: { OptimizationRequests.Maximization()}}
					elif requestType== OptimizationRequests.ObtainValue:
						generatedRequests= {
							requestedOp: {
								OptimizationRequests.ObtainValue(
									value= request.value/ otherValue,
								),
							},
						}
					else:
						raise NotImplementedError
				else:
					if requestType in { OptimizationRequests.Maximization, OptimizationRequests.Minimization}:
						generatedRequests= {
							requestedOp: {
								OptimizationRequests.Minimization(),
								OptimizationRequests.Maximization(),
							}
						}
					elif requestType== OptimizationRequests.ObtainValue:
						generatedRequests= {
							requestedOp: {
								OptimizationRequests.ObtainValue(
									value= request.value,
								),
							},
						}
					else:
						raise NotImplementedError

			else:
				if requestType in { OptimizationRequests.Maximization, OptimizationRequests.Minimization}:
					generatedRequests= {
						"q": {
							OptimizationRequests.Minimization(),
							OptimizationRequests.Maximization(),
						},
						"w": {
							OptimizationRequests.Minimization(),
							OptimizationRequests.Maximization(),
						},
					}
				elif requestType== OptimizationRequests.ObtainValue:
					# Not sure what to do here, 
					# Could request both for both, but the iteration-s where they are both requested to be the same would be wasted
					# This may be a case where site optimisation is insufficient, good example case
					# Just request value for each at the minute
					generatedRequests= {
						"q": {
							OptimizationRequests.ObtainValue(
								value= request.value,
							),
						},
						"w": {
							OptimizationRequests.ObtainValue(
								value= request.value,
							),
						},
					}
				else:
					raise NotImplementedError

			return generatedRequests
			# If both are un decided then 
			# No matter the request

		def _assignInputValue(
			self,
			containedOperation: OperationContainedIdentifier,
			request: OptimizationRequests.OptimizationRequest,
		)-> InputValue| None:
			# Unsure here, maybe just infinity, well if other is known then important
			otherOperationId= self.containedOperationIdentifiers.difference( set( containedOperation)).pop()
			otherOperation= self.containedOperations[ otherOperationId]
			if otherOperation.runtimeData[ OperationType.OPTIMIZATION].valueCalculable:
				otherOperationValue= otherOperation.calculateValue()
			else:
				otherOperationValue= None
				
			match type( request):
				case OptimizationRequests.Maximization:
					if otherOperationValue== None:
						# Try for a positive, all that matters is that it is the oposite of the Minimization response
						return infinity
					else:
						if otherOperationValue< 0:
							return negativeInfinity
						else:
							return infinity
				case OptimizationRequests.Minimization:
					if otherOperationValue== None:
						return negativeInfinity
					else:
						if otherOperationValue< 0:
							return infinity
						else:
							return negativeInfinity
				case OptimizationRequests.ObtainValue:
					if otherOperationValue== None:
						# How to make it easiest for the other choice to pick the exact value, 
						# perhaps it is set here and the other choice must be 1
						return request.value
					else:
						return request.value/ otherOperationValue

	class Clamp( Operation):
		containedOperationIdentifiers: ClassVar= {
			"clamped",
			"minimum",
			"maximum",
		}

		def _calculateValue( self):
			return max(
				min(
					self.containedOperations[ "clamped"].calculateValue(),
					self.containedOperations[ "maximum"].calculateValue(),
				),
				self.containedOperations[ "minimum"].calculateValue(),
			)
		
		def generateRequests(
			self,
			request: OptimizationRequests.OptimizationRequest,
			desiredContained: None| set[ OperationContainedIdentifier],
		)-> dict[
			OperationContainedIdentifier,
			set[ OptimizationRequests.OptimizationRequest],
		]:
			# when maximising
			# for clamped maximize
			# for minimum, maximize
			# for minimum, maximize

			# opposite for minimizing

			# for obtenance as well, set all to the desired request
			# So pass through all for now
			return {
				"clamped": { request},
				"minimum": { request},
				"maximum": { request},
			}

		def _assignInputValue(
			self,
			containedOperation: OperationContainedIdentifier,
			request: OptimizationRequests.OptimizationRequest,
		)-> InputValue| None:
			# I think in a proper approach, one would assign infinitite value-(?) for minimization and maximisarion
			# but here use min and max contained value-(.) if possible
			# for obtenance
			# set each to the value
			match type( request):
				case OptimizationRequests.Maximization:
					if containedOperation== "clamped":
						if self.containedOperations[ "maximum"].runtimeData[ OperationType.OPTIMIZATION].valueCalculable:
							return self.containedOperations[ "maximum"].calculateValue()
						else:
							return None
					else:
						return infinity
				case OptimizationRequests.Minimization:
					if containedOperation== "clamped":
						if self.containedOperations[ "minimum"].runtimeData[ OperationType.OPTIMIZATION].valueCalculable:
							return self.containedOperations[ "minimum"].calculateValue()
						else:
							return None
					else:
						return negativeInfinity
				case OptimizationRequests.ObtainValue:
					return request.value

	class Sin( Operation):
		containedOperationIdentifiers: ClassVar= {
			"contained",
		}

		def _calculateValue( self):
			return sin( self.containedOperations[ "contained"]).calculateValue()
		
		def generateRequests(
			self,
			request: OptimizationRequests.OptimizationRequest,
			desiredContained: None| set[ OperationContainedIdentifier],
		)-> dict[
			OperationContainedIdentifier,
			set[ OptimizationRequests.OptimizationRequest],
		]:
			...
			# Radians
			# So request the value to satisfy the desired result

		def _assignInputValue(
			self,
			containedOperation: OperationContainedIdentifier,
			request: OptimizationRequests.OptimizationRequest,
		)-> InputValue| None:
			...
			# So same as request but assignment
			# perhaps assignment with constraint would require being able to select a request of the constraint system
			# maybe could map out whole constraint space
			# so how would one map out input-(.) dependance on each other?
			# how then would the optimization system hhok into this?, need to know how the optimisation system fit outside of site optimisation
~~~~~~
\normalsize

---
